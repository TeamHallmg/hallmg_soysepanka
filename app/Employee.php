<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\JobPosition;

class Employee extends Model
{
    use SoftDeletes;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idempleado', 'nombre', 'paterno', 'materno', 'fuente', 'rfc', 'curp', 'nss', 'correoempresa', 'correopersonal', 'nacimiento', 'sexo', 'civil', 'telefono', 'extension', 'celular', 'ingreso', 'fechapuesto', 'jefe', 'direccion', 'department', 'seccion', 'job_position_id', 'grado', 'region', 'sucursal', 'enterprise_id', 'division', 'marca', 'centro', 'checador', 'turno', 'tiponomina', 'clavenomina', 'nombrenomina', 'generalista', 'relacion', 'contrato', 'horario', 'jornada', 'calculo', 'vacaciones', 'flotante', 'base', 'rol', 'password', 'extra1', 'extra2', 'extra3', 'extra4', 'extra5', 'fecha', 'version'];

    public function getFullNameAttribute(){
        return $this->nombre . ' ' . $this->paterno . ' ' . $this->materno;
    }

    public function user()
    {
        return $this->hasOne(User::class, 'employee_id');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'jefe', 'idempleado');
    }
  
    public function boss(){
        return $this->hasOne(Employee::class, 'idempleado', 'jefe');
    }

    public function jobPosition()
    {
        return $this->belongsTo(JobPosition::class, 'job_position_id', 'id');
    }

    public function department(){
        if(!is_null($this->jobPosition)){
            return $this->jobPosition->department;
        }
        return null;
    }

    public function area(){
        if(!is_null($this->department)){
            return $this->department->area;
        }
        return null;
    }

}

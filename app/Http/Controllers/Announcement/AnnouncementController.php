<?php

namespace App\Http\Controllers\Announcement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use Laracasts\Flash\Flash;
use Carbon\Carbon;

use App\Http\Controllers\Controller;

use App\Models\Announcement\AnnouncementCategory;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementTypePerView;
use App\Models\Region;
use App\Models\Announcement\AnnouncementForm;
use App\Models\Announcement\AnnouncementFormData;
use App\Models\Announcement\AnnouncementAttribute;
use App\Models\Announcement\Announcement;
use App\Models\Announcement\View;

use App\Models\Access\Access;

class AnnouncementController extends Controller
{
    protected $path;

    public function __construct()
    {
        $this->middleware('auth');

        //$this->path = public_path() . '/img/announcements/';
        $this->path = getcwd() . '/img/announcements/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $type = Input::get('type');
        $view = Input::get('view');
        if($view = View::find($view)){
            $announcements = Announcement::where('view_id',$view->id);
            if($announcement_types = AnnouncementType::find($type)){
                if(!AnnouncementTypePerView::check($view->id, $type)){
                    $announcement_types =  $view->announcementsInThisView;
                    return view('announcement.announcement_types.types', compact('announcements','view','announcement_types','type'));
                }
                $this->deactivateAnnouncementsIfMust($view->id, $type);
                $announcements = $announcements->where('announcement_type_id',$announcement_types->id)->get();
                $announcement_types = AnnouncementType::where('id',$type)->first();
                $announcement_types->headers = $announcement_types->getFormHeaders();
                foreach ($announcements as $key => $announcement) {
                    $announcement->data = $announcement->formData();
                }
                return view('announcement.announcements.index', compact('announcements','view','announcement_types','type'));
            }else{
                $announcement_types =  $view->announcementsInThisView;
                return view('announcement.announcement_types.types', compact('announcements','view','announcement_types','type'));
            }
        }
        $views = View::all();
        return view('announcement.announcements.views', compact('views'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Input::get('type');
        $view = Input::get('view');
        if($view = View::find($view)){
            $view = $view->id;
            if($announcement_type = AnnouncementType::find($type)){
                $announcement_types[$announcement_type->id] = $announcement_type->name;
            }else{
                // $announcement_type = AnnouncementType::all();
                // foreach ($announcement_type as $key => $value) {
                //     $announcement_types[$value->id] = $value->name;
                // }
                return redirect()->to('announcements?view='.$view);
            }

            $parents = [];
            if(!is_null($announcement_type->typeParent)){
                $parents = $announcement_type->typeParent->announcements;
            }
            $annParent = [];
            foreach ($parents as $key => $parent) {
                $parent->data = $parent->formData();
                if(isset($parent->data['title'])){
                    $annParent[$parent->id] = $parent->data['title'];
                }
            }
            
            $regions = Region::pluck('name', 'id')->toArray();
            $categories = AnnouncementCategory::pluck('name', 'id')->toArray();

            $form = AnnouncementForm::getAnnouncementForm($announcement_type->id);
            return view('announcement.announcements.create', compact('view','announcement_types','regions','categories','form','announcement_type', 'annParent'));
        }else{
            flash('La pagina no existe')->warning();
        }
        $views = View::all();
        return view('announcement.announcements.views', compact('views'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $view = View::find($request->view);
        if(!is_null($view)){
            $request = $request->all();
            $form_data = [];

            $now = Carbon::now();
            
            //Sets the Start Date
            $startsHour = empty($request['starts_hour']) ? '00' : $request['starts_hour'];
            $startsMinute = empty($request['starts_minute']) ? '00' : $request['starts_minute'];
            $starts = empty($request['starts']) ? $now : Carbon::parse($request['starts'] . ' '. $startsHour . ':' . $startsMinute);

            //Sets the End Date
            $endsHour = empty($request['ends_hour']) ? '00' : $request['ends_hour'];
            $endsMinute = empty($request['ends_minute']) ? '00' : $request['ends_minute'];
            $ends = empty($request['ends']) ? $now->copy()->addDays(7) : Carbon::parse($request['ends'] . ' '. $endsHour . ':' . $endsMinute);

            $announcement_parent = null;
            if(isset($request['parent'])){
                $announcement_parent = Announcement::find($request['parent'])->id;
            }

            $announcement = Announcement::create([
                'starts' => $starts,
                'ends' => $ends,
                'active' => false,
                'approved_by_user_id' => null,
                'created_by_user_id'  => Auth::user()->id,
                'announcement_type_id' => $request['type'],
                'announcement_category_id' => (isset($request['category']))?$request['category']:null,
                //'announcement_parent_id' => $announcement_parent,
                'region_id' => (isset($region))?$region:null,
                'view_id' => $view->id
            ]);
            //dd($announcement);

            $attrs = AnnouncementForm::getAttr($request['type']);
            //dd($attrs);
            foreach ($attrs as $key => $attr) {
                $campo = '';
                if(isset($request[$attr->form_name])){
                    if($attr->attr_view == 'file'){
                        if (! is_null($request[$attr->form_name])) {
                            $campo = time() . '-'. $request[$attr->form_name]->getClientOriginalName();
                            $request[$attr->form_name]->move($this->path, $campo);
                        }
                    }else if($attr->attr_view == 'multi-file') {
                        if (! is_null($request[$attr->form_name])) {
                            $rutas = [];
                            foreach($request[$attr->form_name] as $file){
                                $campo = time() . '-'. $file->getClientOriginalName();
                                $rutas[] = $campo;
                                $file->move($this->path, $campo);
                            }
                            //dd($rutas);
                            $campo = json_encode($rutas, JSON_UNESCAPED_UNICODE);
                            //dd($campo);
                        }
                    }else{
                        $campo = $request[$attr->form_name];
                    }
                    $formID = AnnouncementForm::where('announcement_type_id',$request['type'])->where('announcement_attribute_id', $attr->id)->first();
                    $form_data = [
                        'value' => $campo,
                        'announcement_form_id' => $formID->id,
                        'announcement_id' => $announcement->id
                    ];
                    //dd($form_data,$request,$campo);
                    AnnouncementFormData::create($form_data);
                }
            }
            // Flash::success('El registro fue agregado');
            return redirect()->to('announcements?view='.$view->id.'&type=' . $announcement->announcement_type_id);
        }else{
            // Flash::success('La pagina no existe');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = Announcement::findOrFail($id);
        $view = $announcement->view_id;
        $announcement_type = $announcement->announcementType;
        $announcement_types[$announcement->announcement_type_id] = $announcement->announcementType->name;

        $parents = [];
        if(!is_null($announcement_type->typeParent)){
            $parents = $announcement_type->typeParent->announcements;
        }
        $annParent = [];
        foreach ($parents as $key => $parent) {
            $parent->data = $parent->formData();
            if(isset($parent->data['title'])){
                $annParent[$parent->id] = $parent->data['title'];
            }
        }

        $regions = Region::pluck('name', 'id')->toArray();
        $categories = AnnouncementCategory::pluck('name', 'id')->toArray();

        $dates = [
            'starts' => [
                'date' => date('Y-m-d',strtotime($announcement->starts)),
                'hour' => date('H', strtotime($announcement->starts)),
                'minute' => date('i', strtotime($announcement->starts)),
            ],
            'ends'=> [
                'date' => date('Y-m-d', strtotime($announcement->ends)),
                'hour' => date('H', strtotime($announcement->ends)),
                'minute' => date('i', strtotime($announcement->ends)),
            ]
        ];

        $form = AnnouncementForm::getAnnouncementForm($announcement_type->id);
        foreach ($form as $key => $attr) {
            $attr->data = optional($attr->getData($announcement->id))->value;
        }
        return view('announcement.announcements.edit', compact('view','announcement_types','regions','categories','form','announcement_type', 'announcement', 'dates', 'annParent'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $announcement = Announcement::findOrFail($id);
        $category = (isset($request->category))?AnnouncementCategory::find($request->category)->id:null;
        $region = (isset($request->region))?Region::find($request->region)->id:null;

        //Sets the Start Date
        $startsHour = empty($request['starts_hour']) ? '00' : $request['starts_hour'];
        $startsMinute = empty($request['starts_minute']) ? '00' : $request['starts_minute'];
        $starts = empty($request['starts']) ? $now : Carbon::parse($request['starts'] . ' '. $startsHour . ':' . $startsMinute);

        //Sets the End Date
        $endsHour = empty($request->ends_hour) ? '00' : $request->ends_hour;
        $endsMinute = empty($request->ends_minute) ? '00' : $request->ends_minute;
        $ends = empty($request->ends) ? $now->copy()->addDays(7) : Carbon::parse($request->ends . ' '. $endsHour . ':' . $endsMinute);

        $announcement_parent = null;
        if(isset($request['parent'])){
            $announcement_parent = Announcement::find($request['parent'])->id;
        }

        Announcement::where('id', $id)->update([                
            'starts' => $starts,
            'announcement_category_id' => $category,
            //'announcement_parent_id' => $announcement_parent,
            'region_id' => $region,
            'ends' => $ends
        ]);
        $request = $request->all();
        // Obtenemos todos los datos del formulario a partir de tipo
        $attrs = AnnouncementForm::getAttr($announcement->announcement_type_id);
        $form_data = [];
        foreach ($attrs as $key => $attr) {
            $form_id = AnnouncementForm::findAndGetID($announcement->announcement_type_id, $attr->id);
            $campo = '';
            $form_data = AnnouncementFormData::findItem($form_id, $announcement->id);
                    
            if(isset($request[$attr->form_name]) && !is_null($form_data)){
                $destinationPath = 'img/announcements/';
                $campo = $request[$attr->form_name];
                if($attr->attr_view == 'file'){
                    if (file_exists($this->path . $form_data->value)){
                        unlink($this->path . $form_data->value);
                    }
                    $campo = time() . '-'. $request[$attr->form_name]->getClientOriginalName();
                    $request[$attr->form_name]->move($this->path, $campo);

                    File::delete($destinationPath.$form_data->value);
                } elseif($attr->attr_view == 'multi-file') {
                    if (! is_null($request[$attr->form_name])) {
                        $rutas = [];
                        foreach($request[$attr->form_name] as $file){
                            $campo = time() . '-'. $file->getClientOriginalName();
                            $rutas[] = $campo;
                            $file->move($this->path, $campo);
                        }
                        $campo = json_encode($rutas, JSON_UNESCAPED_UNICODE);

                        $file_array = json_decode($form_data->value); // Datos anteriores
                        foreach($file_array as $file) {
                            File::delete($destinationPath.$file);
                        }
                    }
                }
                $form_data->value = $campo;
                $form_data->save();
            }else{
                if(!isset($request[$attr->form_name]) || is_null($request[$attr->form_name])){
                    continue;
                }
                if($attr->attr_view == 'file'){
                    if (isset($request[$attr->form_name]) && !is_null($request[$attr->form_name])) {
                        $campo = time() . '-'. $request[$attr->form_name]->getClientOriginalName();
                        $request[$attr->form_name]->move($this->path, $campo);
                    }else{
                        continue;
                    }
                } elseif($attr->attr_view == 'multi-file') {
                    if (! is_null($request[$attr->form_name])) {
                        $rutas = [];
                        foreach($request[$attr->form_name] as $file){
                            $campo = time() . '-'. $file->getClientOriginalName();
                            $rutas[] = $campo;
                            $file->move($this->path, $campo);
                        }
                        //dd($rutas);
                        $campo = json_encode($rutas, JSON_UNESCAPED_UNICODE);
                        //dd($campo);
                    }
                } else{
                    $campo = $request[$attr->form_name];
                }
                $form_data = [
                    'value' => $campo,
                    'announcement_form_id' => $form_id,
                    'announcement_id' => $announcement->id
                ];
                AnnouncementFormData::create($form_data);
            }
        }
        // Flash::info('El registro fue editado');
        return redirect()->to('announcements?view=' .$request['view'].'&type=' . $request['type']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcement = Announcement::find($id);
        $view = $announcement->view_id;
        $type = $announcement->announcement_type_id;
        $form_data = $announcement->announcementFormData;
        foreach ($form_data as $key => $data) {
            $form_elem = $data->announcementForm;
            $attr = $form_elem->announcementAttribute;
            if($attr->attr_view == 'file'){
                if (file_exists($this->path . $data->value)){
                    unlink($this->path . $data->value);
                }
            }
        }
        AnnouncementFormData::where('announcement_id', $id)->delete();
        $announcement->delete();

        // Flash::success('El anuncio se ha borrado exitosamente');
        return redirect()->to('announcements?view=' .$view. '&type=' .$type);
    }

    /**
     * Update the active status for the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        
        $announcement = Announcement::findOrFail($id);
        $status = ! $announcement->active;
        Announcement::where('id', $id)->update([
            'active' => $status,
            'approved_by_user_id' => Auth::user()->id
        ]);
        // Flash::success('El registro fue editado');
        return redirect()->to('announcements?view=' .$announcement->view_id. '&type=' .$request->type);
    }

    public function deleteFiles($id, $type = null){
        $announcement = Announcement::find($id);
        $files = explode(',', trim($announcement->file,'[]'));
        if((is_null($type) || 'file' === $type) && (!is_null($announcement->file) || !empty($announcement->file))){
            foreach ($files as $key => $file) {
                $file = trim($file,'""');
                if (file_exists($this->path . $file)){
                    unlink($this->path . $file);
                }
            }
        }
        if(is_null($type) || 'document' === $type){
            if (!is_null($announcement->document) && !empty($announcement->document) && file_exists($this->path2 . $announcement->document)){
                unlink($this->path . $announcement->document);
            }
        }
        if(is_null($type) || 'image' === $type){
            if (!is_null($announcement->image) && !empty($announcement->image) && file_exists($this->path . $announcement->image)){
                unlink($this->path . $announcement->image);
            }
        }
    }

    public function deactivateAnnouncementsIfMust($view = null, $type = null) {
        $currDate = date('Y-m-d H:m:s');
        Announcement::when(!is_null($view), function ($q) use ($view){
            $q->where('view_id', $view);
        })
        ->when(!is_null($type), function ($q) use ($type) {
            $q->where('announcement_type_id', $type);
        })
        ->where('ends', '<=', $currDate)
        ->update(['active' => 0]);
    }

    public function example()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $banner = Announcement::getAnnouncementsToDisplay($view->id, 'banner');
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $carrousel_secondary = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel_secondary');
        $display_announcements = compact('banner','carrousel','carrousel_secondary');
        return view('home', compact('display_announcements'));
    }

    public function quienes()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $panels = Announcement::getAnnouncementsToDisplay($view->id, 'panels');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('panels');
        return view('quienes-somos', compact('display_announcements'));
    }

    public function valores()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $banner_panel = Announcement::getAnnouncementsToDisplay($view->id, 'banner_panel');
        $cards = Announcement::getAnnouncementsToDisplay($view->id, 'cards');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('cards','banner_panel');
        return view('valores', compact('display_announcements'));
    }

    public function tablero()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $mosaico = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('carrousel','mosaico');
        return view('tablero', compact('display_announcements'));
    }
}

<?php

namespace App\Http\Controllers\Announcement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement\AnnouncementType;
use App\Models\Announcement\AnnouncementTypePerView;
use App\Models\Announcement\View;

class AnnouncementTypePerViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $views = View::all();
        $types = AnnouncementType::all();

        return view('announcement.announcement_types_per_view.index', compact('views', 'types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        AnnouncementTypePerView::truncate();
        $views = $request->all();
        unset($views['_token']);
        unset($views['_method']);
        foreach ($views as $key => $view) {
            foreach ($view as $key2 => $type) {
                AnnouncementTypePerView::insert([
                    'announcement_type_id' => $type,
                    'view_id' => $key
                ]);
            }
        }
        flash('Guardado Correctamente')->success();
        return redirect()->to('announcement_types_per_view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

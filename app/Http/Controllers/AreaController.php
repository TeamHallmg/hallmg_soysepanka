<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use App\Models\DeletedReason;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::select('id','name','description','department_id')->orderBy('id', 'ASC')->get();
        foreach($areas as $area){
            $department = Department::select('id','name','direction_id')->where('id',$area->department_id)->first();
            $direction = Direction::select('id','name')->where('id',$department->direction_id)->first();

            $area->department_name = $department->name;
            $area->direction_name = $direction->name;
        }
        //dd($areas);
        return view('areas.index', compact(['areas']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $directions = Direction::select('id','name')->orderBy('name', 'DESC')->get();
        return view('areas.create', compact(['directions']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $rules = array(
            'name' => 'required',
            'description' => 'max:500',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

        try {
            $form_data = array(
                'department_id' => $request->department,
                'name'  =>  '',
                'description'  =>  $request->description,
            );

            if($request->name == -1)
                $form_data['name'] = $request->name_add;
            else {
                Area::withTrashed()->find($request->name)->restore();
                $form_data['name'] = $request->area_name;
            }
            
            $area = Area::createCascade($request->direccion, $request->department, $form_data['name']);
            $area->description = $form_data['description'];
            $area->save();
        } catch (\Throwable $th) {
            dd($th);
        }

        return redirect()->route('areas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::where('id',$id)->first();
        $department = Department::where('id',$area->department_id)->first();
        $direction = Direction::where('id',$department->direction_id)->first();
        //dd($direction,$department);
        $directions = Direction::select('id','name')->orderBy('name', 'DESC')->get();
        return view('areas.edit', compact(['area','department','direction','directions']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
            'description' => 'max:500',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withErrors(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'department_id' => $request->department,
            'name'  =>  $request->name,
            'description'  =>  $request->description,
        );

        Area::where('id',$id)->update($form_data);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $area = Area::find($id);

            foreach($area->jobs as $key => $job) {
                foreach($job->employees as $key => $employee) {
                    $employee->job_position_id = null;
                    $employee->save();
                }
                $job->delete();
            }

            $data = new DeletedReason;
            $data->user_id = Auth::user()->id;
            $data->reason = $request->del_reason;
            $area->deleted_reason()->save($data);

            $area->delete();
             DB::commit();
        } catch(\Throwable $th) {
            DB::rollback();
            dd($th->getMessage());
        }

        return redirect()->route('areas.index');
    }

    public function trashedAreas($id){
        return Area::select('id','name','description')->where('department_id',$id)->orderBy('id', 'ASC')->onlyTrashed()->get();
    }
}

<?php

namespace App\Http\Controllers\CronAutomatico;

use Hash;
use Mail;

use App\User;
use App\Employee;
use Carbon\Carbon;
use App\Models\Area;
use App\Models\Direction;

use App\Models\Department;
use App\Models\Enterprise;
use App\Models\JobPosition;
use App\Models\Cron\LogChanges;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\ColumnDefinition;
use App\Http\Controllers\CronAutomatico\MoodleCronController as MoodleImport;

class CronController extends Controller
{
    public function __construct(){
      //$this->middleware('auth');
        $this->separador = '|';
        $this->pathLayout = 'Interface/';
        $this->fileName = 'importador2.csv'; //Nombre del archivo a importar

    //   $this->encoding = 'UTF-8';
    //   $this->extern_fileName = 'csv_externo';

        $this->dataError = [];
        $this->created_users = [];
        $this->create_errors = [];
        $this->updated_users = [];
        $this->update_errors = [];
        $this->deleted_users = [];
        $this->delete_errors = [];

        $this->pivotKey = 'rfc';
        $this->folder = '';

        $this->checkColumnsCount = true;
        $this->checkIfHasHeaders = true;
        $this->allowEmptyFile = false;
        $this->usesEmployeeModel = true;
        $this->usesMoodleIntegration = true;
        $this->columnTypes = [];
        $this->setToNull_ifCantConvert = false;
        $this->keepTrackOfChanges = true;
        $this->firstDetectedSource = null;
        $this->workOnlyWithSameSource = false;
        $this->newUsers = [];
        $this->sendMailToNewUsers = true;
        $this->defaultPassword = 'admin'; //Contraseña por defecto para nuevos usuarios
        $this->fieldsToEncryp = ['password']; //Campos a encriptar
        $this->unsetIfEmpty = ['password']; //Eliminar campos que vengan vacios, para no actulizar con información en blanco
        /*
         * Este bloque es por el momento para cuando usamos los dos modelos 
         * Para la tabla de personal el arreglo de datos debe de venir en arreglo separado por comas
         * Para la tabla de users el arreglo debe de venir arreglo con llaves, 
         *      La llave referencia a los datos en el archivo y el valor a la tabla de users
         */

        /* Esta variable nos sirve para indicarle que campo tomara del modelo de personal
         * para usar en el usuario. Se incluye la linea de codigo para Usar. Ejemplo
         * 
         * $personal->{$this->personalFieldToUser['email']},
         */
        $this->personalFieldToUser = ['email' => 'correoempresa'];
        $this->uniquePersonalSimulatedFields = ['correoempresa', 'rfc'];
        $this->uniqueUserSimulatedFields = ['correoempresa' => 'email'];
        /*****/
        $this->canNotBeEmpty = ['correoempresa', 'rfc'];
        $this->hierarchyFields = ['direccion' => 'Sin Dirección', 'departamento' => 'Sin Departamento', 'area' => 'Sin Área', 'puesto' => 'Sin Puesto'];
        $this->except_model_fields = ['departamento', 'puesto', 'empresa', 'idempresa', 'area'];
        $this->fieldsToIgnoreTrack = ['updated_at', 'created_at', 'fecha', 'password'];
        $this->fileHeaders = array('idempleado',
                                    'nombre',
                                    'paterno',
                                    'materno',
                                    'fuente',
                                    'rfc',
                                    'curp',
                                    'nss',
                                    'correoempresa',
                                    'correopersonal',
                                    'nacimiento',
                                    'sexo',
                                    'civil',
                                    'telefono',
                                    'extension',
                                    'celular',
                                    'ingreso',
                                    'fechapuesto',
                                    'jefe',
                                    'direccion',
                                    'departamento',
                                    'seccion',
                                    'puesto',
                                    'grado',
                                    'region',
                                    'sucursal',
                                    'idempresa',
                                    'empresa',
                                    'division',
                                    'marca',
                                    'centro',  
                                    'checador',
                                    'turno',
                                    'tiponomina',
                                    'clavenomina',
                                    'nombrenomina',
                                    'generalista',
                                    'relacion',
                                    'contrato',
                                    'horario',
                                    'jornada',
                                    'calculo',
                                    'vacaciones',
                                    'flotante',
                                    'base',
                                    'rol',
                                    'password',
                                    'extra1',
                                    'extra2',
                                    'extra3',
                                    'extra4',
                                    'extra5',
                                    'fecha',
                                    'version');
    
      $this->campos = array();

      $this->table = 'people'; //Cambiar el nombre de la tabla

      $this->path = Storage::disk('local')
                    ->getDriver()
                    ->getAdapter()
                    ->getPathPrefix();
    }

    /**
     * Funcion principal del cron, la cual es llamada por el archivo de rutas
     *
     * @return void
     */
    public function mainFunction(){
        $data = $this->readFile();
        $this->loadColumnTypes();
        if($this->allowEmptyFile || count($data) > 0){
            $data = $this->formatData($data);
            $data = $this->fillHierarchyInUsers($data);
            $hierarchy = $this->loadDirections($data);
            $hierarchy = $this->loadDepartments($data, $hierarchy);
            $hierarchy = $this->loadAreas($data, $hierarchy);
            $hierarchy = $this->loadJobPositions($data, $hierarchy);
            $enterprises = $this->loadEnterprise($data);
            $this->loadInformationToDatabase($data, $hierarchy, $enterprises);
            // $this->sendWelcomeEmail();
        }
        $this->sendReportEmail();
    }

    /**
     * Leeremos el archivo usando la definición de variables en el construnctor
     * Se validaran ciertas cosas formateandolo todo en un array de datos
     * 
     * Nota: Si el separadador no es el correcto el archivo funciona pero se acomoda mal 
     *
     * @return void
     */
    public function readFile(){
        $File = fopen($this->path . $this->pathLayout . $this->fileName, 'r');
        $i = 0;
        $csvData = [];
        while ($line = fgetcsv($File, 0, $this->separador)){
            $data = [];
            if($this->checkIfHasHeaders){
                if($this->check_if_firstrow_match_headers($line)){
                    $this->checkIfHasHeaders = false;
                }
                else{
                    dd('Aqui muere porque las cabeceras no cuadran');
                }
                if(!$this->check_if_delimiter_is_correct($line)){
                    dd('Muero, posible fallo con el delimitador', $line);
                }
            }else{
                if(!$this->check_if_delimiter_is_correct($line)){
                    dd('Muero, posible fallo con el delimitador', $line);
                }
                //Leemos toda las lineas y las acomodamos ordenadas
                for($i = 0; $i < count($line); $i++){
                    $data[$this->fileHeaders[$i]] = trim($line[$i]);
                }
                $csvData[] = $data;
            }
        }
        if(count($csvData) > 0){
            return $csvData;
        }else if($this->allowEmptyFile){
            return [];
        }
        else{
            dd('No hay datos asi que muero');
        }
    }
    
    /**
     * Formatea la información generada por la función readFile()
     * tomando la estructura de la base de datos, intenta converitir la información 
     * al tipo de dato asignado, si falla quita el registro y lo guarda para reportes
     * 
     * Nota: Tiene mas tipos de validaciones internas definidias en el constructor 
     *
     * @param  array $data
     *
     * @return array
     */
    public function formatData($data){
        $problems = [];
        foreach($data as $pos => &$row){
            foreach($row as $key => &$field){
                switch($this->columnTypes[$key]) {
                    case "string":
                        if(in_array($key, $this->canNotBeEmpty) && empty($field)){
                            $this->dataError[] = 'El campo de la columna _' . $key. '_ no puede venir vacio. Fila: ' . strval($pos + 1);
                            unset($data[$pos]);
                        }
                        break;
                    case "integer":
                        if(is_numeric($field)){
                            $field = intval($field);
                        }else{
                            if($this->setToNull_ifCantConvert){
                                $field = null;
                            }else{
                                $this->dataError[] = 'Este campo en la columna _' . $key. '_ es de tipo INTEGER y el valor (' . $field .') no se puede convertir correctamente. Fila: ' . strval($pos + 1);
                                unset($data[$pos]);
                            }
                        }
                        break;
                    case "date":
                        try {
                            if(empty($field)){
                                $field = null;
                            }else{
                                $date = new Carbon($field);
                                $field = $date->format('Y-m-d');
                            }
                        } catch (\Throwable $th) {                            
                            if($this->setToNull_ifCantConvert){
                                $field = null;
                            }else{
                                $this->dataError[] = 'Este campo en la columna _' . 
                                    $key. '_ es de tipo DATE y el valor (' . 
                                    $field . ') no se puede convertir correctamente. Fila: ' .
                                    strval($pos + 1);
                                unset($data[$pos]);
                            }
                        }
                        break;
                    default:
                        break;
                }
                if($this->workOnlyWithSameSource && is_null($this->firstDetectedSource)){
                    $this->firstDetectedSource = $row['fuente'];
                }
                if(in_array($key, $this->unsetIfEmpty)){
                    if(empty($field)){
                        unset($row[$key]);
                    }
                }else if(in_array($key, $this->fieldsToEncryp)){
                    $field = \Hash::make($field);
                }
            }
        }
        return $data;
    }

    /**
     * Crea o trae la información de la Empresa guardandolo en un arreglo con llaves
     * donde la llave es el nombre de la Empresa y el valor el ID
     *
     * @param array $data
     *
     * @return void
     */
    public function loadEnterprise($data){
        $enterprises = [];
        \DB::beginTransaction();
        foreach($data as $line){
            if(trim($line['empresa']) !== ""){
                try{
                    $enterprise = Enterprise::firstOrCreate([
                        'name' => trim($line['empresa']),
                        'enterprise_code' => $this->issetOrNull($line,'idempresa'),
                    ]);
                    $id = $enterprise->id;
                } catch (\Throwable $th) {
                    $id = null;
                }
                $enterprises[(trim($line['empresa']))] = $id;
            }
        }
        \DB::commit();
        return $enterprises;
    }

    /**
     * Crea o trae la información de las Direcciones guardandolo en un arreglo con llaves
     * donde la llave es el nombre de la Direccion y el valor es un arreglo donde el ID
     * de la empresa viene en el campo ['_id'];
     *
     * @param array $data
     *
     * @return array
     */
    public function loadDirections($data){
        $directions = [];
        \DB::beginTransaction();
        foreach($data as $line){
            try {
                $direction = Direction::firstOrCreate([
                    'name' => $line['direccion'],
                ]);
                $directions[$line['direccion']] = ['_id' => $direction->id];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return $directions;
    }

    /**
     * Crea o trae la información de los Departamentos guardandolo en un arreglo con llaves
     * donde la llave es el nombre del Departamento y el valor es un arreglo donde el ID
     * de la empresa viene en el campo ['_id'];
     *
     * @param  array $data
     * @param  array $hierarchy //Esta información debe de ser 
     * generada por la funcion loadDirections();
     *
     * @return array 
     */
    public function loadDepartments($data, $hierarchy){
        \DB::beginTransaction();
        foreach($data as $line){
            try{
                $department = Department::firstOrCreate([
                    'name' => $line['departamento'],
                    'direction_id' => $hierarchy[$line['direccion']]['_id'],
                ]);
                $hierarchy[$line['direccion']][$line['departamento']] = ['_id' => $department->id];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return $hierarchy;
    }

    /**
     * Crea o trae la información de las Areas guardandolo en un arreglo con llaves
     * donde la llave es el nombre del Area y el valor es un arreglo donde el ID
     * de la empresa viene en el campo ['_id'];
     *
     * @param  array $data
     * @param  array $hierarchy //Esta información debe de ser 
     * generada por la funcion loadDepartments();
     *
     * @return array
     */
    public function loadAreas($data, $hierarchy){
        \DB::beginTransaction();
        foreach($data as $line){
            try{
                $area = Area::firstOrCreate([
                    'name' => $line['area'],
                    'department_id' => $hierarchy[$line['direccion']][$line['departamento']]['_id'],
                ]);
                $hierarchy[$line['direccion']][$line['departamento']][$line['area']] = ['_id' => $area->id];
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return $hierarchy;
    }

    /**
     * Crea o trae la información de los Puestos guardandolo en un arreglo con llaves
     * donde la llave es el nombre del Puesto y el valor es el ID
     *
     * @param  array $data
     * @param  array $hierarchy //Esta información debe de ser 
     * generada por la funcion loadAreas();
     *
     * @return array
     */
    public function loadJobPositions($data, $hierarchy){
        \DB::beginTransaction();
        foreach($data as $line){
            try {
                $jobPosition = JobPosition::firstOrCreate([
                    'name' => $line['puesto'],
                    'area_id' => $hierarchy[$line['direccion']][$line['departamento']][$line['area']]['_id'],
                ]);
                $hierarchy[$line['direccion']][$line['departamento']][$line['area']][$line['puesto']] = $jobPosition->id;
            } catch (\Throwable $th) {
                dd($th->getMessage());
            }
        }
        \DB::commit();
        return $hierarchy;
    }
    
    /**
     * Fill the array given with the data set in the constructor var "hierarchyFields"
     * when the array does not have this information.
     * 
     * @param  Array $data
     *
     * @return void
     */
    public function fillHierarchyInUsers($data){
        foreach($data as &$line){
            foreach($this->hierarchyFields as $key => $field){
                if(isset($line[$key]) && empty($line[$key])){
                    $line[$key] = $field;
                }elseif(!isset($line[$key])){
                    $line[$key] = $field;
                
                }
            }
        }
        return $data;
    }

    /**
     * Redirige el trabajo a la funcion correspondiente dependiendo si el cron
     * maneja 1 o los 2 modelos que manejamos
     * 
     * NOTA IMPORTANTE: La parte de loadSigleModel() no esta bien probada ya que no se usa con frecuencia
     *
     * @param  array $data
     * @param  array $jobs
     * @param  array $enterprise
     *
     * @return void
     */
    public function loadInformationToDatabase($data, $jobs, $enterprise){
        if($this->usesEmployeeModel){
            $this->loadWithBothModels($data, $jobs, $enterprise);
        }else{
            $this->loadSigleModel($data, $jobs);
        }
    }

    /**
     * Aqui es donde hacemos todas las inserciones y actualizaciones a la base de datos
     * utilizando los dos modelos
     * 
     * Nota: Tiene multiples configuraciones, revisar a fondo.
     *
     * @param  array $data
     * @param  array $hierachy
     * @param  array $enterprise
     *
     * @return void
     */
    public function loadWithBothModels($data, $hierachy, $enterprise){
        $personalUpdated = [];
        $personalCreated = [];
        foreach ($data as $key => $user) {
            $personal = Employee::withTrashed()
            ->with([
                'user' => function($q){
                    $q->withTrashed();
                }
            ])
            ->where($this->pivotKey, $user[$this->pivotKey])
            ->first();
            if($personal){
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'FUENTE');
                        continue;
                    }
                }
                if($this->activatedIfNeeded($personal)){
                    $beforePersonal = $personal->replicate();
                    if(!$this->verfyUniqueFields($user, $personal)){
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'DUPLICATE_FIELDS');
                        continue;
                    }
                    $personal = $this->changePersonalData($personal, $user);
                    $personal->job_position_id = $this->getJobInHierarchy($user, $hierachy);
                    $personal->enterprise_id = $this->issetOrNull($enterprise, $user['empresa']);

                    $hasChanged = count($personal->getChanges()) > 0 ?true:false;
                    \DB::beginTransaction();
                    try {
                        $personal->save();
                    } catch (\Throwable $th) {
                        \DB::rollback();
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                        continue;
                    }
                    try{
                        $personal->user()->update([
                            'first_name' => $personal->nombre,
                            'last_name' => $personal->paterno . ' ' . $personal->materno,
                            'email' => $personal->{$this->personalFieldToUser['email']},
                            'password' => $personal->password,
                        ]);
                    } catch (\Throwable $th) {
                        \DB::rollback();
                        $this->update_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                        continue;
                    }
                    if(!$this->saveChangesToLog($personal, $beforePersonal)){
                        //Aqui truena si no pudimos guardar el LOG
                        // continue;
                    }
                    \DB::commit();
                    if($hasChanged){
                        $this->updated_users[] = $this->formatMessageWithPersonnelData($user);
                    }
                    $personalUpdated[] = $personal->id;
                };
            }else{
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'FUENTE');
                        continue;
                    }
                }
                if(!$this->verfyUniqueFields($user)){
                    $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'DUPLICATE_FIELDS');
                    continue;
                }
                \DB::beginTransaction();
                $user['job_position_id'] = $this->getJobInHierarchy($user, $hierachy);
                $user['enterprise_id'] = $this->issetOrNull($enterprise,strtoupper($user['empresa']));
                unset($user['puesto']);
                unset($user['empresa']);
                $user['password'] = \Hash::make($this->defaultPassword);
                try {
                    $personal = Employee::create($user);
                } catch (\Throwable $th) {
                    DB::rollback();
                    $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                    continue;                    
                }
                try {
                    $_user = User::create([
                        'employee_id' => $personal->id,
                        'first_name' => $personal->nombre,
                        'last_name' => $personal->paterno . ' ' . $personal->materno,
                        'email' => $personal->{$this->personalFieldToUser['email']},
                        'password' => $personal->password,
                        'role' => 'employee',
                        'active' => 1,
                    ]);
                } catch (\Throwable $th) {
                    $this->create_errors[] = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                    \DB::rollback();
                    continue;
                }
                \DB::commit();
                $personalCreated[] = $personal->id;
                $this->created_users[] = $this->formatMessageWithPersonnelData($user);
                $this->newUsers[] = $_user;
            }
        }
        $personalDeleted = 0;
        $personalNotToDelete = array_merge($personalUpdated, $personalCreated);
        if(count($personalNotToDelete) > 0){
            $queryPersonals = Employee::whereNotIn('id', $personalNotToDelete);
            $personalDeleted = $queryPersonals->count();
            $personals = $queryPersonals->get();
            foreach ($personals as $personal) {
                try {
                    \DB::beginTransaction();
                    $user = $personal->toArray();
                    if($personal->user)
                        $personal->user->delete();
                    $personal->delete();
                    $this->deleted_users[] = $this->formatMessageWithPersonnelData($user);
                    \DB::commit();
                } catch (\Throwable $th) {
                    \DB::rollback();
                    $this->delete_errors = $this->formatMessageWithPersonnelData($user, 'EXCEPTION', $th->getMessage());
                }    
            }   
        }
    }

    /**
     * Aqui es donde hacemos todas las inserciones y actualizaciones a la base de datos
     * utilizando un unico modelo
     * 
     * ADVERTENCIA: NO SE HA TERMINADO, FALLARA SEGURAMENTE
     *
     * @param  mixed $data
     *
     * @return void
     */
    public function loadSigleModel($data){
        $personalUpdated = [];
        $personalCreated = [];
        foreach ($data as $key => $user) {
            $personal = User::withTrashed()
            ->where($this->pivotKey, $user[$this->pivotKey])
            ->first();
            
            if($personal){
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->update_errors[] = 'Se quiere asignar la fuente('.$user['fuente'].') que no corresponde a la primera(' . $this->firstDetectedSource . ') con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                        continue;
                    }
                }
                if($this->activatedIfNeeded($personal)){
                    $beforePersonal = $personal->replicate();
                    if(!$this->verfyUniqueFields($user, $personal)){
                        $this->update_errors[] = 'Ya existe un usuario con ese correo _' . $user['correoempresa'] . '_ con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                        continue;
                    }
                    $personal = $this->changePersonalData($personal, $user);
                    \DB::beginTransaction();
                    try {
                        $personal->save();
                    } catch (\Throwable $th) {
                        \DB::rollback();
                        $this->update_errors[] = 'No se pudo actualizar el USUARIO con el campo('.$user[$this->pivotKey].')  |  ' . $th->getMessage();
                        continue;
                    } 
                    if(!$this->saveChangesToLog($personal, $beforePersonal)){
                        //Aqui truena si no pudimos guardar el LOG
                        // continue;
                    }
                    \DB::commit();

                    $personalUpdated[] = $personal->id;
                };
            }else{
                if(!is_null($this->firstDetectedSource)){
                    if($user['fuente'] != $this->firstDetectedSource){
                        $this->create_errors[] = 'Se quiere asignar la fuente('.$user['fuente'].') que no corresponde a la primera(' . $this->firstDetectedSource . ') con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                        continue;
                    }
                }
                if(!$this->verfyUniqueFields($user)){
                    $this->create_errors[] = 'Ya existe un usuario con ese correo _' . $user['correoempresa'] . '_ con el ' . strtoupper($this->pivotKey) . ': ' . $user[$this->pivotKey];
                    continue;
                }
                \DB::beginTransaction();
                $user['password'] = \Hash::make($this->defaultPassword);
                try {
                    $personal = User::create($user);    
                } catch (\Throwable $th) {
                    DB::rollback();
                    $this->create_errors[] = 'No se pudo crear el EMPLEADO con el campo('.$user[$this->pivotKey].')  |  ' . $th->getMessage();
                    continue;                    
                }
                \DB::commit();
                $personalCreated[] = $personal->id;
                $this->newUsers[] = $personal;
            }
        }
        $personalDeleted = 0;
        $personalNotToDelete = array_merge($personalUpdated, $personalCreated);
        if(count($personalNotToDelete) > 0){
            try {
                $queryPersonals = User::whereNotIn('id', $personalNotToDelete);
                $personalDeleted = $queryPersonals->count();
                $queryPersonals->delete();
            } catch (\Throwable $th) {

            }            
        }
        dd($this->dataError, $this->update_errors, $this->create_errors, count($personalUpdated), $personalDeleted);
    }

    /**
     * Intenta reactivar un usuario borrado logicamente si es necesario
     *
     * @param  Illuminate\Database\Eloquent\Model $personal
     * @param  bool $first
     *
     * @return bool
     */
    public function activatedIfNeeded($personal, $first = true){
        $val = false;
        if($personal){
            if(!is_null($personal->deleted_at)){
                try {
                    $personal->restore();
                } catch (\Throwable $th) {
                    $val = false;
                }
            }
            $val = true;
        }
        if($this->usesEmployeeModel && $first){
            $this->activatedIfNeeded($personal->user, false);
        }
        return $val;
    }

    /**
     * Revisa si la primer fila en el archivo cuadra con las cabeceras definidas
     * en la var $this->fileHeaders, si estas no cuadran falla
     *
     * @param  mixed $data
     *
     * @return bool
     */
    public function check_if_firstrow_match_headers($data){
        $fileHeaders = [];
        $readFileHeaders = [];
        /*
         * Convertimos a MAYUSCULAS las cabeceras para comparar el orden y la cantidad
         */
        foreach($this->fileHeaders as $header){
            $fileHeaders[] = strtoupper($header);
        }
        foreach($data as $header){
            $readFileHeaders[] = strtoupper($header);
        }
        for ($i = 0; $i < count($fileHeaders); $i++) { 
            if($fileHeaders[$i] !== $readFileHeaders[$i]){
                dd($fileHeaders[$i], $readFileHeaders[$i]);
                return false;
            }
        }
        return true;
    }

    /**
     * Comprueba si el delimitador proporcionado es correcto comparando el tamaño
     * del arreglo obtenido contra el total de cabeceras definidas en @var $this->fileHeaders
     *
     * @param  array $data
     *
     * @return bool
     */
    public function check_if_delimiter_is_correct($data){
        if(count($data) == count($this->fileHeaders)){
            return true;
        }
        return false;
    }

    /**
     * Carga los tipos de columnas a partir de la estructura de la base de datos
     * formando un arreglo donde la llave es el nombre y el valor es el tipo de dato
     *
     * @return void
     */
    public function loadColumnTypes(){
        $array = [];
        if($this->usesEmployeeModel){
            $tableName = with(new Employee)->getTable();
        }else{
            $tableName = with(new User)->getTable();
        }
        foreach($this->fileHeaders as $header){ 
            if(Schema::hasColumn($tableName, $header)){
                $type = \DB::getSchemaBuilder()->getColumnType($tableName, $header);
                $array[$header] = $type;
            }else{
                $array[$header] = null;
            }
        }
        $this->columnTypes = $array;
    }

    /**
     * Actualiza la información del modelo con la información proporcionada
     * ignorado los elementos listados en @var $this->except_model_fields
     *
     * @param  Illuminate\Database\Eloquent\Model $personal
     * @param  array $data
     *
     * @return void
     */
    public function changePersonalData($personal, $data){
        foreach ($data as $key => $value) {
            if(!in_array($key, $this->except_model_fields))
                $personal->$key = $value;
        }
        return $personal;
    }

    /**
     * Scope a query to only include 
     *
     * @param  
     * @return 
     */

    /**
     * Hace consultas orWhere por cada elemento en 
     * @var $this->uniqueUserSimulatedFields para la tabla de users
     * @var $this->uniquePersonalSimulatedFields para la tabla de empleado
     * para simular la unicidad de datos en la base de datos
     * 
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  array $user
     * @param  string $type
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function whereClausesForUnique($query, $user, $type = 'user'){
        $whereClauses = [];
        if($type == "user"){
            foreach($this->uniqueUserSimulatedFields as $key => $field){
                $query->orWhere($field, $user[$key]);
            }
        }else{
            foreach($this->uniquePersonalSimulatedFields as $field){
                $query->orWhere($field, $user[$field]);
            }
        }
        return $query;
    }

    /**
     * Aqui estructuramos las consultas necesarias para verificar 
     * si no hay complictos con campos unicos en la base de datos
     *
     * @param  array $userData
     * @param  array $personalData
     *
     * @return bool
     */
    public function verfyUniqueFields($userData, $personalData = null){
        $unique = false;
        if($this->usesEmployeeModel){
            $personal = Employee::when(!is_null($personalData), function($q) use($personalData){
                $q->where('id', '!=', $personalData->id);
            })
            ->where(function($q) use($userData){
                $this->whereClausesForUnique($q, $userData, 'employee');
            })
            ->first();

            $personalAux = null;
            if(!is_null($personalData)){
                $personalAux = Employee::find($personalData->id);
            }

            $user = null;
            $user = User::when(!is_null($personalAux) && !is_null($personalAux->user), function($q) use ($personalAux){
                $q->where('id', '!=', $personalAux->user->id);
            })
            ->where(function($q) use ($userData){
                $this->whereClausesForUnique($q, $userData);
            })
            ->first();

            $unique = (is_null($personal) && is_null($user))?true:false;
        }else{
            $user = User::when(!is_null($personalData), function($q) use($personalData){
                $q->where('id', '!=', $personalData->id);
            })
            ->where(function($q) use ($userData){
                $this->whereClausesForUnique($q, $userData);
            })
            ->count();
            $unique = ($user > 0)?false:true;
        }
        return $unique;
    }

    /**
     * Apartir de la estructura generara en las funciones de Hierarchy
     * regresaremos el id del puesto apartir de la información generada del archivo
     *
     * @param  array $line
     * @param  array $hierarchy
     *
     * @return integer //ID
     */
    public function getJobInHierarchy($line, $hierarchy){
        try {
            return $hierarchy[$line['direccion']][$line['departamento']][$line['area']][$line['puesto']];
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
    }

    /**
     * Enviamos los correos de bien a los usuarios nuevos
     *
     * @return void
     */
    public function sendWelcomeEmail(){
        if($this->sendMailToNewUsers){
            foreach($this->newUsers as $user){
                Mail::raw('Bienvenido a la plataforma tu Usuario es: ' . 
                    $user->email . 
                    ' y tu Contraseña es: ' . $this->defaultPassword , function ($message) use ($user) {
                    $message->from('soporte@cron.com', 'Cron Soporte');
                    $message->to($user->email, $user->FullName);
                    $message->subject('Bienvenido');
                });
            }
        }
    }

    /**
     * Enviamos el correo de reporte
     *
     * @return void
     */
    public function sendReportEmail(){
        $data = [
            'data_error' => $this->dataError,
            'created_users' => $this->created_users,
            'create_error' => $this->create_errors,
            'updated_users' => $this->updated_users,
            'update_errors' => $this->update_errors,
            'deleted_users' => $this->deleted_users,
            'delete_errors' => $this->delete_errors
        ];
        dd($data);
        Mail::send('emails.cron.status_report', ['data' => $data], function ($message) {
            $message->from('soporte@cron.com', 'Cron Soporte');
            $message->to('soporte@hallmg.com', 'Soporte Hallmg');
            $message->subject('Resumen de importación de Usuarios');
        });
    }

    /**
     * Guardamos los cambios detectados en la base de datos
     *
     * @param  Illuminate\Database\Eloquent\Model $personal
     * @param  array $user
     *
     * @return void
     */
    public function saveChangesToLog($personal, $user){
        if($this->keepTrackOfChanges){
            $changes = $personal->getChanges();
            if(count($changes) > 0){
                foreach ($this->fieldsToIgnoreTrack as $key) {
                    if(isset($changes[$key])){
                        unset($changes[$key]);
                    }
                }
                $logChanges = [];
                foreach ($changes as $key => $value) {
                    if($personal->$key !== $user->$key){
                        $logChanges[] = [
                            'old-'.$key => $user->$key,
                            'new-'.$key => $personal->$key,
                        ];
                    }
                }
                if(count($logChanges) <= 0){
                    return true;
                }
                try {
                    if($this->usesEmployeeModel){
                        LogChanges::create([
                            'personal_id' => $personal->id,
                            'changes' => json_encode($logChanges),
                        ]);
                    }else{
                        LogChanges::create([
                            'user_id' => $personal->id,
                            'changes' => json_encode($logChanges),
                        ]);
                    }
                    return true;
                } catch (\Throwable $th) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * Almacenamos el layout en la carpeta designada
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function storeLayout(){
        
    }

    /**
     * Verifica si existe el index y regresa el valor
     * Si no existe regresa Null
     *
     * @param  array $value
     * @param  mixed $index
     *
     * @return mixed
     */
    public function issetOrNull($value, $index){
        return isset($value[$index])?$value[$index]:null;
    }

    /**
     * Formateamos la información del empleado para informar sobre quien se trabajo
     *
     * @param  array $data
     * @param  string $error
     *
     * @return void
     */
    public function formatMessageWithPersonnelData($data, $error = "", $specific = ""){
        $msg = "";
        if(!empty($error)){
            $msg = $this->formatErrorMessage($data, $error, $specific) . ' || ';
        }
        $aux = trim($data['nombre'] . ' ' . $data['paterno'] . ' ' . $data['materno']);
        $msg .= (!empty($aux))?'Nombre: ' . $aux . ' -- ' : '';
        $aux = trim($data['rfc']);
        $msg .= (!empty($aux))?'RFC: ' . $aux . ' -- ' : '';
        $aux = trim($data['correoempresa']);
        $msg .= (!empty($aux))?'CORREOEMPRESA: ' . $aux . ' -- ' : '';
        $aux = trim($data['correopersonal']);
        $msg .= (!empty($aux))?'CORREOPERSONAL: ' . $aux . ' -- ' : '';
        $msg = trim($msg);
        $msg = rtrim($msg, ' --');
        return $msg;
    }

    /**
     * A partir de los parametros formeatea el error solicitado
     *
     * @param  array $data
     * @param  string $error
     * @param  string $specific
     *
     * @return string
     */
    public function formatErrorMessage($data, $error, $specific = ""){
        $msg = "";
        switch($error){
            case "FUENTE":
                $msg = 'Se quiere asignar la fuente(' . $user['fuente'] . 
                ') que no corresponde a la primera(' . $this->firstDetectedSource . ')';
            break;
            case "DUPLICATE_FIELDS":
                $msg = 'Conflicto de duplicidad con alguno de los siguientes campos (';
                $msg .= implode(', ', $this->uniquePersonalSimulatedFields);
                $msg .= ')';
            break;
            case "EXCEPTION":
                $msg = 'Excepción encontrada (' . $specific . ')';
        }
        return $msg;
    }
}

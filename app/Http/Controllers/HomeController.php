<?php

namespace App\Http\Controllers;

use App\Models\Announcement\Announcement;
use App\Models\Announcement\View;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $carrousel = Announcement::getAnnouncementsToDisplay($view->id, 'carrousel');
        $mosaico = Announcement::getAnnouncementsToDisplay($view->id, 'mosaico');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('carrousel','mosaico');
        return view('home', compact('display_announcements'));
    }

    public function filosofia()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = substr($url, 1);

        $view = View::where('name',$url)->first();
        $panels = Announcement::getAnnouncementsToDisplay($view->id, 'panels');
        // $display_announcements = compact('banner','cards','banner_panel','panels','carrousel','carrousel_secondary','mosaico','tabla_anuncios','cuatro_columnas','por_categorias');
        $display_announcements = compact('panels');
        return view('filosofia', compact('display_announcements'));
    }
}

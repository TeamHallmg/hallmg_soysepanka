<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\EmployeeForm;
use App\HCpuesto;
use App\Employee;
use App\EmployeeEnterprise;
use App\Models\Moodle\User as UserMdl;
use Hash;
use Illuminate\Validation\Rule;
use Exception;
use DB;
use Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;

use App\Models\JobPosition;
use App\Models\Direction;
use App\Models\Department;
use App\Models\Area;
use App\Models\JobPositionLevel;
use App\Models\Enterprise;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function logActivity($request){
        LogActivity::create([
            'user_id'       =>   Auth::id(),
            'user_email'    =>   Auth::user()->email,
            'tag'           =>  $request->method(),
            'url'           =>  $request->fullUrl(),
            'user_agent'    =>  \Illuminate\Support\Facades\Request::header('User-Agent'),
            'ip_address'    =>  \Illuminate\Support\Facades\Request::ip()
        ]);
    }*/

    public function index(Request $request)
    {
        
        $usuarios = User::with(['employee' => function($q) {
            $q->withTrashed();
        }])
        ->whereHas('employee', function($q){
            $q->withTrashed();
        })
        ->orderBy('id', 'DESC')->withTrashed()->get();

        foreach ($usuarios as $key => $usuario) {
            $direction = Direction::select('id','name')->where('id',$usuario->employee->direccion)->first();
            $job = JobPosition::select('id','name')->where('id',$usuario->employee->job_position_id)->first();

            // if($direction != null)
            // {
            //     $usuario->employee->direccion = $direction->name;
            // }

            if(isset($usuario->employee->jobPosition->area->department->direction->name)) {
                $usuario->employee->direccion = $usuario->employee->jobPosition->area->department->direction->name;
            } else {
                $usuario->employee->direccion = 'N/A';
            }

            if($job != null)
            {
                $usuario->employee->job_position_id = $job->name;
            }
            
        }


        // $mdlUsuarios = UserMdl::orderBy('id', 'DESC')->where('icq','2')->get();

        return view('/userAdmin/index', compact(['usuarios']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::with('employee')
        ->whereHas('employee')
        ->orderBy('id', 'DESC')
        ->get();
        
        //columnas de la tabla para generar inputs en la vista
        $inputs = EmployeeForm::orderBy('id', 'ASC')->where('active',true)->get();

        $directions = Direction::select('id','name','description')->orderBy('id', 'DESC')->get();
        $enterprises = Enterprise::select('id','name')->orderBy('name', 'ASC')->get();
        //dd($enterprises);
        $bosses = JobPosition::orderBy('id', 'ASC')->get();
        //$levels = JobPositionLevel::orderBy('id', 'ASC')->get();

        foreach($inputs as $input){
            $form[] = $input->name;
        }

        $columns = Schema::getColumnListing('employees');

        foreach($form as $column){
            $name = EmployeeForm::select('name_show')->where('name',$column)->where('active',true)->first();
            $fields[] = [
                'type' => Schema::getColumnType('employees',$column),
                'column' => $column,
                'nameShow' => $name->name_show,
                //'value' => $usuario->employee->$column
            ];
        }
        // dd($enterprises);

        return view('userAdmin.create', compact(['fields','directions','usuarios','enterprises']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $rules = array(
            'idempleado' => 'required|unique:employees',
            'nombre' => 'required',
            'paterno' => 'required',
            'materno' => 'required',
            'rfc' => 'required|unique:employees',
            'curp' => 'required',
            'correoempresa' => 'required',
            'password' => 'required',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails()){
            return redirect()->back()->withInput()->withErrors(['errors' => $error->errors()->first()]);
        }

        $data = $request->all();
        //dd($data);
        $data['password'] = Hash::make($data['password']);


        //creates
        //Guardar en la tabla de empleados
        \DB::beginTransaction();
        try {
            //code...
            Employee::create($data);

            //Guardar en la tabla de users
            $newEmployee = Employee::orderBy('id', 'DESC')->first();
            User::create([
                'employee_id' => $newEmployee->id,
                'first_name' => $newEmployee->nombre,
                'last_name' => $newEmployee->paterno.' '.$newEmployee->materno,
                'email' => $newEmployee->correoempresa,
                'password' => $newEmployee->password,
                'role' => $newEmployee->rol,
            ]);

            // Guardar en la tabla de mdl_user
            // $newUser = User::orderBy('id', 'DESC')->first();
            // UserMdl::create([
            //     'username' => $newUser->email,
            //     'password' => $newUser->password,
            //     'firstname' => $newUser->first_name,
            //     'lastname' => $newUser->last_name,
            //     'email' => $newUser->email,
            //     'icq' => $newUser->id,
            //     'skype' => $newEmployee->nacimiento,
            //     'yahoo' => $newEmployee->curp,
            //     'aim' => $newEmployee->sexo,
            //     'confirmed' => 1,
            //     'mnethostid' => 1,
            // ]);

            \DB::commit();
            return redirect()->route('admin-de-usuarios.index')->with('success0',$newEmployee->idempleado);
        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
            \DB::rollback();
            return redirect()->route('admin-de-usuarios.index')->withErrors(['Lo sentimos ocurrió un error al crear el usuario, por favor inténtalo más tarde.']);
        }

        return redirect()->route('admin-de-usuarios.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //Datos de employees de usuarios
        //$usuario = User::where('employee_id',$id)->with('employee')->first();
        $users = User::with('employee')
        ->whereHas('employee')
        ->orderBy('id', 'DESC')
        ->get();

        $directions = Direction::select('id','name','description')->orderBy('id', 'DESC')->get();
        $enterprises = Enterprise::select('id','name')->orderBy('name', 'ASC')->get();
        $bosses = JobPosition::orderBy('id', 'ASC')->get();

        $usuario = User::where('employee_id',$id)->with(['employee' => function($q) {
            $q->withTrashed();
        }])
        ->whereHas('employee', function($q){
            $q->withTrashed();
        })
        ->orderBy('id', 'DESC')->withTrashed()->first();

        $job = JobPosition::where('id',$usuario->employee->job_position_id)->first();
        //dd($job->area->name);

        //campos de la tabla de employees_form
        $inputs = EmployeeForm::orderBy('id', 'ASC')->where('active',true)->get();

        foreach($inputs as $input){
            $form[] = $input->name;
        }

        $columns = Schema::getColumnListing('employees');

        foreach($form as $column) {
            $name = EmployeeForm::select('name_show')->where('name',$column)->where('active',true)->first();
            $fields[] = [
                'type' => Schema::getColumnType('employees',$column),
                'column' => $column,
                'nameShow' => $name->name_show,
                'value' => $usuario->employee->$column
            ];
        }

        return view('/userAdmin/edit', compact(['usuario','fields','directions','job','users','enterprises']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
            //dd($id);
            $moodleTable = "mdl_user";

            //datos del formulario
            $nombre = $request->input("nombre");
            $paterno = $request->input("paterno");
            $materno = $request->input("materno");
            $email = $request->input("correoempresa");
            $nacimiento = $request->input("nacimiento");
            $curp = $request->input("curp");
            $sexo = $request->input("sexo");

            $newPassword = $request->input('password');
            $oldPassword = $request->input('oldPassword');

            $user = User::where('id',$id)->has('employee')->first();
            
            $old_email = $user->email;
            $new_email = $request->input('correoempresa');
    
            if( $old_email != $new_email ) {
                $request->validate([
                    'correoempresa' => 'unique:employees',
                ]);
                $user->update(['email'=> $new_email]);
                $user->employee->update(['correoempresa'=> $new_email]);
            } else {
                $mail = $old_email;
            }

        \DB::beginTransaction();
        try {

            if (!empty($newPassword)) {
                $hashedPassword = $user->employee->password;
                if (!Hash::check($oldPassword, $hashedPassword)) {
                    $newPassword = Hash::make($newPassword);
                    
                    $user->update(['password'=> $newPassword]);

                    $user->employee->update(['password'=> $newPassword]);
                    //dd($id,$newPassword,$oldPassword,$moodleTable);
                    UserMdl::where('icq',$id)
                    ->update([
                        'password' => $newPassword,
                    ]);
                }
            }

            //UPDATE datos de Personal sin las siguientes columnas
            $request->offsetUnset('correoempresa');
            $request->offsetUnset('password');
            $request->offsetUnset('_method');
            $request->offsetUnset('_token');

            //UPDATE datos de Personal
            $user->employee->update($request->all());
            
            $user->update([
                'first_name' => $request->input('nombre'),
                'last_name' => $request->input('paterno').' '.$request->input('materno'),
                'role' => $request->input('rol'),
            ]);

            // UserMdl::where('icq',$id)->update([
            //     'firstname' => $nombre,
            //     'lastname' => $paterno.' '.$materno,
            //     'email' => $email,
            //     'username' => $email,
            //     'skype' => $nacimiento,
            //     'yahoo' => $curp,
            //     'aim' => $sexo,
            // ]);

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollback();
            report($e);
            //dd($e);
            if(isset($e->errorInfo)){
                $errors = $e->errorInfo;
                return view('errors.globalError', compact('errors'));
            }elseif(!isset($e->errorInfo)){
                return redirect()->back()->with('success','Ok');
            }
        } 

        //$this->logActivity($request);

        return redirect()->back()->with('success','Ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $user = User::where('employee_id',$id)->with('employee')->first();
        // $mdlUser = UserMdl::where('icq', $user->id)->first();
        $userId = $user->employee->idempleado;
        
        \DB::beginTransaction();
        try {
            //code...
            // $mdlUser->update([
            //     'suspended' => 1,
            // ]);
            $user->delete();
            $user->employee->delete();
            \DB::commit();
            return redirect()->back()->with('success1',$userId);
        } catch (\Throwable $th) {
            //throw $th;
            \DB::rollback();
            return redirect()->back()->withErrors(['Lo sentimos ocurrió un error al suspender al usuario, por favor inténtalo más tarde.']);
        }
        
        return redirect()->back()->with('success');
    }

    public function activate(Request $request, $id)
    {   
        $user = User::where('employee_id',$id)->with(['employee' => function($q) {
            $q->withTrashed();
        }])
        ->whereHas('employee', function($q){
            $q->withTrashed();
        })
        ->orderBy('id', 'DESC')->withTrashed()->first();
        $userId = $user->employee->idempleado;

        // $mdlUser = UserMdl::where('icq', $user->id)->first();

        \DB::beginTransaction();
        try {
            //code...
            // $mdlUser->update([
            //     'suspended' => 0,
            // ]);
            $user->restore();
            $user->employee->restore();
            \DB::commit();
            return redirect()->back()->with('success2',$userId);
        } catch (\Throwable $th) {
            //throw $th;
            \DB::rollback();
            return redirect()->back()->withErrors(['Lo sentimos ocurrió un error al reactivar al usuario, por favor inténtalo más tarde.']);
        }

        return back();
    }
}

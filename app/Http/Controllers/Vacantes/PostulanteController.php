<?php

namespace App\Http\Controllers\Vacantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Vacantes\Postulante;
use App\Models\Vacantes\Vacante;
use App\Models\Profile\Profile;
use App\Models\Profile\ProfileScholarships;
use App\Models\Profile\ProfileApplication;
use App\Models\Profile\ProfileLanguage;
use App\Models\Profile\ProfileKnowledge;
use App\Models\Profile\ProfileExperience;
use App\Models\Profile\ProfileReference;
use App\Models\Profile\ProfileAdditional;
use App\Models\Entidad;
use App\User;

class PostulanteController extends Controller
{
    public function __construct() {
        //$this->middleware('auth');
        // $this->middleware('permission:create_postulation')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();

        //dd($data);

        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        if ($request->hasFile('file')) {
            foreach($request->file as $name => $file) {
                if (!empty($file)) {
                    $filename = uniqid() . '-' .$file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    
                    if ($check) {
                        $file->storeAs('public/profile', $filename);
                        $data[$name] = $filename;
                    } else {
                        return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                    }
                }
            }
        }

        if(Auth::check()) {
            $data['user_id'] = Auth::id();
            $profile = Profile::find(Auth::id());

            if(!is_null($profile)) {
                $data['profile_id'] = $profile->user_id;
            } else {
                $data['profile_id'] = 0;
            }
        } else {
            $data['user_id'] = 0;
            $data['know_vacancy'] = $data['know_vacancy'][0];
            
            try {
                DB::beginTransaction();
                    $profile_id = $this->crearPerfil($data);
                DB::commit();
            } catch (\Throwable $e) {
                DB::rollback();
                /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
                die(); */
                return redirect()->back()->with('alert-danger', '
                    ¡UPS!... NO SE CREO EL PERFIL, Toda la información es obligatoria,
                    DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.'. $e->getMessage());
            }

            $data['profile_id'] = $profile_id;
        }

        $postulado = Postulante::onlyTrashed()->where('user_id', $data['user_id'])->where('user_id', $data['vacante_id'])->exists();
        
        if($postulado) {
            $motivo = Postulante::onlyTrashed()->where('user_id', $data['user_id'])->where('user_id', $data['vacante_id'])->first();
            return redirect()->back()->with('motivo', $motivo->motivo);
        } else {
            $data['intentos_postulacion'] = 'SI';
            try {
                DB::beginTransaction();
                    Postulante::create($data);
                DB::commit();
            }
            catch (\Throwable $e) {
                DB::rollback();
                /* echo 'ERROR (' . $e->getCode() . ') - - - - > ' . $e->getMessage();
                die(); */
                return redirect()->back()->with('alert-danger', '
                    ¡UPS!... NO SE POSTULÓ, ALGO SALIÓ MAL.
                    VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                    DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.'. $e->getMessage());
            }
        }
		// redirect
        if (Auth::check()) {
        	return redirect()->to('vacantes')->with('alert-success', 'Ha sido postulado correctamente!!!');
		} else {
			return redirect()->back()->with('alert-success', 'Ha sido postulado correctamente!!!');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        /*
        * el id que recibe, es el número de la vacante a buscar
        */
        $postulados = Postulante::with('vacante')->with('perfil')->where('vacante_id', $id)->get();
        $can = $postulados->count();

        return view('vacantes.postulante.index', compact('postulados', 'can'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $id = $request->input('id');
        try {
            DB::beginTransaction();
                Postulante::where('id', $id)->update([
                    'motivo'=> $request->input('motivo'),
                    'intentos_postulacion'=> 'NO',
                ]);
                Postulante::findOrFail($id)->delete();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            die; */
            return redirect()->back()->with('alert-danger', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
        }

        return redirect()->back()->with('alert-success', 'El postulante se ha eliminado correctamente!!!');
    }

    public function crear ($id) {
        $vacante = Vacante::with('requisicion')->where('id', $id)->first();
        
        $estados = Entidad::pluck('name', 'id');

        $vacante_id = $vacante->id;
        $puesto = $vacante->requisicion->puesto;

        $profile = null;

        if (Auth::check()) {
            $user_id = Auth::id();
            $profile = Profile::where('user_id', $user_id)->first();
            $user = User::with('employee')->findOrfail($user_id);
        }

        if(is_null($profile)){
            return view('profile.admin', compact('vacante_id', 'puesto', 'estados', 'profile'));
        } else {
            //return View('profile.show', compact('perfil'));
        }
    }

    private function crearPerfil($data) {
        $allowedfileExtension=['pdf','docx', 'jpeg', 'jpg', 'png'];

        $profile = Profile::create($data);
        $data['profile_id'] = $profile->id;

        //guardar los datos en la tabla application
        //leemos si existe un archivo para el domicilio, de lo contrario solo guardamos la info
        if (isset($data['file_address'])) {
            $file = $data['file_address'];
            $filename = uniqid() . '-' .$file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array($extension, $allowedfileExtension);
            
            if ($check) {
                $file->storeAs('public/profile', $filename);
                $data['file_address'] = $filename;
            } else {
                return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
            }
        }
        $application = ProfileApplication::create($data); 
        
        
        //guardar los datos en la tabla escolaridad
        foreach ($data['career_basica'] as $n => $scholarship) {
            $escolaridad = new ProfileScholarships;
            $escolaridad->profile_id = $data['profile_id'];
            $escolaridad->career = $data["career_basica"][$n];
            $escolaridad->school = $data["school_basica"][$n];
            $escolaridad->date_end = $data["date_end_basica"][$n];
            $escolaridad->type = $data["type_basica"][$n];
            $escolaridad->save();
        }

        foreach ($data['studio'] as $n => $scholarship) {
            $escolaridad = new ProfileScholarships;
            $escolaridad->profile_id = $data['profile_id'];
            $escolaridad->studio = $data["studio"][$n];
            $escolaridad->school = $data["school"][$n];
            $escolaridad->date_end = $data["date_end"][$n];
            $escolaridad->voucher = $data["voucher"][$n];
            $escolaridad->type = $data["type"][$n];
            if (isset($data['file_studio'][$n])) {
                $file = $data['file_studio'][$n];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if ($check) {
                    $file->storeAs('public/profile', $filename);
                    $escolaridad->file_studio = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
            $escolaridad->save();
        }

        foreach ($data['studio_uni'] as $n => $scholarship) {
            $escolaridad = new ProfileScholarships;
            $escolaridad->profile_id = $data['profile_id'];
            $escolaridad->studio = $data["studio_uni"][$n];
            $escolaridad->career = $data["career_uni"][$n];
            $escolaridad->voucher = $data["voucher_uni"][$n];
            $escolaridad->type = $data["type_uni"][$n];
            if (isset($data['file_studio_uni'][$n])) {
                $file = $data['file_studio_uni'][$n];
                $filename = uniqid() . '-' .$file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                
                if ($check) {
                    $file->storeAs('public/profile', $filename);
                    $escolaridad->file_studio = $filename;
                } else {
                    return redirect()->back()->with("alert-danger", "Solo se permiten Archivos con extensión pdf|docx|jpeg|jpg|png.");
                }
            }
            $escolaridad->save();
        } 


        //guardar los datos en la tabla language, historial de idiosmas
        foreach ($data['language'] as $n => $language) {
            $language = new ProfileLanguage;
            $language->profile_id = $data['profile_id'];
            $language->language = $data["language"][$n];
            $language->spoken = $data["spoken"][$n];
            $language->reading = $data["reading"][$n];
            $language->writing = $data["writing"][$n];
            $language->save();
        } 
                
        
        //guardar los datos en la tabla conocimientos, historial de mis habilidades
        foreach ($data['knowledge_type'] as $n => $knowledge) {
            $knowledge = new ProfileKnowledge;
            $knowledge->profile_id = $data['profile_id'];
            $knowledge->knowledge_type = $data["knowledge_type"][$n];
            $knowledge->knowledge_name = $data["knowledge_name"][$n];
            $knowledge->save();
        } 


        //guardar los datos en la tabla conocimientos, historial de mis trabajos anteriores
        foreach ($data['job'] as $n => $experience) {
            $experience = new ProfileExperience;
            $experience->profile_id = $data['profile_id'];
            $experience->job = $data["job"][$n];
            $experience->company = $data["company"][$n];
            $experience->date_begin_experience = $data["date_begin_experience"][$n];
            $experience->date_end_experience = $data["date_end_experience"][$n];
            $experience->salary = $data["salary"][$n];
            $experience->reason_separation = $data["reason_separation"][$n];
            $experience->activity = $data["activity"][$n];
            $experience->save();
        } 



        //guardar los datos en la tabla conocimientos, historial de mis referenci9as personales
        foreach ($data['reference_name'] as $n => $reference) {
            $reference = new ProfileReference;
            $reference->profile_id = $data['profile_id'];
            $reference->reference_name = $data["reference_name"][$n];
            $reference->reference_phone = $data["reference_phone"][$n];
            $reference->reference_time_meet = $data["reference_time_meet"][$n];
            $reference->reference_occupation = $data["reference_occupation"][$n];
            $reference->save();
        } 

        $additional = ProfileAdditional::create($data);

        return $profile->id;
    }

}

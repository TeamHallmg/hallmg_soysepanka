<?php

namespace App\Http\Controllers\Vacantes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Vacantes\Reclutador;
use App\User;


class ReclutadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reclutadores = Reclutador::get();
        $usuarios = User::orderby('first_name', 'ASC')->get()->pluck('FullName', 'id')->toArray();

        return View('vacantes.reclutador.index', compact('reclutadores', 'usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $reclutador = $request->input('reclutador');
		
		try {
			DB::beginTransaction();
				Reclutador::create([
                    'user_id' => $reclutador
                ]);
			DB::commit();
		}
		catch (\Throwable $e) {
            DB::rollback();
            return redirect()->back()->with('alert-danger', '
				VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
				DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
		}
	
		// redirect
        return redirect()->back()->with('alert-success', 'Los datos se han agregado correctamente!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            DB::beginTransaction();
                Reclutador::findOrFail($id)->delete();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            die; */
            return redirect()->back()->with('alert-danger', '
                VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
                DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
        }

        return redirect()->back()->with('alert-success', 'El reclutador se ha eliminado correctamente!!!');
    }
}

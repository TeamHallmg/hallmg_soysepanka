<?php

namespace App\Http\Controllers\Vacantes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Vacantes\Vacante;
use App\Models\Requisitions\Requisition;
use App\Models\Vacantes\Reclutador;
use App\Models\Vacantes\Postulante;

class VacantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logueado = Auth::id();
        //mandamos llamar todas las vacantes disponibles
        //$vacantes = Vacante::get();
        $vacantes = Vacante::whereDoesntHave('postulante', function($q) use ($logueado){
            $q->where('user_id', $logueado)
            ->where('intentos_postulacion', 'SI');
        })->get();

        return View('vacantes.vacantes.index', compact('vacantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $requisition = Requisition::find($id);

		// show the edit form and pass the requisitions
		return View('vacantes.vacantes.show', compact('requisition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$requisition = Requisition::with('vacante')->where('id', $id)->first();
        $vacante = Vacante::with('requisicion')->where('id', $id)->first();

		// show the edit form and pass the requisitions
		return View('vacantes.vacantes.edit', compact('vacante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function administrar() {
        //mandamos llamar todas las requisiciones que su estatus es autorizada, pre vacantes
        //será vacante cuando se le asigne un reclutador
        /* $requisitions = Requisition::where('estatus_requi', 'AUTORIZADA')->whereNull('estatus_general')->get();

        if(!empty($requisitions)) {
            foreach ($requisitions as $requisition) {
                $vacante[] = Vacante::where('requisicion_id', $requisition->id)->exists();
            }
        } */

        $requisitions = Requisition::with('vacante')->where('estatus_requi', 'AUTORIZADA')->get();

        //dd($requisitions);

        return View('vacantes.vacantes.admin', compact('requisitions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function recruiter($id) {
        $requisition = Requisition::find($id);
        $reclutadores = Reclutador::get();

		// show the edit form and pass the requisitions
		return View('vacantes.vacantes.recruiter', compact('requisition', 'reclutadores'));
    }

    /**
     * agreggar un reclutador, para la vacante
     */
    public function addRecruiter(Request $request) {
        $data = $request->all();
		
		try {
			DB::beginTransaction();
				Vacante::create($data);
			DB::commit();
		}
		catch (\Throwable $e) {
            DB::rollback();
            /* echo 'ERROR (' . $e->getCode() . '): ' . $e->getMessage();
            die; */
            return redirect()->back()->with('alert-danger', '
				VERIFIQUE QUE LA INFORMACIÓN INGRESADA SEA CORRECTA,
				DE LO CONTRARIO COMUNIQUESE CON EL ÁREA CORRESPONDIENTE.');
		}
	
		// redirect
        return redirect()->to('vacantes')->with('alert-success', 'Los datos se han agregado correctamente!!!');
    }

    public function showExt($id) {
        $vacante = Vacante::with('requisicion')->where('id', $id)->first();

		// show the edit form and pass the requisitions
		return View('vacantesExt.show', compact('vacante'));
    }
}

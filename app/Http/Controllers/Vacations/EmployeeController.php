<?php

namespace App\Http\Controllers\Vacations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Http\Requests;
use App\Models\Vacations\Event;
use App\Models\Vacations\Benefits;
use App\Models\Vacations\Balances;
use App\Models\Vacations\Userfields;
use App\Models\Vacations\Incident;

use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function balances()
    {
        $data = EmployeeController::getEmployeeBalance();
        $data['vacations'] = EmployeeController::getBalanceDetail(5); //Arbitrario
        $data['comodin'] = EmployeeController::getBalanceDetail(6); //Arbitrario
        return view('common.balances',$data);
    }

    public function calendar()
    {
        $data = EmployeeController::getEmployeeBalance();
        $data['vacations'] = EmployeeController::getBalanceDetail(5);
        // $data['config'] = [];
        return view('common.calendar',$data);
    }

    public static function getBalanceDetail($bid, $uid = null )
    {
        if($uid == null) $user_id = Auth::user()->id;
        else $user_id = $uid;

        $rows = Balances::where([['user_id',$user_id],['benefit_id',$bid]])
                    ->orderBy('until','ASC')
                    ->get();

        $data = [];
        $pending = 0;
        $amount = 0;
        $last = null;

        if(!$rows->first()) {
            $data['rows'][] = new Balances(['year'=>date('Y') + 1,'user_id'=>$user_id,'benefit_id'=>$bid]);
            $data['benefit'] = Benefits::find($bid);
            $data['pending'] = $pending;
            $data['amount'] = $amount;
            $data['solicited'] = 0;
            $data['diff'] = 0;
            return($data);
        }

        foreach($rows as $row) {
            $row->diff = $row->pending - $row->amount;
            $pending += $row->pending;
            $amount += $row->amount;
            $events = Event::where('status','pending')->where('js','like','%"id":'.$row->id.',%')->get();
            $row->solicited = 0;
            foreach($events as $event) {
                $js = json_decode($event->js);
                foreach($js->balances as $b) {
                    if($b->id == $row->id)$row->solicited += $b->amount;
                }
            }

            if($row->until == null) {
                $last = $row;
            } else {
                $data['rows'][] = $row;
            }
        }
        $solicited = Incident::where([
			['from_id',Auth::user()->id],
			['status','pending'],
			['event_id','<>','0'],
			['benefit_id', $bid]
		])->sum('amount');
        if($bid == 6){
            // dd($data,$solicited);
        }
        if(!$solicited) $solicited = 0;
        
        if($last)$data['rows'][] = $last;
        $data['benefit'] = Benefits::find($bid);
        $data['pending'] = $pending;
        $data['amount'] = $amount;
        $data['solicited'] = intval($solicited);
        $data['diff'] = $pending - $amount;
        return ($data);
    }

    public static function getEmployeeBalance( $id = null )
    {
        if($id == null) {
            $user = Auth::user();
            $user_id = $user->id;
        } else {
            $user_id = $id;
            $user = User::where('id',$id)->first();
        }
        //Get balances
        $benefits = Benefits::get();
        $balances = [];
        /*No suma bien */
        //SELECT SUM(pending) as pending,SUM(amount) as amount,year,user_id,benefit_id FROM balances WHERE benefit_id = 5 AND user_id = 820 AND year = 2016 GROUP by benefit_id
        foreach($benefits as $benefit) {
            $balance = Balances::select(\DB::raw('SUM(amount) AS amount, SUM(pending) AS pending, benefit_id, user_id, year'))
                        ->where([['user_id',$user_id],['benefit_id',$benefit->id]])
                        ->whereHas('benefit',function($query) use ($user){
                            $query->where('gender','x')
                                ->orWhere('gender',strtolower($user->gender));
                        })
                        ->groupBy('year')
                        ->groupBy('user_id')
                        ->groupBy('benefit_id')
                        ->first();
                        //->toSql();
            if($balance) {
                $balances[] = $balance;
            } else {
                if($benefit->gender != 'x' && $benefit->gender != strtolower($user->gender)) continue;
                $b = [
                    'amount' => 0,
    				'pending' => 0,
    				'benefit_id' => $benefit->id,
    				'user_id' => $user_id,
                    'year' => date('Y'),
                ];
                $balances[] = new Balances($b);
            }
        }
        return (['balances'=>$balances]);
    }

    public static function processTxt()
    {
        $eight = 8 * 60;
        $balances = Balances::where([['benefit_id',10],['pending','>',$eight]])->get(); //Arbitrario

        foreach($balances as $balance) {
            $d = 0;
            while($balance->pending > $eight) {
                $d++;
                $balance->pending -= $eight;
            }
            $b = EmployeeController::getBalanceDetail(11,$balance->user_id); //Arbitrario
            $b['rows'][0]->pending += $d;
            unset($b['rows'][0]->diff);
            $b['rows'][0]->save();
            $balance->save();
        }
    }

    public static function processComodin()
    {
        $year   = date('Y');
        $users  = User::select('id')->get();
        $now    = date('Y-m-d H:m:s');
        $until  = date('Y-m-d',mktime(0,0,0,12,31,$year)); //Arbitrario
        $b = [
            'amount' => 0,
            'pending' => 1,
            'benefit_id' => 6, //Arbitrario...
            'year' => date('Y'),
            'created_at' => $now,
            'updated_at' => $now,
            'until' => $until,
        ];
        $bash = [];
        echo "Generating Bash <br />";
        foreach($users as $user) {
            $b['user_id'] = $user->id;
            $bash[] = $b;
        }
        echo "Inserting Bash in Balances <br />";
        Balances::insert($bash);
    }

    public static function processVacationsCSV($data)
    {
        //Delete * 5 and 6
        $users = User::select(['id','number'])->get();
        $b = [];
        $u = [];
        $date = date('Y-m-d');

        foreach($users as $user) {
            if(isset($data[$user->number])) {
                $tmp = Userfields::where('user_id',$user->id)
                            ->where('fieldname','files')
                            ->where('value','like','%"week":"'.$data[$user->number]['files']['week'].'"%')
                            ->first();
                if($tmp) {
                    Balances::where('user_id',$user->id)->where('benefit_id','5')->update(
                        ['user_id'=>$user->id,'benefit_id'=>'5','amount'=>0,'pending'=>$data[$user->number]['VAC']['pending'],'year'=>$data[$user->number]['VAC']['year']]
                    );
                    Balances::where('user_id',$user->id)->where('benefit_id','6')->update(
                        ['user_id'=>$user->id,'benefit_id'=>'6','amount'=>0,'pending'=>(2 - $data[$user->number]['FD']['pending']),'year'=>$data[$user->number]['FD']['year']]
                    );


                    $tmp->value = json_encode(['from'=>$date,$data[$user->number]['files']]);
                    //if($tmp->id == '987') {dd($data[$user->number],$tmp); die;}
                    $tmp->save();
                    continue;
                }

                Balances::where('user_id',$user->id)->whereIn('benefit_id',['5','6'])->delete();
                //$amount = Incident::where([['benefit_id','5'],['user_id',$user->id],['event_id','<>','0'],['status','complete']])->sum('amount');
                if(isset($data[$user->number]['VAC']))$b[] = ['user_id'=>$user->id,'benefit_id'=>'5','amount'=>0,'pending'=>$data[$user->number]['VAC']['pending'],'year'=>$data[$user->number]['VAC']['year']];
                if(isset($data[$user->number]['FD'])) $b[] = ['user_id'=>$user->id,'benefit_id'=>'6','amount'=>0,'pending'=>(2 - $data[$user->number]['FD']['pending']),'year'=>$data[$user->number]['FD']['year']];
                if($data[$user->number]['files']['pdf']!="") $u[] = ['user_id'=>$user->id,'fieldname'=>'files','value'=>json_encode(['from'=>$date,$data[$user->number]['files']])];
            }
        }
        Balances::insert($b);
        Userfields::insert($u);
    }

    public static function processVacations()
    {
        $year  = date('Y');
        $month = date('m');
        $day   = date('d');
        $now   = date('Y-m-d H:m:s');
        $until = date('Y-m-d',mktime(0,0,0,$month+6,$day,$year+1)); //Arbitrario

        //Un join entre usuarios y balances, 
        //"until" se nulo, sean vacaciones, tienen que tener ser del mes actual y del dia
        //en resumen son los usuarios que cumplen años
        $users  = User::select(['users.id','users.grade','users.started_at','balances.id AS bid','balances.amount'])
                        ->join('balances','users.id','=','balances.user_id')
                        ->where([['balances.until',null],['balances.benefit_id',5]])
                        ->whereMonth('users.started_at',$month)
                        ->whereDay('started_at',$day)
                        // ->where("balances.deleted_at",null)
                        ->get();
        // dd($users,$until);
        $balance = [
            'benefit_id' => 5, //Arbitrario...
            'year' => date('Y'),
            'created_at' => $now,
            'updated_at' => $now,
            'until' => $until,
        ];
        // dd($users,$balance);
        $bash = [];
        $in   = [];
        echo "Generating Bash <br />";
        foreach($users as $user) {
            $balance['amount']  = $user->amount;
            $balance['user_id'] = $user->id;
            $balance['pending'] = $user->getMyVacations();
            if($user->id == 197){
            $bash[] = $balance;
            $in[] = $user->bid;
            }
        }
        // dd($bash,$in);
        echo "Inserting Bash <br />";
        Balances::insert($bash);
        Balances::whereIn('id',$in)->delete();
    }

    public static function removeBenefits()
    {
        $now   = date('Y-m-d');
        $balances = Balances::whereDate('until','<',$now)->delete();
    }

    public static function erraseDuplis($id){
        //SELECT * FROM balances WHERE id IN ( SELECT id FROM balances GROUP BY year,until HAVING count(id) >1 ) and user_id = 291
        //
        $array = Balances::select('id')
            ->where('user_id',$id)
            ->groupBy('year','until')
            ->havingRaw('count(id) > 1')
            ->get();
        foreach ($array as  $value) {
            $value->forceDelete();
        }
        // dd($array);
        // $balances = Balances::whereIn($array)
        // ->where('user_id',$id)
        // ->get();
    }

    public static function fixVacations()
    {
        /*$today  = date('Y-m-d');
        $year   = date('Y');
        Balances::where([['benefit_id',5],['year',$year-1],['until','>',$today]])->update(['year'=>$year]);
        Balances::where([['benefit_id',5],['year',$year-1],['until',null]])->update(['year'=>$year]);*/
    }

    public static function setPartials()
    {
        $bash = [];
        $balance = [];
        // $banderaVic = false;
        //Tomar la lista de usuarios
        $users = User::select(['id','grade','started_at'])->get();//Se tienen todos los usuarios
        $balances = Balances::select('id','user_id')->where([['until',null],['benefit_id',5]])->get();//se obtienen todos los balances con fecha until en null y que sean del beneficio 5 "Vacaciones"
    
        //Se crea un arreglo con [0]id del registro y [1] la id del usuario
        foreach($balances as $balance) {
            $bash[$balance->user_id] = ['id' => $balance->id, 'user_id' => $balance->user_id];
        }

        $now = date('Y-m-d H:m:s');
        $balance = [
            'amount' => 0,
            'benefit_id' => 5, //Arbitrario...
            'created_at' => $now,
            'updated_at' => $now,
        ];

        $when = '';
        $in   = '';
		$flag = false;

        foreach($users as $user) {
            $today = time();
            //Es la fecha de inicio del usuario y se le suman 3 meses
            $stamp = strtotime($user->started_at) + (3 * 30 * 24 * 60 * 60);
            //El if descarta a usuarios muy nuevos que todavia no tienen 3 meses en la empresa
            if($stamp > $today) {
                // dd($stamp,$today,$stamp > $today,$user,"hola");
                unset($bash[$user->id]);
                continue;
            }

            //Se envia mi fecha de inicio en la empresa, se le modifica el año para hacer la fecha lo mas cercana al dia de hoy y se obtienen los dias de diferencia de la fecha a hoy siendo hoy siempre mayor que la fecha creada
            $z = EmployeeController::calculateZ($user->started_at);
            //Numerode vacaciones que me tocan
            $total = $user->getMyVacations();
            $c = floor(($total * $z) / 365);

            //Ni idea para que es C
            // dd($total,$z,$c,$bash[$user->id]);
            if(isset($bash[$user->id]['id'])) {
                $id = $bash[$user->id]['id'];
                $when .= "WHEN '".$id."' THEN '".$c."' ";
                $in .= $id . ', ';
                unset($bash[$user->id]);
            } else {
                // dd($stamp,$today,$user,"adios");
				$flag = true;
                //Si el dia/mes del año en curso no ha pasado, se guarda el año actual, de lo contrario es el siguiente
                $balance['year'] = $user->getMyNextYear();
                $balance['pending'] = $c;
                $balance['user_id'] = $user->id;
                /*$exist = Balances::where('year',$balance['year'])
                ->where('user_id',$balance['user_id'])
                ->where('benefit_id',5)
                ->get();*/
                $bash[$user->id] = $balance;
                // dd($1bash[$user->id],$user);
            }
        }
        // Se insertan los balances de usuarios que acaban de pasar los 3 meses en la empresa
        // dd($bash);
        
        if($flag) Balances::insert($bash);
        //Actualiza las vacaciones
        if($in!='') {
            $in  = rtrim($in, ", ");
            $q  = "UPDATE balances SET pending = CASE id ";
            $q .= $when;
            $q .= "END WHERE id IN (";
            $q .= $in;
            $q .= ")";
            \DB::update(\DB::raw($q));
        }
        // dd($bash);

    }

    private static function calculateZ($date)
    {
        $now   = time();
        $time  = strtotime($date);
        $m     = date('m',$time);
        $d     = date('d',$time);
        $year  = date('Y');
        //A la fecha recibida se le quita el año y se le pone el actual
        $time  = mktime(0,0,0,$m,$d,$year);
        //Si la nueva fecha es mayor a hoy se le resta un año a la nueva fecha $time
        if($time > $now) $time  = mktime(0,0,0,$m,$d,$year-1);

        $datetime1 = date_create(date('Y-m-d',$time));
        $datetime2 = date_create(date('Y-m-d',$now));
        
        $interval = date_diff($datetime1, $datetime2);
        //Se regresan los dias de diferencia
        return ($interval->format('%a'));
    }
}

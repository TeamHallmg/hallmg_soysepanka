<?php

namespace App\Http\Controllers\Vacations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Vacations\Incident;

use App\User;

class ExportController extends Controller
{
    //
    public function export()
    {
        return view('admin.export');
    }

    public function getcsv(Request $request)
    {
        $start = strtotime($request->start);
        $end = strtotime($request->end);
        // dd($request->all());
        $incidents = Incident::with([
            'event',
            'event.eventDays' => function($q){
                // $q->where('processed', '!=', 1);
            }
        ])
        ->whereHas('event', function($q) use ($start, $end){
            $q->whereBetween('start', [$start, $end])
            ->orWhereBetween('end', [$start, $end])
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '>=', $start)
                ->where('end', '<=', $end);
            })
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '<=', $start)
                ->where('end', '>=', $end);
            });
        })->get();
        $csvIncidents = [];
        foreach ($incidents as $key => $incident) {
            $dates = [];
            $beginDate = '';
            $endDate = '';
            $days = 0;
            foreach ($incident->event->eventDays as $key => $day) {
                // if($incident->id == 11)
                // dd($incident->id, date('Y-m-d', $start), $day->date, $day->date, date('Y-m-d',$end), $day->processed, $start <= strtotime($day->date) && strtotime($day->date) <= $end && $day->processed == 0);
                if($start <= strtotime($day->date) && strtotime($day->date) <= $end && $day->processed == 0){
                    $dates[] = $day->date;
                    if(empty($beginDate)){
                        $beginDate = date('d/m/Y', strtotime($day->date));
                    }
                    $endDate = date('d/m/Y', strtotime($day->date));
                    $days++;
                }
            }
            if($days > 0){
                $csvIncidents[] = [
                    'employee' => $incident->from->number,
                    'period'  => 'V'. date('Y', $start),
                    'days' => $days,
                    'zeros' => '0.00',
                    'start' => $beginDate . ',00:00:00',
                    'end' => $endDate . ',00:00:00',
                ];
                // if($incident->id == 11)
                // dd($csvIncidents);
            }
            if(isset($request->mark)){
                $incident->event->eventDays()
                ->whereIn('date', $dates)
                ->update(['processed' => 1]);
            }
        }
        // $myfile = fopen(, "w") or die("Unable to open file!");
        $data = "";
        foreach ($csvIncidents as $key => $line) {
            $data .= "E".$line['employee']."\n";
            $data .= $line['period'].'|'.$line['days'].'|'.$line['zeros'].'|'.$line['start'].'|'.$line['end'].'||'.$line['end']."\n";            
        }
        \Storage::put(date('Y-m-d')."_vac.txt", $data);
        
        return \Storage::download(date('Y-m-d')."_vac.txt");
        // Thursday, 18 April 2019 0:00:00
        // Saturday, 20 April 2019 0:00:00
    }

    public function exportOvertime()
    {
        return view('admin.exportOvertime')
        ;
    }

    public function getcsvOvertime(Request $request)
    {
        $start = strtotime($request->start);
        $end = strtotime($request->end);

        $incidents = Incident::with([
            'event',
            'event.eventDays' => function($q){
                // $q->where('processed', '!=', 1);
            }
        ])
        ->whereHas('event', function($q) use ($start, $end){
            $q->whereBetween('start', [$start, $end])
            ->orWhereBetween('end', [$start, $end])
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '>=', $start)
                ->where('end', '<=', $end);
            })
            ->orWhere(function($q) use($start, $end){
                $q->where('start', '<=', $start)
                ->where('end', '>=', $end);
            });
        })->get();

        // dd($incidents, $request->start, $request->end);
        $vacations = [];
        foreach ($incidents as $key => $incident) {
            $currentTime = $start;
            while($currentTime <= $end){
                if($currentTime >= $incident->event->start && $currentTime <= $incident->event->end){
                    if(isset($vacations[$incident->from->id])){
                        $vacations[$incident->from->id]++;
                    }else{
                        $vacations[$incident->from->id] = 1;
                    }
                }
                $currentTime += 24*60*60;
            }
        }
        dd($vacations);
    }

    public function exportExtras(){
        return view('admin.exportExtra');
    }

    public function getExtras(Request $request){
        // dd($request->all());
        $timeStart = strtotime($request->start);
        $timeEnd = strtotime($request->end);
        $start = $request->start;
        $end = $request->end;

        /*$users = User::where(function($query) use($start, $end){
            $query->whereHas('incidents.event', function($q) use ($start, $end){
                $q->whereBetween('start', [$start, $end])
                ->orWhereBetween('end', [$start, $end])
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '>=', $start)
                    ->where('end', '<=', $end);
                })
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '<=', $start)
                    ->where('end', '>=', $end);
                });
            });
        })->get();*/

        $users = User::with([
            /*'incidents.event' => function($q) use($start, $end){
                $q->with('eventDays')
                ->whereBetween('start', [$start, $end])
                ->orWhereBetween('end', [$start, $end])
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '>=', $start)
                    ->where('end', '<=', $end);
                })
                ->orWhere(function($q) use($start, $end){
                    $q->where('start', '<=', $start)
                    ->where('end', '>=', $end);
                });
            },*/
            'incidents.event.eventDays' => function($q) use($start, $end){
                $q->whereBetween('date', [$start, $end]);
            },
            'incidents.incidentOvertime' => function($q) use($start, $end){
                $q->whereBetween('date', [$start, $end]);
            },
            // 'exchanges.applicantWorkshift' => function($q) use($start, $end){
            //     $q->whereBetween('date', [$start, $end]);
            // },
            // 'exchanges.requestedWorkshift',
            // 'exchangesWithMe.requestedWorkshift' => function($q) use($start, $end){
            //     $q->whereBetween('date', [$start, $end]);
            // },
            // 'exchangesWithMe.applicantWorkshift',
        ])->get();

        
        $extra3hourDiscounts = [
            2 => 1,
            3 => 2,
            4 => 3,
            5 => 4,
            6 => 5,
        ];

        $txt = "Empleado,Prolongacion Dobles, Prolongacion Triples, Extension Dobles, Extension Tiples\n";
        foreach ($users as $key => $user) {
            /* Por Regla de Flexfilm todos los empleados tienen 3 horas extras por cajon */
            $extra3hour = 3;
            $prolongacionJornada = 0;
            $descansoLaborado = 0;
            foreach ($user->incidents as $key => $incident) {
                if($incident->event){
                    if(isset($extra3hourDiscounts[count($incident->event->eventDays)])){
                        /* Estas horas se descuentan conforme una tablita ya definida 
                        $extra3hourDiscounts = [2 => 1,3 => 2,4 => 3,5 => 4,6 => 5,];
                        */
                        $extra3hour -= count($incident->event->eventDays);
                    }
                }
                if($incident->incidentOvertime){
                    if($incident->from->region->isDayWorkable($incident->incidentOvertime->date)){
                        $prolongacionJornada += 8;
                    }else{
                        $descansoLaborado += 8;
                    }
                }
            }
            
            if($extra3hour > 0){
                if($prolongacionJornada === $descansoLaborado){
                    $prolongacionJornada += $extra3hour;
                }else{
                    $descansoLaborado += ($prolongacionJornada == 0 && $descansoLaborado >= 0)?$extra3hour:0;
                    $prolongacionJornada += ($descansoLaborado == 0 && $prolongacionJornada >= 0)?$extra3hour:0;
                }
            }

            $prolongacionDobles = ($prolongacionJornada > 9)?9:$prolongacionJornada;
            $prolongacionTriples = ($prolongacionJornada > 9)?$prolongacionJornada-9:0;
            $descansoDobles = ($descansoLaborado > 9)?9:$descansoLaborado;
            $descansoTriples = ($descansoLaborado > 9)?$descansoLaborado-9:0;

            if($user->number == "emplo"){
                // dd($descansoLaborado, $prolongacionJornada);
            }

            $txt .= $user->number . "," . $prolongacionDobles . "," . $prolongacionTriples . "," . $descansoDobles . "," . $descansoTriples . "\n";
        }

        $currTimeStamp = strtotime(date('Y-m-d H:i:s'));
        \Storage::put($currTimeStamp . ".csv", $txt);
        
        return \Storage::download($currTimeStamp . ".csv");
    }
}

<?php

namespace App\Http\Controllers\Vacations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApproveOvertimeRequest;

use App\User;
use App\Models\Schedule;
use App\Models\Groups\Group;
use App\Models\Vacations\Incident;
use App\Models\Vacations\IncidentOvertime;

use DB;

class OvertimePleaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidents = Incident::where('benefit_id', 1)
        ->where('user_id', Auth::user()->id)
        ->with('incidentOvertime', 'incidentOvertime.schedule', 'from')
        ->get();

        $myIncidents = Incident::where('benefit_id', 1)
        ->where('from_id', Auth::user()->id)
        ->with('incidentOvertime', 'incidentOvertime.schedule', 'from')
        ->get();
        
        return view('common.time.index', compact('incidents', 'myIncidents'));
    }

    public function approve()
    {
        $routes = Auth::user()->authorizatorOfLevel(2); 
        $routes = $routes->load('authorizationRoute.group');
        $groups = [];
        foreach($routes as $route){
            $groups[] = $route->authorizationRoute->group->id;
        }
        
        $groups = Group::whereIn('id', $groups)->get();
        $users = [];
        foreach($groups as $group){
            $route = $group->authorizationRoutes()->with('usersInRoute')->where('level', 1)->has('usersInRoute')->first();
            if($route){
                foreach($route->usersInRoute as $user){
                    $users[$user->user_id] = $user->user_id;
                }
            }
        }
        $incidents = Incident::where('benefit_id', 1)
        ->whereIn('user_id', $users)
        ->where('status', 'pending')
        ->with('incidentOvertime', 'incidentOvertime.schedule', 'from')
        ->get();

        return view('common.time.approve', compact('incidents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Esto podría estar fijo (config) y no obtenerlo de la base de datos...
        // $data['config'] = $this->config;
        
        $routes = Auth::user()->authorizatorIn()
        ->whereHas('authorizationRoute', function($q) {
            $q->where('level', 1);
        })->get();

        $groups = [];
        foreach ($routes as $key => $route) {
            $groups[] = $route->authorizationRoute->group_id;
        }

        $users = User::whereIn('group_id', $groups)
        ->orderBy('firstname', 'ASC')
        ->get()->pluck('FullName','id')->toArray();
        $data['config'] = [];
        return view('common.time.create',compact('data','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $from = User::findOrFail($request->user);
        $shift = Schedule::findOrFail($request->shift);
        $time = strtotime($request->date);
        $amount = (strtotime($request->date.' '.$request->amount) - $time)/60;        
        DB::beginTransaction();
        $incident = [
            'user_id'   => $user->id,
            'from_id'   => $from->id,
            'week'      => date('W',$time),
            'time'      => $time,
            'comment'   => $request->description,
            'value'     => 0,
            'info'      => $request->date,
            'amount'    => $amount,
            'benefit_id'=> 1,
        ];
        $inci = null;
        try {
            $inci = Incident::create($incident);
        } catch (Exception $e) {
            DB::rollback();
            flash('Error al crear la Incidencia.  | ' . $e->getMessage(),'error');
            return redirect()->back();
        }
        try {
            $inci->incidentOvertime()->create([
                'date' => $request->date,
                'schedule_id' => $shift->id,
            ]);
        } catch (Exception $e) {
            DB::rollback();
            flash('Error al crear la Incidencia de tiempo Extra.  | ' . $e->getMessage(),'error');
            return redirect()->back();
        }
        DB::commit();
        flash('Solicitud creada exitosamente.','success');
        return redirect('/common/time/create');
    }

    public function approve_store(ApproveOvertimeRequest $request){
        $errors = [];
        foreach($request->requests as $key => $request){
            if(is_null($request))
                continue;
            $incident = Incident::find($key);
            try {
                $incident->status = $request;
                $incident->save();
            } catch (\Throwable $th) {
                $errors[] = 'Error al actualizar estatus del folio #' . $incident->id;
            }
        }
        return redirect()->route('common.time.approve')->withErrors($errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function changeStatus(Request $request){
        dd($request->all());
    }

    public function download($id){ 
        $incident = Incident::with('incidentOvertime','incidentOvertime.schedule', 'from', 'user')->where('id', $id)->first();

        if($incident){
            if(is_null($incident->incidentOvertime) || is_null($incident->from) || is_null($incident->user)){
                flash('Informacion incompleta para generar el reporte')->error;
                return redirect()->back();
            }
            $pdf = \PDF::loadView('pdfs.overtime_report', compact('incident'))->setPaper('a4', 'landscape');
            return $pdf->stream();
            // return view('pdfs.overtime_report', compact('incident'));
        }
        // $pdf = \PDF::loadView('pdfs.overtime_report', ['exchange' => $exchange]);
        // return $pdf->download('Reporte_TiempoExtra.pdf');
    }
}

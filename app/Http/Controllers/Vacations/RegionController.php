<?php

namespace App\Http\Controllers\Vacations;

use Illuminate\Http\Request;
use App\Http\Requests\RegionRequest;

use App\Http\Controllers\Controller;

use App\Models\User\Region;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regions = Region::all();
        return view('admin.regions.index', compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.regions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegionRequest $request)
    {
        try {
            Region::create([
                'name' => $request->name,
                'workshift' => $request->workshift,
            ]);
        } catch (\Throwable $th) {
            return redirect()->to('regions/create')->withErrors(['error', 'Fallo al crear  |  ' . $th->getMessage() ]);
        }
        flash('Guardado correctamente')->success();
        return redirect()->to('regions/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $region = Region::find($id);
        if($region){
            return view('admin.regions.edit', compact('region'));
        }else{
            return redirect()->to('regions')->withErrors(['error', 'No existe la región']);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RegionRequest $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Region::find($id)->delete();
        } catch (\Throwable $th) {
            return redirect()->to('regions')->withErrors(['error', 'Fallo al eliminar  |  ' . $th->getMessage() ]);
        }
        flash('Eliminado correctamente')->success();
        return redirect()->to('regions');
    }
}

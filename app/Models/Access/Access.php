<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Access extends Model
{
	use SoftDeletes;
    //
    
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['role_id','module_id','user_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Access belongs to Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
    	// belongsTo(RelatedModel, foreignKey = role_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Role::class);
    }

    /**
     * Access belongs to Module.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
    	// belongsTo(RelatedModel, foreignKey = module_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Module::class);
    }

    /**
     * Access belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    	// belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
    	return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
	use SoftDeletes;
    //
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['name','description'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Role has many Accesses.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accesses()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = role_id, localKey = id)
    	return $this->hasMany(Access::class);
    }
}

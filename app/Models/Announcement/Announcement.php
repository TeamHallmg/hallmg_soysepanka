<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\Region;
use Carbon\Carbon;

class Announcement extends Model
{
    //
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['title','description','announcement_type_id','link','file','starts','ends','active','approved_by_user_id','created_by_user_id','announcement_category_id','view_id','region_id'];

    /**
     * Announcement belongs to AnnouncementType.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementType()
    {
    	// belongsTo(RelatedModel, foreignKey = announcementType_id, keyOnRelatedModel = id)
    	return $this->belongsTo(AnnouncementType::class);
    }

    /**
     * Announcement belongs to ApprovedByUser.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function approvedByUser()
    {
        // belongsTo(RelatedModel, foreignKey = approvedByUser_id, keyOnRelatedModel = id)
        return $this->belongsTo(User::class,'approvedByUser_id');
    }

    /**
     * Announcement belongs to ApprovedByUser.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdByUser()
    {
        // belongsTo(RelatedModel, foreignKey = createdByUser_id, keyOnRelatedModel = id)
        return $this->belongsTo(User::class,'createdByUser_id');
    }

    /**
     * Announcement belongs to AnnouncementCategory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementCategory()
    {
        // belongsTo(RelatedModel, foreignKey = announcementCategory_id, keyOnRelatedModel = id)
        return $this->belongsTo(AnnouncementCategory::class);
    }

    /**
     * Announcement belongs to View.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function view()
    {
        // belongsTo(RelatedModel, foreignKey = view_id, keyOnRelatedModel = id)
        return $this->belongsTo(View::class);
    }

    /**
     * Announcement has many AnnouncementFormData.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcementFormData()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = announcement_id, localKey = id)
        return $this->hasMany(AnnouncementFormData::class);
    }

    /**
     * Announcement has many FormData.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formData($type = "")
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = announcement_id, localKey = id)
        $formData = $this->hasMany(AnnouncementFormData::class)->orderBy('announcement_form_id','ASC')->get();
        $data = [];
        foreach ($formData as $key => $value) {
            if($value->announcementForm->announcementAttribute->attr_view === "multi-file"){
                $json = json_decode($value->value);
                $data[$value->announcementForm->announcementAttribute->form_name . '-first']  = (count($json) > 0)?$json[0]:null;
                $data[$value->announcementForm->announcementAttribute->form_name . '-last']  = (count($json) > 1)?$json[count($json) - 1]:null;
            }
            $data[$value->announcementForm->announcementAttribute->form_name]  = $value->value;
        }
        return $data;
    }

    /**
     * Announcement belongs to Region.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        // belongsTo(RelatedModel, foreignKey = region_id, keyOnRelatedModel = id)
        return $this->belongsTo(Region::class);
    }

    /**
     * Announcement belongs to HasParentCategory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hasParentCategory()
    {
        // belongsTo(RelatedModel, foreignKey = hasParentCategory_id, keyOnRelatedModel = id)
        $cat = $this->belongsTo(AnnouncementCategory::class,'announcement_category_id')->first();
        return (is_null($cat->announcement_category_id))?false:true;
    }

    /**
     * Announcement belongs to Category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category($top = '')
    {
        if($top == 'top'){
            $cat = $this->belongsTo(AnnouncementCategory::class,'announcement_category_id')->first();            
            while($cat->announcement_category_id != null){
                $cat = $cat->parentCategory;                    
            }
            return $cat;
        }else{
            // belongsTo(RelatedModel, foreignKey = category_id, keyOnRelatedModel = id)
            return $this->belongsTo(AnnouncementCategory::class,'announcement_category_id');
        }
    }

    public static function getAnnouncements($view = null, $type = null)
    {

        $announcements =  Announcement::query();
        if(!is_null($type) && !empty($type)){
            $announcements->where('announcement_type_id',$type);
        }
        if(!is_null($view) && !empty($view)){
            $announcements->where('view_id',$view);
        }
        $announcements->orderBy('starts','DESC')
        ->where('active',1)
        ->where('starts','<=',Carbon::now())
        ->where('ends','>=',Carbon::now());
        return ($announcements->get()->count() > 0)?$announcements->get():null;
    }

    public static function getAnnouncementsGroupByType($view = null, $type = null)
    {
        $announcements = [];
        $announcement_types = AnnouncementType::all();
        foreach ($announcement_types as $key => $value) {
            if($ann = self::getAnnouncements($view, $value->id)){
                $announcements[$value->view_file] = $ann;
            }
        }
        return $announcements;
    }

    public static function getAnnouncementsToDisplay($view, $typeName){
        $type = AnnouncementType::where('name',$typeName)->first();
        $announcements = Announcement::where('view_id', $view)
        ->where('announcement_type_id', $type->id)
        ->where('active',1)
        ->where('starts','<=',Carbon::now())
        ->where('ends','>=',Carbon::now())
        ->get();
        foreach ($announcements as $key => $announcement) {
            // $announcement->{$property};
            $announcement->data = $announcement->formData();
        }    
        return [$type->view_file => $announcements, 'type' => $type, 'view' => $view];
    }

    public static function getAnnouncementsToDisplayByCategory($view, $typeName){
        $type = AnnouncementType::where('name',$typeName)->first();
        $announcements = Announcement::where('view_id', $view)
        ->where('announcement_type_id', $type->id)
        ->where('active',1)
        ->where('starts','<=',Carbon::now())
        ->where('ends','>=',Carbon::now())
        ->get();

        $group = [];
        foreach ($announcements as $key => $announcement) {
            $announcement->data = $announcement->formData();
            if(is_null($announcement->announcement_category_id)){
                $group[$announcement->announcement_category_id][] = $announcement;
            }else{
                if($announcement->hasParentCategory()){
                    $group[$announcement->category('top')->name]['sub'][$announcement->category->name][] = $announcement;
                }else{
                    $group[$announcement->category->name][$announcement->category->name] = $announcement;
                }
            }
        }
        return [$type->view_file => $group, 'type' => $type, 'view' => $view];
    }
}

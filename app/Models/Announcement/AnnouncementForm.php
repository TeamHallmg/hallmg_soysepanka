<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;

class AnnouncementForm extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['announcement_type_id', 'announcement_attribute_id','view_order','required'];

    /**
     * AnnouncementForm belongs to AnnnouncementType.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementType()
    {
    	// belongsTo(RelatedModel, foreignKey = annnouncementType_id, keyOnRelatedModel = id)
    	return $this->belongsTo(AnnouncementType::class);
    }

    /**
     * AnnouncementForm belongs to AnnouncementAttribute.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcementAttribute()
    {
    	// belongsTo(RelatedModel, foreignKey = announcementAttribute_id, keyOnRelatedModel = id)
    	return $this->belongsTo(AnnouncementAttribute::class);
    }

    /**
     * AnnouncementForm has one AnnouncementFormData.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function announcementFormData()
    {
    	// hasOne(RelatedModel, foreignKeyOnRelatedModel = announcementForm_id, localKey = id)
    	return $this->hasOne(AnnouncementFormData::class);
    }

    /**
     * Return an array with the announcement types that has form 
     * Regresa un arreglo con los anuncios que ya tienen formulario creado
     * 
     * @return array 
     */
    public static function existingForms()
    {
        return AnnouncementForm::groupBy('announcement_type_id');
    }

    /**
     * Return an array of announcements types id that doesn't have form created
     * Regresa un arreglo con las id de los tipos de anuncios que no tienen un formulario relacionado
     * 
     * @return array 
     */
    public static function missingForms()
    {   
        $existingForms = self::existingForms()->pluck('announcement_type_id')->toArray();
        return AnnouncementType::whereNotIn('id', $existingForms)->get(); 
    }

    /**
     * 
     */
    public static function getAnnouncementForm($id)
    {
        return AnnouncementForm::where('announcement_type_id',$id)->orderBy('view_order','ASC')->get();
    }


    public static function getAttr($announcement_type_id){
        $attr_ids = AnnouncementForm::where('announcement_type_id', $announcement_type_id)->pluck('announcement_attribute_id')->toArray();
        $attrs = AnnouncementAttribute::whereIn('id', $attr_ids)->get();
        return $attrs;
    }



    /**
     * AnnouncementForm has one GetData.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getData($announcement_id)
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = announcementForm_id, localKey = id)
        return $this->hasOne(AnnouncementFormData::class)->where('announcement_id', $announcement_id)->first();
    }

    public static function findAndGetID($announcement_type_id, $announcement_attribute_id){
        $val = AnnouncementForm::where('announcement_type_id', $announcement_type_id)
        ->where('announcement_attribute_id', $announcement_attribute_id)
        ->first();

        return ($val)?$val->id:null;
    }

}

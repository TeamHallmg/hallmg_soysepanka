<?php

namespace App\Models\Announcement;

use Illuminate\Database\Eloquent\Model;
use App\Models\Announcement\View;
use App\Models\Announcement\AnnouncementType;

class AnnouncementTypePerView extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'announcement_type_per_view';

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['announcement_type_id', 'view_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    public static function check($view, $type){
    	$val = AnnouncementTypePerView::where('view_id', $view)
    	->where('announcement_type_id', $type)
    	->first();
    	// dd(($val)?true:false, $view, $type);
    	return ($val)?true:false;
    }
}

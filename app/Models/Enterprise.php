<?php

namespace App\Models;

use App\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enterprise extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'enterprises';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'enterprise_code'];

    public function belong_to()
    {
        return $this->belongsTo(EnterpriseBelong::class, 'belong', 'id');
    }

    public function get_agent()
    {
        return $this->belongsTo(Employee::class, 'agent', 'id');
    }
}

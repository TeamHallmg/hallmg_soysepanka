<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Employee;

class JobPosition extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_positions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'file', 'experience', 'knowledge', 'comments', 'job_position_level_id', 'job_position_boss_id', 'area_id'];

    public function scopeBossTop($query){
        return $query->whereNull('job_position_boss_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    public function level()
    {
        return $this->belongsTo(JobPositionLevel::class, 'job_position_level_id', 'id');
    }

    public function jobs()
    {
        return $this->hasMany(JobPosition::class, 'job_position_boss_id', 'id');
    }

    public function employees() {
        return $this->hasMany(Employee::class, 'job_position_id','id');
    }

    public function deleted_reason() {
        return $this->morphMany(DeletedReason::class, 'deleted_reason');
    }
}

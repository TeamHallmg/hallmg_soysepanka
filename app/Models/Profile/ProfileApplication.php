<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileApplication extends Model
{
    protected $table = 'profile_application';

    protected $fillable = [
        'profile_id',
        'family_working',
        'family_working_name',
        'availability_travel',
        'availability_shifts',
        'availability_residence',
        'unionized',
        'unionized_name'
    ];
}

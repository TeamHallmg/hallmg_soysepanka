<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileExperience extends Model
{
    use SoftDeletes;

    protected $table = 'profile_experience';

    protected $date = ['deleted_at'];
    
    protected $fillable = [
        'profile_id',
        'job',
        'company',
        'date_begin',
        'date_end',
        'salary',
        'reason_separation',
        'activity'
    ];
}

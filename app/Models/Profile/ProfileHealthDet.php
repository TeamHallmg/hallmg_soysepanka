<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ProfileHealthDet extends Model
{
    protected $table = 'profile_health_det';

    protected $fillable = [
        'profile_health_id',
        'date',
        'file_medical'
    ];
}

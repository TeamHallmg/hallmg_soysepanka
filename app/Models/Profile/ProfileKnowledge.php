<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileKnowledge extends Model
{
    use SoftDeletes;

    protected $table = 'profile_knowledge';

    protected $date = ['deleted_at'];

    protected $fillable = [
        'profile_id',
        'knowledge_name',
        'knowledge_type'
    ];
}

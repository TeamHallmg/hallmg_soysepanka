<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileReference extends Model
{
    use SoftDeletes;

    protected $table = 'profile_reference';

    protected $date = ['deleted_at'];

    protected $fillable = [
        'profile_id',
        'reference_name',
        'reference_phone',
        'reference_time_meet',
        'reference_occupation'
    ];
}

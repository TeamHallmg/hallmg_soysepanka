<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileScholarships extends Model
{
    use SoftDeletes;

    protected $table = 'profile_scholarships';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'profile_id',
        'studio',
        'career',
        'school',
        'date_begin',
        'date_end',
        'speciality',
        'voucher',
        'file_studio',
        'type'
    ];
}

<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\Vacantes\Postulante;
use App\User;

class Profile extends Model
{
    use SoftDeletes;

    protected $table = 'profile';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = [
        'user_id',
        'application',
        'name',
        'surname_father',
        'surname_mother',
        'cellphone',
        'phone',
        'email',
        'gender',
        'date_birth',
        'rfc',
        'know_vacancy',
        'know_vacancy_other',
        'image',
        'file_cv'
    ];

    public function getFullNamePerfilAttribute(){
        return $this->name . ' ' . $this->surname_father . ' ' . $this->surname_mother;
    }

    public function getFileExists($file) {
        // return $exists = Storage::exists('public/profile/'. $file);
        return $exists = asset('/uploads/profile/'. $file);
        
    }
    
    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    public function postulantes() {
        return $this->hasMany(Postulante::class);
    }

    public function perfilEscuela() {
        return $this->hasMany(ProfileScholarships::class);
    }

    public function perfilEscuelaBasica() {
        return $this->hasMany(ProfileScholarships::class)->where('type', 'basica');
    }

    public function perfilEscuelaSuperior() {
        return $this->hasMany(ProfileScholarships::class)->where('type', 'superior');
    }

    public function perfilLenguaje() {
        return $this->hasMany(ProfileLanguage::class);
    }

    public function perfilConocimientos() {
        return $this->hasMany(ProfileKnowledge::class);
    }
    
    public function perfilExperiencia() {
        return $this->hasMany(ProfileExperience::class);
    }
    
    public function perfilReferencias() {
        return $this->hasMany(ProfileReference::class);
    }

    public function perfilSolicitud()
    {
        return $this->hasOne(ProfileAdditional::class);
    }
    // Al revés voltiado
    public function perfilAdicional() {
        return $this->hasOne(ProfileApplication::class);
    }

    public function perfilRegistro ()
    {
        return $this->hasOne(ProfileRegistry::class);
    }

    public function perfilSalud()
    {
        return $this->hasOne(ProfileHealth::class);
    }
    
    public function perfilBeneficiarios()
    {
        return $this->hasOne(ProfileBeneficiaries::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Anuncios\Announcement;

class Region extends Model
{
	use SoftDeletes;
    //
     /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * AnnouncementRegion has many Announcements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = announcementRegion_id, localKey = id)
    	return $this->hasMany(Announcement::class);
    }
}

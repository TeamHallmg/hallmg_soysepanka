<?php

namespace App\Models\Requisitions;

use Illuminate\Database\Eloquent\Model;
use App\User;

class RequiConfig extends Model
{
    protected $table = "requisitions_config";

    protected $fillable = [
        'user_id',
        'orden',
        'tipo_vacante',
        'editar_requi',
        'crea_requi_edita',
        'autoriza_requi_edita'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    public static function sigCurrent($requisition){
        if (!is_null($requisition)) {
            $autoriza = RequiConfig::select('orden')->where('tipo_vacante', $requisition->tipo_vacante)->where('user_id', $requisition->current_id)->first();
	        $sig_current = $autoriza->orden + 1;
            $autoriza = RequiConfig::where('tipo_vacante', $requisition->tipo_vacante)->where('orden', $sig_current)->first();

            return $autoriza;
        } 
        
        return null;
    }

    public static function current($tipo_vacante){
        $current = RequiConfig::where('tipo_vacante', $tipo_vacante)->orderBy('orden','ASC')->first();
        
        return $current;
    }
}

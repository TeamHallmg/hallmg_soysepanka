<?php

namespace App\Models\Requisitions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\Vacantes\Vacante;

class Requisition extends Model
{
    //use Notifiable;
    use SoftDeletes; //para el borrado logico

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $table = "requisitions";

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'solicitante_id',
        'current_id',
        'fecha_requerida',
        'fecha_elaborada',
        'fecha_autorizacion',
        'estatus_requi',
        'estatus_general',
        'confidencial',
        'tipo_vacante',
        'cantidad',
        'puesto',
        'puesto_descripcion',
        'area',
        'horario',
        'horario_descripcion',
        'edad_min',
        'edad_max',
        'edo_civil',
        'sexo',
        'objetivo',
        'rolesyresponsabilidades',
        'experiencia',
        'experiencia_anios',
        'experiencia_conocimientos',
        'experiencia_habilidades',
        'experiencia_grado_escolar',
        'experiencia_especializado',
        'sueldo',
        'ispt',
        'imss',
        'sd',
        'sdi',
        'sueldo_aut',
        'prestaciones',
        'lugar_trabajo',
        'informacion_confidencial',
        'pc_requiere',
        'sap_requiere',
        'viajar_requiere',
        'carro_requiere',
        'tel_requiere',
        'pc_descripcion',
        'sap_descripcion',
        'viajar_descripcion',
        'carro_descripcion',
        'tel_descripcion',
        'rechazo_descripcion',
        'comentarios'
    ];


    public function usuario() {
        return $this->belongsTo(User::class, 'solicitante_id', 'id')->withDefault();
    }

    public function vacante() {
        return $this->hasOne(Vacante::class, 'requisicion_id', 'id');
    }
    
	/*public function empleado() {
        return $this->belongsTo(Employee::class, 'solicitante_id', 'id');
        //1.- no0mbre del modelo a utilizar
        //2.- nombre del campo de la tabla actual que guarda el valor del id foraneo
        //3.- nombre del campo foraneo, que tiene la relacion conel campo del modelo actual
    }

    public function usuarioId() {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function nombre() {
        return $this->belongsTo('App\User', 'current_id', 'id');
    }

    public function jefeSolicitante() {
        return $this->belongsTo('App\User', 'bossjob_id', 'id');
    }
    /**
     * Requisition belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    public function currentUser()
    {
        // belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->belongsTo('App\User', 'current_id');
    }

    /**
     * Requisition has one Solicitante.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
    public function solicitante()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = requisition_id, localKey = id)
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Requisition has one .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
    public function postulados()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = requisition_id, localKey = id)
        return $this->hasOne('App\Models\VacantesInternas\Vacante', 'id_catalogo', 'id');
    }

    /**
     * Requisition belongs to Company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    public function company()
    {
        // belongsTo(RelatedModel, foreignKey = company_id, keyOnRelatedModel = id)
        return $this->belongsTo('App\Models\Companys\Company');

    }

    public function rutaAutorizacion() {
        $ruta = "";
        $user = $this->usuarioId;
        $current = $this->currentUser;

        if($this->requisitionType == 0){
            while($user->id != $user->boss_id && !Director::isDirector($user->boss_id)){
                $ruta .= $user->id.'|'.$user->first_name.'|';
                $user = $user->boss;
            }
            $ruta .= $user->id.'|'.$user->first_name.'|';
        }else{
            while($user->id != $user->boss_id){
                $ruta .= $user->id.'|'.$user->first_name.'|';
                $user = $user->boss;
            }
            $ruta .= $user->id.'|'.$user->first_name.'|';
        }
        return $ruta;
    }*/
}

<?php

namespace App\Models\Vacantes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\Vacantes\Vacante;
// use App\Models\Vacantes\Postulante;
use App\Models\Profile\Profile;

class Postulante extends Model
{
    use SoftDeletes; //para el borrado logico

    protected $table = 'vacancies_postulants';

    protected $dates = ['deleted_at'];

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'user_id',
        'vacante_id',
        'profile_id',
        'intentos_postulacion',
        'aceptacion_postulacion',
        'motivo',
        'motivo_cierre_vacante'
    ];

    public function usuario() {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

    public function vacante() {
        return $this->belongsTo(Vacante::class, 'vacante_id', 'id')->withDefault();
    }

    public function perfil() {
        return $this->belongsTo(Profile::class, 'profile_id', 'id')->withDefault();
    }
    
}

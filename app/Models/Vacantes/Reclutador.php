<?php

namespace App\Models\Vacantes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Models\Vacantes\Vacante;

class Reclutador extends Model
{
    use SoftDeletes; //para el borrado logico

    protected $table = 'vacancies_recruiters';

    protected $dates = ['deleted_at'];

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'user_id'
    ];

    public function usuario() {
        return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\ClimaOrganizacional\Period;
use App\Models\ClimaOrganizacional\PeriodUser;

use App\Models\Permission;
use App\Models\Role;
use App\Employee;
use App\Models\Profile\Profile;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id', 'first_name', 'last_name', 'email', 'password', 'role', 'active', 'external',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function periods()
    {
        return $this->belongsToMany(Period::class, 
            with(new PeriodUser)->getTable(),
            'user_id',
            'period_id')
        ->withPivot('status');
    }

    public function userPermissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_user', 'user_id', 'permision_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function getMyPermission(){
        $id = $this->id;
        $rolePermissions = Permission::whereHas('roles', function($q) use($id) {
            $q->whereHas('users', function ($q1) use($id) {
                $q1->where('users.id', $id);
            });
        })->pluck('id', 'action')->toArray();
        
        $userPermissions = Permission::whereHas('users', function($q) use($id) {
            $q->where('users.id', $id);
        })->pluck('id', 'action')->toArray();

        $permissions = array_merge($rolePermissions, $userPermissions);
        return $permissions;
    }

    public function hasRolePermission($permission){
        return $this->roles()
        ->whereHas('rolePermissions', function($q) use ($permission) {
            $q->where('action', $permission);
        })
        ->exists();
    }

    public function hasPermission($permission){
        return $this->userPermissions()
        ->where('action', $permission)
        ->exists();
    }

    public function existsProfile() {
        $id = $this->id;
        $profile = Profile::where('user_id', $id)->exists();

        if($profile) {
            return true;
        } else {
            return false;
        }
    }

    public function hasProfileImage() {
        $id = $this->id;
        $profile = Profile::where('user_id', $id)->first();
        if($profile != null)
            $fileExists = file_exists('uploads/profile/'.$profile->image);
        else
            return false;
        if(!is_null($profile) && $fileExists) {
            return true;
        } else {
            return false;
        }
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }
}

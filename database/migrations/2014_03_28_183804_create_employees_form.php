<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_form', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Nombre del campo');
            $table->string('name_show')->nullable()->comment('Nombre del campo a mostrar en la vista');
            $table->boolean('mandatory')->default(true)->comment('Verífica si el campo es obligatorio');
            $table->boolean('active')->default(true)->comment('Verífica si el campo está activo para el CRUD de usuarios');
            $table->timestamps();
            //$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementViewSequences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_view_sequences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sequence');
            $table->integer('view_id')->unsigned();
            $table->foreign('view_id')->references('id')->on('views');
            $table->integer('announcement_type_id')->unsigned();
            $table->foreign('announcement_type_id')->references('id')->on('announcement_types');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_view_sequences');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('requisitions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('solicitante_id')->unsigned();
			$table->integer('current_id')->nullable()->unsigned();

			$table->foreign('solicitante_id')->references('id')->on('users');
			$table->foreign('current_id')->references('id')->on('users');

			$table->date('fecha_requerida')->nullable();
			$table->date('fecha_elaborada')->nullable();
			$table->date('fecha_autorizacion')->nullable();
			$table->string('estatus_requi', 45)->nullable();
			$table->tinyInteger('confidencial')->nullable();
			
			$table->string('tipo_vacante',45)->nullable();
			$table->smallInteger('cantidad')->nullable();
			$table->string('puesto', 60)->nullable();
			$table->string('puesto_descripcion', 1000)->nullable();
			$table->string('area', 45)->nullable();
			$table->smallInteger('horario')->nullable(); //radio button
			$table->string('horario_descripcion', 1000)->nullable();
			$table->integer('edad_min')->nullable()->unsigned();
			$table->integer('edad_max')->nullable()->unsigned();
			$table->string('edo_civil', 45)->nullable();
			$table->string('sexo', 15)->nullable();
			
			$table->string('objetivo', 1000)->nullable();
			$table->string('rolesyresponsabilidades', 1000)->nullable();
			$table->smallInteger('experiencia')->nullable(); //radio button
			$table->string('experiencia_anios', 15)->nullable();
			$table->string('experiencia_conocimientos', 1000)->nullable();
			$table->string('experiencia_habilidades', 1000)->nullable();
			$table->string('experiencia_grado_escolar', 45)->nullable();
			$table->string('experiencia_especializado', 45)->nullable();
			
			$table->float('sueldo', 8,2)->nullable();
			$table->float('ispt', 8,2)->nullable();
			$table->float('imss', 8,2)->nullable();
			$table->float('sd', 8,2)->nullable();
			$table->float('sdi', 8,2)->nullable();
			$table->float('sueldo_aut', 8,2)->nullable();
			$table->string('prestaciones', 45)->nullable();

			$table->tinyInteger('lugar_trabajo')->nullable();
			$table->tinyInteger('informacion_confidencial')->nullable();
			$table->tinyInteger('pc_requiere')->nullable();
			$table->tinyInteger('sap_requiere')->nullable();
			$table->tinyInteger('viajar_requiere')->nullable();
			$table->tinyInteger('carro_requiere')->nullable();
			$table->tinyInteger('tel_requiere')->nullable();
			$table->string('pc_descripcion', 300)->nullable();
			$table->string('sap_descripcion', 300)->nullable();
			$table->string('viajar_descripcion', 300)->nullable();
			$table->string('carro_descripcion', 300)->nullable();
			$table->string('tel_descripcion', 300)->nullable();

			$table->string('rechazo_descripcion', 1000)->nullable();
			$table->string('comentarios', 1000)->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('requisitions');
	}
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileHealthDetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_health_det', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('profile_health_id')->unsigned();
            $table->foreign('profile_health_id')->references('id')->on('profile_health');

            $table->date('date')->nullable();
            $table->string('file_medical', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_health_det');
    }
}

<?php

use Illuminate\Database\Seeder;

class AnnouncementFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_forms')->insert([
            [
                'announcement_type_id' => 1,
                'announcement_attribute_id' => 4,
                'required' => 1,
            ],[
                'announcement_type_id' => 1,
                'announcement_attribute_id' => 3,
                'required' => 0,
            ],[
                'announcement_type_id' => 2,
                'announcement_attribute_id' => 4,
                'required' => 1,
            ],[
                'announcement_type_id' => 2,
                'announcement_attribute_id' => 3,
                'required' => 0,
            ],[
                'announcement_type_id' => 3,
                'announcement_attribute_id' => 4,
                'required' => 1,
            ],[
                'announcement_type_id' => 3,
                'announcement_attribute_id' => 3,
                'required' => 0,
            ],[
                'announcement_type_id' => 5,
                'announcement_attribute_id' => 4,
                'required' => 1,
            ],[
                'announcement_type_id' => 5,
                'announcement_attribute_id' => 3,
                'required' => 0,
            ],[
                'announcement_type_id' => 4,
                'announcement_attribute_id' => 1,
                'required' => 1,
            ],[
                'announcement_type_id' => 4,
                'announcement_attribute_id' => 2,
                'required' => 1,
            ],[
                'announcement_type_id' => 4,
                'announcement_attribute_id' => 3,
                'required' => 0,
            ],[
                'announcement_type_id' => 4,
                'announcement_attribute_id' => 4,
                'required' => 1,
            ],[
                'announcement_type_id' => 4,
                'announcement_attribute_id' => 5,
                'required' => 0,
            ],[
                'announcement_type_id' => 4,
                'announcement_attribute_id' => 6,
                'required' => 0,
            ]
        ]);
    }
}

/*,[
	'announcement_type_id' => ,
	'announcement_attribute_id' => ,
    'view_order' => 
]*/
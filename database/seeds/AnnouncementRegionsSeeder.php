<?php

use Illuminate\Database\Seeder;

class AnnouncementRegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('announcement_regions')->insert([
    		['name' => 'Zona General'],
    		['name' => 'Zona 1'],
    		['name' => 'Zona 2'],
    		['name' => 'Zona 3'],
    		['name' => 'Zona 4'],
    		['name' => 'Zona 5'],
    		['name' => 'Zona 6'],
    		['name' => 'Zona 7'],
    		['name' => 'Zona 8']
    	]);
    }
}

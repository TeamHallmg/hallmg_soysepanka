<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(ViewsSeeder::class);
        //$this->call(RegionsSeeder::class);
        $this->call(AnnouncementCategoriesSeeder::class);
        //$this->call(AnnouncementRegionsSeeder::class);
        $this->call(AnnouncementTypesSeeder::class);
        $this->call(AnnouncementAttributesSeeder::class);
        $this->call(AnnouncementFormSeeder::class);
        //$this->call(EmployeesTableSeeder::class);
        //$this->call(EmployeesFormSeeder::class);
        //$this->call(RequisitionConfigSeeder::class);
        //$this->call(EntidadesTableSeeder::class);
        //$this->call(MunicipiosTableSeeder::class);
    }
}

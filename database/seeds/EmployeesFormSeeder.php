<?php

use Illuminate\Database\Seeder;

class EmployeesFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees_form')->insert([
            [
                'name' => 'idempleado',
                'name_show' => 'Número de Empleado'
            ],
            [
                'name' => 'nombre',
                'name_show' => 'Nombre'
            ],
            [
                'name' => 'paterno',
                'name_show' => 'Paterno'
            ],
            [
                'name' => 'materno',
                'name_show' => 'Materno'
            ],
            [
                'name' => 'fuente',
                'name_show' => 'Fuente'
            ],
            [
                'name' => 'rfc',
                'name_show' => 'RFC'
            ],
            [
                'name' => 'curp',
                'name_show' => 'CURP'
            ],
            [
                'name' => 'nss',
                'name_show' => 'NSS'
            ],
            [
                'name' => 'correoempresa',
                'name_show' => 'Correo Empresarial'
            ],
            [
                'name' => 'correopersonal',
                'name_show' => 'Correo Personal'
            ],
            [
                'name' => 'nacimiento',
                'name_show' => 'Fecha de Nacimiento'
            ],
            [
                'name' => 'sexo',
                'name_show' => 'Sexo'
            ],
            [
                'name' => 'civil',
                'name_show' => 'Estado Civil'
            ],
            [
                'name' => 'telefono',
                'name_show' => 'Teléfono'
            ],
            [
                'name' => 'extension',
                'name_show' => 'Extensión'
            ],
            [
                'name' => 'celular',
                'name_show' => 'Teléfono Celular'
            ],
            [
                'name' => 'ingreso',
                'name_show' => 'Fecha de Ingreso'
            ],
            [
                'name' => 'fechapuesto',
                'name_show' => 'Fecha de Asignación de Puesto'
            ],
            [
                'name' => 'jefe',
                'name_show' => 'Jefe'
            ],
            [
                'name' => 'direccion',
                'name_show' => 'Dirección'
            ],
            [
                'name' => 'seccion',
                'name_show' => 'Sección'
            ],
            [
                'name' => 'job_position_id',
                'name_show' => 'Puesto'
            ],
            [
                'name' => 'grado',
                'name_show' => 'Grado'
            ],
            [
                'name' => 'region',
                'name_show' => 'Región'
            ],
            [
                'name' => 'sucursal',
                'name_show' => 'Sucursal'
            ],
            [
                'name' => 'enterprise_id',
                'name_show' => 'Empresa'
            ],
            [
                'name' => 'division',
                'name_show' => 'División'
            ],
            [
                'name' => 'marca',
                'name_show' => 'Marca'
            ],
            [
                'name' => 'centro',
                'name_show' => 'Centro'
            ],
            [
                'name' => 'checador',
                'name_show' => 'Checador'
            ],
            [
                'name' => 'turno',
                'name_show' => 'Turno'
            ],
            [
                'name' => 'tiponomina',
                'name_show' => 'Tipo de Nómina'
            ],
            [
                'name' => 'clavenomina',
                'name_show' => 'Clave de Nómina'
            ],
            [
                'name' => 'nombrenomina',
                'name_show' => 'Nombre de Nómina'
            ],
            [
                'name' => 'generalista',
                'name_show' => 'Generalista'
            ],
            [
                'name' => 'relacion',
                'name_show' => 'Relación'
            ],
            [
                'name' => 'contrato',
                'name_show' => 'Contrato'
            ],
            [
                'name' => 'horario',
                'name_show' => 'Horario'
            ],
            [
                'name' => 'jornada',
                'name_show' => 'Jornada'
            ],
            [
                'name' => 'calculo',
                'name_show' => 'Cálculo'
            ],
            [
                'name' => 'vacaciones',
                'name_show' => 'Vacaciones'
            ],
            [
                'name' => 'flotante',
                'name_show' => 'Flotante'
            ],
            [
                'name' => 'base',
                'name_show' => 'Base'
            ],
            [
                'name' => 'rol',
                'name_show' => 'Rol'
            ],
            [
                'name' => 'password',
                'name_show' => 'Contraseña'
            ],
            [
                'name' => 'extra1',
                'name_show' => 'extra1'
            ],
            [
                'name' => 'extra2',
                'name_show' => 'extra2'
            ],
            [
                'name' => 'extra3',
                'name_show' => 'extra3'
            ],
            [
                'name' => 'extra4',
                'name_show' => 'extra4'
            ],
            [
                'name' => 'extra5',
                'name_show' => 'extra5'
            ],
            [
                'name' => 'fecha',
                'name_show' => 'Fecha'
            ],
        ]);
    }
}

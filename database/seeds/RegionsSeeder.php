<?php

use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
    		['name' => 'Zona General'],
    		['name' => 'Zona 1'],
    		['name' => 'Zona 2']
    	]);
    }
}

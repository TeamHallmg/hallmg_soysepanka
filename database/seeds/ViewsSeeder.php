<?php

use Illuminate\Database\Seeder;

class ViewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('views')->insert([
            ['name' => 'example'],
            ['name' => 'home'],
            ['name' => 'eventos'],
            ['name' => 'tablero'],
            ['name' => 'mistica-guia'],
        ]);
    }
}


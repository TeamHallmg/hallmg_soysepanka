<div class="form-group">
    @if(!is_null($data))
        @if($attr->form_name == 'image')
            <img src="{{asset('/img/announcements/'.$data)}}" alt="" class="img-fluid" style="max-height: 300px">
        @elseif($attr->form_name == 'document')
            <a href="{{asset('/img/announcements/'.$data)}}" download>{{ $data }}</a>
        
        @endif
    @endif
    <label for="{{$attr->form_name}}">{{$attr->name}}</label>
    <input class="form-control" name="{{$attr->form_name}}[]" value="{{is_null($data)?$required:''}}" type="file" multiple>
</div>
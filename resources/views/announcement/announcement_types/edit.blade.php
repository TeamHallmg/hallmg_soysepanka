@extends('announcement.app')

@section('title', 'Editar Tipo de Anuncio')

@section('content')
<div class="card card-2">
	<div class="card-header">
		<h1>Editar Tipo de Anuncio: {{$announcement_type->name}}</h1>
	</div>
	<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<form method="POST" action="{{ route('announcement_types.update',$announcement_type->id) }}">
						{!! method_field('PUT') !!}
						{!! csrf_field() !!}	
						<div class="form-group col-12 col-md-12">
							<label for="name" class="col-form-label font-weight-bold">Nombre del Puesto:</label>
							<input type="text" class="form-control" name="name" id="name" value="{{$announcement_type->name}}" required>
						</div>
						<div class="form-group col-12 col-md-12">
							<label for="show_name" class="col-form-label font-weight-bold">Nombre a mostrar:</label>
							<input type="text" class="form-control" name="show_name" id="show_name" value="{{$announcement_type->show_name}}" required>
						</div>
						<div class="form-group col-12 col-md-12">
							<label for="view_file" class="col-form-label font-weight-bold">Archivo de la vista:</label>
							<input type="text" class="form-control" name="view_file" id="view_file" value="{{$announcement_type->view_file}}" required>
							<small id="passwordHelpBlock" class="form-text text-muted">
								Este Archivo es el blade que se sube manualmente y da formato a la vista del tipo de anuncio.
							</small>
						</div>
						<div class="form-group col-12 col-md-12">
							<label for="max_quantity" class="col-form-label font-weight-bold">Número máximo de anuncios de este tipo a mostrar:</label>
							<input type="number" class="form-control" name="max_quantity_stored" id="max_quantity" value="{{$announcement_type->max_quantity_stored}}" required>
						</div>
						<div class="form-group col-12 col-md-12">
							<button type="submit" class="btn btn-success card-1">Guardar Cambios</button>
						</div>
					</form>
				</div>
			</div>
	</div>
	<div class="card-footer text-muted">
	</div>
</div>

<div class="container">
	<div class="row my-5">
		<div class="col text-center">
			
		</div>
	</div>

</div>
@endsection
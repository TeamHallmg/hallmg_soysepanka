<div id="carouselExampleIndicators{{$type}}" class="carousel slide mx-auto row card-2" data-ride="carousel">
  <ol class="carousel-indicators">
    @foreach($announcements as $key => $announcement)
      <li data-target="#carouselExampleIndicators{{$type}}" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
    @endforeach
  </ol>
  <div class="carousel-inner" role="listbox">
 
    @foreach ($announcements as $key => $announcement)
      <div class="carousel-item {{ $loop->first ? 'active' : '' }}" >
        <img class="w-100" src="img/announcements/{{$announcement->data['image']}}" alt="{{ isset($announcement->data['title'])?$announcement->data['title']:'' }}">
        <div class="carousel-caption d-none d-md-block d-sm-block">
        </div>
      </div>
    @endforeach
  </div>
  
   <!-- Controls -->
  @if(count($announcements) > 1)
  <a class="carousel-control-prev" href="#carouselExampleIndicators{{$type}}" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators{{$type}}" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  @endif
</div>
{{-- {{dd($announcements)}} --}}
@foreach($announcements as $key => $categories)
<h2 class="font-weight-bold box-title mb-5">{{$key}}</h2>
@if(empty($key))
    @foreach($categories as $key2 => $announcement)
    <div class="accordion" id="accordion{{$announcement->id}}">
        <div class="card mb-3">
            <div class="card-header mark-header" id="headingOne">
                <div class="row">
                    <div class="col-10">
                        <h5 class="my-2">
                            {{$announcement->data['title']}}
                        </h5>
                    </div>
                    <div class="col-2 text-right">
                        <button class="btn btn-blue" type="button" data-toggle="collapse" data-target="#collapse{{$announcement->id}}" aria-expanded="true" aria-controls="collapse{{$announcement->id}}">
                        +
                        </button>
                    </div>
                </div>
            </div>
            <div id="collapse{{$announcement->id}}" class="collapse " aria-labelledby="headingOne" data-parent="#accordion{{$announcement->id}}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-10 col-md-10">
                            {{$announcement->data['description']}}
                        </div>
                        <div class="col-2 col-md-2">
                            <a target="_blank" href="documents/{{$announcement->data['document']}}" class="btn btn-pink">PDF <i class="fas fa-file-download ml-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@else
    <div class="accordion" id="accordion{{$categories[$key]->id}}">
        <div class="card mb-3">
            <div class="card-header mark-header" id="headingOne">
                <div class="row">
                    <div class="col-10">
                        <h5 class="my-2">
                            {{$categories[$key]->data['title']}}
                        </h5>
                    </div>
                    <div class="col-2 text-right">
                        <button class="btn btn-blue" type="button" data-toggle="collapse" data-target="#collapse{{$categories[$key]->id}}" aria-expanded="true" aria-controls="collapse{{$categories[$key]->id}}">
                        +
                        </button>
                    </div>
                </div>
            </div>
            <div id="collapse{{$categories[$key]->id}}" class="collapse " aria-labelledby="headingOne" data-parent="#accordion{{$categories[$key]->id}}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            @foreach($categories['sub'] as $keysub => $ann)
                                @foreach($ann as $item)
                                <h2 class="font-weight-bold box-title mb-5">{{$keysub}}</h2>
                                <div class="accordion" id="accordion{{$item->id}}">
                                    <div class="card mb-3">
                                        <div class="card-header mark-header" id="headingOne">
                                            <div class="row">
                                                <div class="col-10">
                                                    <h5 class="my-2">
                                                        {{$item->data['title']}}
                                                    </h5>
                                                </div>
                                                <div class="col-2 text-right">
                                                    <button class="btn btn-blue" type="button" data-toggle="collapse" data-target="#collapse{{$item->id}}" aria-expanded="true" aria-controls="collapse{{$item->id}}">
                                                    +
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse{{$item->id}}" class="collapse " aria-labelledby="headingOne" data-parent="#accordion{{$item->id}}">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-10 col-md-10">
                                                        {{$item->data['description']}}
                                                    </div>
                                                    <div class="col-2 col-md-2">
                                                        <a target="_blank" href="documents/{{$item->data['document']}}" class="btn btn-pink">PDF <i class="fas fa-file-download ml-2"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endforeach
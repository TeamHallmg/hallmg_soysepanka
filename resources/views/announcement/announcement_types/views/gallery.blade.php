<div class="row">
@foreach($announcements->sortByDesc('created_at') as $announcement)
    <div class="col-md-6 encont my-4">
        <div class="container-fluid border-gold-gallery card-1 p-3">
            <div class="row">
                <div class="col-md-4">
                    {{--dd($announcement)--}}
                    @if(isset($announcement->data['gallery']))
                        @if(isset($announcement->data['gallery-first']) && !is_null($announcement->data['gallery-first']) )
                            <div>
                                <img style="max-height: 140px" class="h-100 w-100 rounded" src="/img/announcements/{{ $announcement->data['gallery-first'] }}" alt="">
                            </div>
                        @endif
                        <div>
                            <button type="button" class="btn btn-dark-blue col-12 mt-3 card-1 font-weight-bold" data-toggle="modal" data-target="#exampleModal" data-name="{{ trim($announcement->data['gallery'], "[]" )}}">
                                Ver Galería
                            </button>
                        </div>
                    @endif
                </div>
                <div class="col-8">
                    <div class="txtcolor-blue-gallery font-weight-bolder font-size-1">Nombre del Evento:</div>
                    @if(isset($announcement->data['title']))
                        <div for="">{{ $announcement->data['title'] }}</div>
                    @endif
                    <div class="txtcolor-blue-gallery font-weight-bolder font-size-1">Fecha:</div>
                    @if(isset($announcement->data['date']))
                        <div for="">{{ $announcement->data['date'] }}</div>
                    @endif
                    <div class="txtcolor-blue-gallery font-weight-bolder font-size-1">Lugar:</div>
                    @if(isset($announcement->data['place']))
                        <div for="">{{ $announcement->data['place'] }}</div>
                    @endif
                </div>
            </div>
            
            @if(isset($announcement->data['link']))
                <a href="http://{{$announcement->data['link']}}">
            @endif
            @if(isset($announcement->data['image']))
                <a href="{{ url('eventos') }}">
                    <img class="mx-auto d-block" src="/img/announcements/{{$announcement->data['image']}}" alt="">
                </a>
            @endif
            @if(isset($announcement->data['link']))
                </a>
            @endif
        </div>
    </div>
@endforeach
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body animated fadeIn delay-1">
                <div class="container-fluid">
                    <div class="row">
                        <div class="slider-for col-12 col-md-12">
                            
                        </div>
                    </div>
                    <div class="row py-4">
                        <div class="slider-nav col-12 col-md-12">
                                
                        </div>
                    </div>
                </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
	.img-mosaico{
		padding: 0px;
		margin: 0px;
		width: 100%;
	}
</style>

@php
	$max = 3;
@endphp
<div class="container-fluid my-5">
	<div class="row">
	@foreach($announcements as $key => $announcement)
		{{--@if ($loop->index % $max === 0)
			
		@endif--}}
			<div class="col-4 px-0">
				@if(isset($announcement->data['link']))
					<a href="http://{{$announcement->data['link']}}">
				@endif
					<img class="img-mosaico img-fluid" src="{{ asset('/img/announcements/'.$announcement->data['image']) }}">
				@if(isset($announcement->data['link']))
					</a>
				@endif
			</div>
		{{--@if ($loop->iteration % $max === 0 && $loop->index !== 0 || $loop->last)
			
		@endif--}}
	@endforeach
	</div>
</div>

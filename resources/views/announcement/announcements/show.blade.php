@extends('announcement.app')

@section('title', 'Detalles Anuncio')

@section('content')
<div class="row margin-top-20">
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="text-center">
				<h3 class="text-blue">{{ $announcement->title }}</h3>
				<br>
				<p>{{ $announcement->description }}</p>
				{{--
				<p>
					<a href="{{ $announcement->link }}" target="_blank">{{ $announcement->link }}</a>
				</p>
				--}}
				<br>
				@if (strpos($announcement->image, '.mp4') !== false)
					<video width="100%" controls>
						<source src="{{ asset('/img/announcements/' . $announcement->image) }}" type="video/mp4">

						Tu navegador no implementa el elemento <code>video</code>.
					</video>
				@else
					<p>
						<img class="img-responsive center" src="{{ asset('/img/announcements/' . $announcement->image) }}" alt="{{ $announcement->image }}">
					</p>
				@endif
				<br>
				<a class="btn btn-success" href="{{ URL::previous() }}">Regresar</a>
			</div>
		</div>
	</div>
</div>
@endsection
@if(Auth::user()->hasAccessTo('anuncios'))
	<div>
		<a class="btn btn-primary" href="{{url('announcements?view='.$view->id.'&type=all')}}">Administrar Anuncios</a>
	</div>
@endif
@foreach($announcement_types as $key => $announcements)
	<a class="btn btn-primary" href="{{url('announcements?view='.$view->id.'&type='.$announcements[0]->announcement_type_id)}}">Administrar {{$announcements[0]->announcementType->name}}</a>
	<div class="container">
		@include('announcement.announcement_types.views.'.$key)
	</div>
@endforeach
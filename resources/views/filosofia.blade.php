@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {{-- <img class="img-fluid" src="/img/mosaico.png" alt=""> --}}
            @foreach ($display_announcements as $announcement)
				@include('announcement.announcement_types.displays.display_announcement', ['announcements' => $announcement])
			@endforeach
        </div>
    </div>
</div>
@endsection

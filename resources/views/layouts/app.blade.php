<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="bg-white">
    <div id="app">
        <header class="bg-white">   
            <div class="container-fluid py-2 px-5">
                <div class="row d-flex justify-content-around align-content-center">
                    <div>
                        <a href="{{ url('home') }}"><img class="img-fluid" src="{{ asset('img/logo login.png') }}" style="max-height: 150px;" alt=""></a>
                    </div>
                    <div class="my-auto">
                        {{-- <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                            <img class="img-fluid" src="/img/user.png" alt="">
                        </a> --}}
                        <div class="row">
                            <div class="col-md-8 text-right mt-4">
                                <h5><strong>Bienvenido</strong></h5>
                                <div class="dropdown">
                                    <a class="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="align-middle mr-2"><i class="fas fa-th text-danger fa-2x"></i></span> <strong>{{ Auth::user()->getFullNameAttribute() }}</strong>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                        <a href="{{url('home')}}" class="dropdown-item">Módulo Cómunicación</a>
                                        <a href="#" class="dropdown-item not-allowed">Módulo Vacantes</a>
                                        {{-- <button class="dropdown-item disabled text-dark not-allowed" style="background: #CCC" type="button">Módulo Incidencias</button> --}}
                                    </div>
                                </div>
                                <a class="text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-power-off"></i> Cerrar sesión</a>
                            </div>
                            <div class="col-md-4">
                                @if(Auth::user()->hasProfileImage())
                                    <img src="{{ asset('uploads/profile/'.Auth::user()->profile->image) }}" alt="..." class="rounded-circle img-fluid" style="height:120px;">
                                    {{-- <img src="{{ asset('img/vacantes/sinimagen.png') }}" alt="..." class="rounded-circle img-fluid"> --}}
                                @else
                                    <img src="{{ asset('img/vacantes/sinimagen.png') }}" alt="..." class="rounded-circle img-fluid">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel">
            <div class="container">
                {{--<a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>--}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-white" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav nav-fill w-100">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Nuestra Empresa <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdown">
                                <a href="{{ route('mision') }}" class="dropdown-item">Misión</a>
                                <a href="{{ route('vision') }}" class="dropdown-item">Visión</a>
                                <a href="{{ route('valores') }}" class="dropdown-item">Valores</a>
                                <a href="{{ route('historia') }}" class="dropdown-item">Historia</a>
                                <a href="{{ route('organigrama') }}" class="dropdown-item">Organigrama</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Procedimientos / Políticas <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdown">
                                <a href="#" class="dropdown-item not-allowed">En espera de Matriz</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Recursos Humanos <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-dark" href="{{ route('directorio') }}">Directorio</a>
                                <a class="dropdown-item text-dark not-allowed" href="#">Eventos</a>
                                <a class="dropdown-item text-dark not-allowed" href="#">Cumpleaños</a>
                                <li class="dropdown-submenu">
                                    <a class="text-decoration-none text-dark dropdown-item" href="#">Beneficios MAVER</a>
                                    <ul class="dropdown-menu">
                                        <a class="text-decoration-none text-dark" tabindex="-1" href="{{ route('prestaciones') }}"><li class="dropdown-item">Prestaciones</li></a>
                                        <a class="text-decoration-none text-dark" tabindex="-1" href="{{ route('convenios') }}"><li class="dropdown-item">Convenios</li></a>
                                    </ul>
                                </li>
                                <a class="dropdown-item text-dark not-allowed" href="#">Estacionamiento</a>
                                <a class="dropdown-item text-dark not-allowed" href="#">Promociones Internas</a>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Línea Ética <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a href="#" class="dropdown-item not-allowed">Código de Ética</a>
                                <a href="#" class="dropdown-item not-allowed">Denuncia</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                Aplicaciones RH <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-dark not-allowed" href="#">Universidad MAVER</a>
                                <a class="dropdown-item text-dark not-allowed" href="#">Kisko</a>
                                <li class="dropdown-submenu">
                                    <a class="text-decoration-none text-dark dropdown-item" href="#">Sepanka Suite</a>
                                    <ul class="dropdown-menu">
                                        <a class="text-decoration-none text-dark not-allowed" tabindex="-1" href="#"><li class="dropdown-item">Requisiciones</li></a>
                                        <a class="text-decoration-none text-dark not-allowed" tabindex="-1" href="#"><li class="dropdown-item">Vacantes</li></a>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @auth
                            {{--<li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fas fa-toolbox"></i> Módulos <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> HeadCount & Organigrama</a>
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> Gestión de Vacantes</a>
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> Selección y Reclutamiento</a>
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> Vacaciones</a>
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> Módulo de Vacantes</a>
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> Módulo de Incidencias</a>
                                    <a href="#" class="dropdown-item"><i class="fas fa-box"></i> Módulo de Comunicación Interna</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('admin-de-usuarios') }}"><i class="fas fa-cogs"></i> Admininstración de Usuarios</a>
                            </li>--}}
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{asset('login')}}">Iniciar Sesión</a>
                            </li>
                        @else
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    @include('flash::message')
                </div>
            </div>

            @if(View::hasSection('sidebar'))
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            @yield('sidebar')
                        </div>
                        <div class="col-md-10">
                            @yield('content')
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-10 offset-md-1">
                    {{-- @yield('mainContent') --}}
                    @yield('content')
                </div>
            @endif
            
        </main>
    </div>
</body>
<footer class="pb-5 pt-2 bg-dark">
    <div class="container">
        <div class="row d-flex justify-content-center align-content-start">
            <div class="text-white text-center">
                Maver
            </div>
        </div>
    </div>
</footer>
</html>

{{-- @yield('mainScripts') --}}
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    });
</script>

@yield('scripts')
<div class="container-fluid">
    <h5>FICHA DE BENEFICIARIO:</h5>
    <div class="form-row">
        <div class="col-12 col-md-3 text-center">
            @if (isset($profile) && !is_null($profile->image) && $fileExists)
                <img src="{{ asset('storage/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
            @else
                <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
            @endif
        </div>
        <div class="col-9">
            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="" class="requerido">Número de empleado</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->idempleado : null : null }}" disabled>
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="name" class="requerido">Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ isset($profile) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $user->first_name }}" disabled={{ is_null($user) ? false : true }}>
                </div>
                
                <div class="form-group col">
                    <label for="surname_father" class="requerido">Apellido Paterno</label>
                    <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ isset($profile) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $user->last_name }}" disabled={{ is_null($user) ? false : true }}>
                </div>

                <div class="form-group col">
                    <label for="surname_mother" class="requerido">Apellido Materno</label>
                    <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ isset($profile) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col"></div>
                    
                <div class="form-group col"></div>
    
                <div class="form-group col">
                    <label for="" class="requerido">Estado Civil:</label>
                    <input type="text" name="" id="" class="form-control" value="{{  !is_null($user) ? !is_null($user->employee) ? $user->employee->civil : null : null  }}" disabled>
                </div>
            </div>
        </div>
    </div>

    
    <h5>CÓNYUGE</h5>
    @if(is_null($profileBeneficiarios))
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="spouse_name">Nombre:</label>
                <input class="form-control" type="text" name="spouse_name" id="spouse_name">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="date_marriage">Fecha de matrimonio:</label>
                <input class="form-control" type="date" name="date_marriage" id="date_marriage">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_marriage" id="file_marriage">
                    <label class="custom-file-label" for="file_marriage" data-browse="Buscar"></label>
                </div>
            </div>
        </div>

        <h5 class="mt-4">HIJOS</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_beneficiarios_hijos">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        <div id="beneficiario_s">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-4">
                    <label for="beneficiarie_name">Nombre:</label>
                    <input class="form-control" type="text" name="son[beneficiarie_name][]" id="beneficiarie_name">
                </div>
                <div class="col-12 mb-3 col-md-4">
                    <label for="date_birth_beneficiarie">Fecha de nacimiento:</label>
                    <input class="form-control" type="date" name="son[date_birth_beneficiarie][]" id="date_birth_beneficiarie">
                </div>
                <div class="col-12 mb-3 col-md-3">
                    <label for="">Subir Acta:</label>
                    <div class="custom-file col">
                        <input class="form-control-file custom-file-input" type="file" name="son[file_birth_certificate_beneficiarie][]" id="file_birth_certificate_beneficiarie">
                        <label class="custom-file-label" for="file_birth_certificate_beneficiarie" data-browse="Buscar"></label>
                    </div>
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_beneficiarios_hijos">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        </div>

        <div id="destino_beneficiarios_hijos"></div>
        

        <h5 class="mt-4">PADRES</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="father_name">Nombre del padre:</label>
                <input class="form-control" type="text" name="father_name" id="father_name">
            </div>

            <div class="col-12 mb-3 col-md-4">
                <label for="date_birth_father">Fecha de nacimiento:</label>
                <input class="form-control" type="date" name="date_birth_father" id="date_birth_father">
            </div>

            <div class="col-12 mb-3 col-md-4">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_birth_certificate_father" id="file_birth_certificate_father">
                    <label class="custom-file-label" for="file_birth_certificate_father" data-browse="Buscar"></label>
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="mother_name">Nombre de la madre:</label>
                <input class="form-control" type="text" name="mother_name" id="mother_name">
            </div>

            <div class="col-12 mb-3 col-md-4">
                <label for="date_birth_mother">Fecha de nacimiento:</label>
                <input class="form-control" type="date" name="date_birth_mother" id="date_birth_mother">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_birth_certificate_mother" id="file_birth_certificate_mother">
                    <label class="custom-file-label" for="file_birth_certificate_mother" data-browse="Buscar"></label>
                </div>
            </div>
        </div>

        <h5 class="mt-4">OTRO</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="beneficiarie_name">Nombre:</label>
                <input class="form-control" type="text" name="other[beneficiarie_name]" id="beneficiarie_name">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="date_birth_beneficiarie">Fecha de nacimiento:</label>
                <input class="form-control" type="date" name="other[date_birth_beneficiarie]" id="date_birth_beneficiarie">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="other[file_birth_certificate_beneficiarie]" id="file_birth_certificate_beneficiarie">
                    <label class="custom-file-label" for="file_birth_certificate_beneficiarie" data-browse="Buscar"></label>
                </div>
            </div>
        </div>
    @else
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="spouse_name">Nombre:</label>
                <input class="form-control" type="text" name="spouse_name" id="spouse_name" value="{{ $profileBeneficiarios->spouse_name }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="date_marriage">Fecha de matrimonio:</label>
                <input class="form-control" type="date" name="date_marriage" id="date_marriage" value="{{ $profileBeneficiarios->date_marriage }}">
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_marriage" id="file_marriage">
                    <label class="custom-file-label" for="file_marriage" data-browse="Buscar"></label>
                </div>
            </div>
            @if (!is_null($profileBeneficiarios->file_marriage))
                @if ($profile->getFileExists($profileBeneficiarios->file_marriage))
                    <div class="col-12 mb-3 col-md-1 mt-4">
                        <a class="btn morado" href="{{ asset('/storage/profile/'. $profileBeneficiarios->file_marriage) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    </div>
                @endif
            @endif
        </div>

        <h5 class="mt-4">HIJOS</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_beneficiarios_hijos">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        @forelse (isset($profileBeneficiarios) ? $profileBeneficiarios->perfilBeneficiariosDetHijo : [] as $det )
            <div id="beneficiario_s">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-4">
                        <label for="beneficiarie_name">Nombre:</label>
                        <input class="form-control" type="text" name="son[beneficiarie_name][]" id="beneficiarie_name" value="{{ $det->beneficiarie_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_birth_beneficiarie">Fecha de nacimiento:</label>
                        <input class="form-control" type="date" name="son[date_birth_beneficiarie][]" id="date_birth_beneficiarie" value="{{ $det->date_birth_beneficiarie }}">
                    </div>
                    @if (!is_null($det->file_birth_certificate_beneficiarie))
                        <div class="col-12 mb-3 col-md-3">
                    @else
                        <div class="col-12 mb-3 col-md-4">
                    @endif
                        <label for="">Subir Acta:</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="son[file_birth_certificate_beneficiarie][]" id="file_birth_certificate_beneficiarie">
                            <label class="custom-file-label" for="file_birth_certificate_beneficiarie" data-browse="Buscar"></label>
                        </div>
                    </div>
                    @if (!is_null($det->file_birth_certificate_beneficiarie))
                        @if ($profile->getFileExists($det->file_birth_certificate_beneficiarie))
                            <div class="col-12 mb-3 col-md-1 mt-4">
                                <a class="btn morado" href="{{ asset('/storage/profile/'. $det->file_birth_certificate_beneficiarie) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            </div>
                        @endif
                    @endif
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_beneficiarios_hijos">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="beneficiario_s">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-4">
                        <label for="beneficiarie_name">Nombre:</label>
                        <input class="form-control" type="text" name="son[beneficiarie_name][]" id="beneficiarie_name">
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="date_birth_beneficiarie">Fecha de nacimiento:</label>
                        <input class="form-control" type="date" name="son[date_birth_beneficiarie][]" id="date_birth_beneficiarie">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="">Subir Acta:</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="son[file_birth_certificate_beneficiarie][]" id="file_birth_certificate_beneficiarie">
                            <label class="custom-file-label" for="file_birth_certificate_beneficiarie" data-browse="Buscar"></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_beneficiarios_hijos">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_beneficiarios_hijos"></div>

        <h5 class="mt-4">PADRES</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="father_name">Nombre del padre:</label>
                <input class="form-control" type="text" name="father_name" id="father_name" value="{{ $profileBeneficiarios->father_name }}">
            </div>

            <div class="col-12 mb-3 col-md-4">
                <label for="date_birth_father">Fecha de nacimiento:</label>
                <input class="form-control" type="date" name="date_birth_father" id="date_birth_father" value="{{ $profileBeneficiarios->date_birth_father }}">
            </div>

            <div class="col-12 mb-3 col-md-3">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_birth_certificate_father" id="file_birth_certificate_father">
                    <label class="custom-file-label" for="file_birth_certificate_father" data-browse="Buscar"></label>
                </div>
            </div>

            @if (!is_null($profileBeneficiarios->file_birth_certificate_father))
                @if ($profile->getFileExists($profileBeneficiarios->file_birth_certificate_father))
                    <div class="col-12 mb-3 col-md-1 mt-4">
                        <a class="btn morado" href="{{ asset('/storage/profile/'. $profileBeneficiarios->file_birth_certificate_father) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    </div>
                @endif
            @endif
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="mother_name">Nombre de la madre:</label>
                <input class="form-control" type="text" name="mother_name" id="mother_name" value="{{ $profileBeneficiarios->mother_name }}">
            </div>

            <div class="col-12 mb-3 col-md-4">
                <label for="date_birth_mother">Fecha de nacimiento:</label>
                <input class="form-control" type="date" name="date_birth_mother" id="date_birth_mother" value="{{ $profileBeneficiarios->date_birth_mother }}">
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_birth_certificate_mother" id="file_birth_certificate_mother">
                    <label class="custom-file-label" for="file_birth_certificate_mother" data-browse="Buscar"></label>
                </div>
            </div>
            @if (!is_null($profileBeneficiarios->file_birth_certificate_mother))
                @if ($profile->getFileExists($profileBeneficiarios->file_birth_certificate_mother))
                    <div class="col-12 mb-3 col-md-1 mt-4">
                        <a class="btn morado" href="{{ asset('/storage/profile/'. $profileBeneficiarios->file_birth_certificate_mother) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    </div>
                @endif
            @endif
        </div>

        <h5 class="mt-4">OTRO</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="beneficiarie_name">Nombre:</label>
                <input class="form-control" type="text" name="other[beneficiarie_name]" id="beneficiarie_name" value="{{ $profileBeneficiarios->perfilBeneficiariosDetOtro->beneficiarie_name }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="date_birth_beneficiarie">Fecha de nacimiento:</label>
                <input class="form-control" type="date" name="other[date_birth_beneficiarie]" id="date_birth_beneficiarie" value="{{ $profileBeneficiarios->perfilBeneficiariosDetOtro->date_birth_beneficiarie }}">
            </div>
            <div class="col-12 mb-3 col-md-3">
                <label for="">Subir Acta:</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="other[file_birth_certificate_beneficiarie]" id="file_birth_certificate_beneficiarie">
                    <label class="custom-file-label" for="file_birth_certificate_beneficiarie" data-browse="Buscar"></label>
                </div>
            </div>
            @if (!is_null($profileBeneficiarios->perfilBeneficiariosDetOtro->file_birth_certificate_beneficiarie))
                @if ($profile->getFileExists($profileBeneficiarios->perfilBeneficiariosDetOtro->file_birth_certificate_beneficiarie))
                    <div class="col-12 mb-3 col-md-1 mt-4">
                        <a class="btn morado" href="{{ asset('/storage/profile/'. $profileBeneficiarios->perfilBeneficiariosDetOtro->file_birth_certificate_beneficiarie) }}" target="_blank">
                            <i class="fa fa-file-alt"></i> Ver
                        </a>
                    </div>
                @endif
            @endif
        </div>     
    @endif

    <div class="row mt-3">
        <div class="col text-right">
            <button type="submit" class="btn morado">{{ is_null($profileBeneficiarios) ? 'Guardar' : 'Actualizar' }}</button>
        </div>
    </div>
</div>
    

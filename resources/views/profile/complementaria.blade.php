<div class="container-fluid">
        @if(!is_null($profile))
        <h5>Datos Generales</h5>

            <div class="form-row">
                <div class="col-12 col-md-3 text-center">
                    @if (isset($profile) && !is_null($profile->image) && $fileExists)
                        <img src="{{ asset('uploads/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
                    @else
                        <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
                    @endif
                    <div class="custom-file form-group col">                
                        <input type="file" name="file[image]" class="form-control custom-file-input" id="image" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="image" data-browse="Actualizar"></label>
                    </div>
                </div>
                <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                    <label for="name">Nombre(s)</label>
                    <input class="form-control mb-3" type="text" name="name" id="name" value="{{ isset($profile) ? $profile->name : null}}" disabled>
                    <label for="cellphone">Número de Celular</label>
                    <input class="form-control mb-3" type="text" name="cellphone" id="cellphone" value="{{ isset($profile) ? $profile->cellphone : null}}" disabled>
                    <label for="gender">Sexo</label>
                    <input class="form-control mb-3" type="text" name="gender" id="gender" value="{{ isset($profile) ? $profile->gender : null}}" disabled>
                </div>
                <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
                    <label for="surname_father">Apellido Paterno</label>
                    <input class="form-control mb-3" type="text" name="surname_father" id="surname_father" value="{{ isset($profile) ? $profile->surname_father : null}}" disabled>
                    <label for="phone">Número de Teléfono Fijo</label>
                    <input class="form-control mb-3" type="text" name="phone" id="phone" value="{{ isset($profile) ? $profile->phone : null}}" disabled>
                    <label for="date_birth">Fecha de Nacimiento</label>
                    {{-- <input class="form-control mb-3" type="text" name="date_birth" id="date_birth" value="{{ isset($profile) ? Carbon\Carbon::createFromFormat('Y-m-d', $profile->date_birth)->format('d-m-Y') : null }}" disabled> --}}
                    <input class="form-control mb-3" type="text" name="date_birth" id="date_birth" value="{{ isset($profile) ? $profile->date_birth : null }}" disabled>
                </div>
                <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
                    <label for="surname_mother">Apellido Materno</label>
                    <input class="form-control mb-3" type="text" name="surname_mother" id="surname_mother" value="{{ isset($profile) ? $profile->surname_mother : null}}" disabled>
                    <label for="lname">Correo Eléctronico</label>
                    <input class="form-control mb-3" type="text" name="email" id="email" value="{{ isset($profile) ? $profile->email : null}}" disabled>
                    <label for="rfc">NIT</label>
                    <input class="form-control mb-3" type="text" name="rfc" id="rfc" value="{{ isset($profile) ? $profile->rfc : null}}" disabled>
                </div>
            </div>
            <br><br>
        @endif
        
        <h5>Domicilio Actual</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="address">Calle</label>
                <input class="form-control mb-3" type="text" name="address" id="address_c" value="{{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->address : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_outside">No. Exterior</label>
                <input class="form-control mb-3" type="text" name="num_outside" id="num_outside_c" value="{{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->num_outside : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="num_inside">No. Interior</label>
                <input class="form-control mb-3" type="text" name="num_inside" id="num_inside_c" value="{{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->num_inside : null }}">
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="colony">Colonia</label>
                <input class="form-control mb-3" type="text" name="colony" id="colony_c" value="{{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->colony : null }}">
            </div>
        </div>
            
        <div class="form-row">
            <div class="col-12 mb-3 col-md-2">
                <label for="state">Estado</label>
                <select name="state" id="state_c" class="selectpicker form-control mb-3" data-live-search="true">
                    @foreach ($estados as $id => $estado)
                        @isset($profile->perfilSolicitud->state)
                            <option value="{{ $id }}" {{ ($id==$profile->perfilSolicitud->estado->id) ? 'selected' : null }}> {{ $estado }}</option>
                        @else
                            <option disabled value="" selected hidden>Debe seleccionar un Estado...</option>
                            <option value="{{ $id }}"> {{ $estado }}</option>
                        @endisset
                    @endforeach
                </select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="city">Ciudad</label>
                <select class="selectpicker form-control mb-3" name="city" id="city_c" data-live-search="true">
                    @isset($profile->perfilSolicitud)
                        @foreach ($municipios as $id => $municipio)
                            <option value="{{ $id }}" {{ ($id==$profile->perfilSolicitud->municipio->id) ? 'selected' : null }}> {{ $municipio }}</option>
                        @endforeach
                    @else
                        <option disabled value="" selected hidden>Debe seleccionar un Estado...</option>
                    @endisset
                </select>
            </div>
            <div class="col-12 mb-3 col-md-2">
                <label for="zip_code">Código Postal</label>
                <input class="form-control mb-3" type="text" name="zip_code" id="zip_code_c" value="{{ isset($profile->perfilSolicitud) ? $profile->perfilSolicitud->zip_code : null }}">
            </div>
                
            <div class="col-12 mb-3 col-md-4">
                <label for="">Comprobante de Domicilio</label>
                <div class="custom-file col">
                    <input class="form-control-file custom-file-input" type="file" name="file_address" id="file_address">
                    <label class="custom-file-label" for="file_address" data-browse="Buscar"></label>
                </div>
            </div>
    
            <div class="col-12 mb-3 mt-4 col-md-2">
                @isset($profile->perfilSolicitud)
                    @if (!is_null($profile->perfilSolicitud->file_address))
                        @if ($profile->getFileExists($profile->perfilSolicitud->file_address))
                            <a class="btn morado" href="{{ asset('/uploads/profile/'. $profile->perfilSolicitud->file_address) }}" target="_blank">
                                <i class="fa fa-file-alt"></i> Ver
                            </a>
                        @endif
                    @endif
                @endisset
            </div>
        </div>
        <br><br>
    
        <h5>Estudios</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_escolar_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilEscuelaBasica as $escuela) --}}
        @forelse (isset($profile) ? $profile->perfilEscuelaBasica : [] as $escuela)
            <div id="escolar_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-2">
                        <input type="hidden" name="type[]" value="basica">
                        <label for="studio" class="requerido">Nivel Alcanzado</label>
                        <select name="studio[]" id="studio_c" class="form-control mb-3">
                            {{-- <option disabled value="" selected hidden>Seleccione una opción...</option> --}}
                            <option value="Primaria" class="s-Primaria-c" {{ ($escuela->studio=='Primaria') ? 'selected' : null }}>Primaria</option>
                            <option value="Secundaria" class="s-Secundaria-c" {{ ($escuela->studio=='Secundaria') ? 'selected' : null }}>Secundaria</option>
                            <option value="Preparatoria" class="s-Preparatoria-c" {{ ($escuela->studio=='Preparatoria') ? 'selected' : null }}>Preparatoria</option>
                            <option value="Técnico" class="s-Técnico-c" {{ ($escuela->studio=='Técnico') ? 'selected' : null }}>Técnico</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-5">
                        <label for="school" class="requerido">Institución que Expidió Constancia</label>
                        <input type="text" name="school[]" id="school_c" class="form-control mb-3" value="{{ $escuela->school }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="date_end" class="requerido">Fecha Terminación</label>
                        <input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3" value="{{ $escuela->date_end }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="comp">Comprobante</label>
                        <div class="form-row">
                            <div class="col-1 col-md-6">
                                <label for="yes">Sí</label>
                                <input type="radio" name="voucher[]" id="voucher_c" value="1" class="mb-3" {{ ($escuela->voucher == '1') ? 'checked' : null }}>
                            </div>
                            <div class="col-1 col-md-6">
                                <label for="no">No</label>
                                <input type="radio" name="voucher[]" id="voucher_c" value="0" class="mb-3" {{ ($escuela->voucher == '0') ? 'checked' : null }}>
                            </div>
                        </div>
                    </div> 
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar Comprobante de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
                            <label class="custom-file-label" for="file_studio" data-browse="Buscar"></label>
                        </div>
                    </div>
                    <div class="col-12 mb-3 mt-4 col-md-2">
                        @if (!is_null($escuela->file_studio))
                            @if ($profile->getFileExists($escuela->file_studio))
                                <a class="btn morado" href="{{ asset('/uploads/profile/'. $escuela->file_studio) }}" target="_blank">
                                    <i class="fa fa-file-alt"></i> Ver
                                </a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-2">
                        <input type="hidden" name="type[]" value="basica">
                        <label for="studio" class="requerido">Nivel Alcanzado</label>
                        <select name="studio[]" id="studio_c" class="form-control mb-3">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Primaria" class="s-Primaria-c">Primaria</option>
                            <option value="Secundaria" class="s-Secundaria-c">Secundaria</option>
                            <option value="Preparatoria" class="s-Preparatoria-c">Preparatoria</option>
                            <option value="Técnico" class="s-Técnico-c">Técnico</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-5">
                        <label for="school" class="requerido">Institución que Expidió Constancia</label>
                        <input type="text" name="school[]" id="school_c" class="form-control mb-3">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="date_end" class="requerido">Fecha Terminación</label>
                        <input type="date" name="date_end[]" id="date_end_c" class="form-control mb-3">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="comp">Comprobante</label>
                        <div class="form-row">
                            <div class="col-1 col-md-6">
                                <label for="yes">Sí</label>
                                <input type="radio" name="voucher[]" id="voucher_c" value="1" class="mb-3">
                            </div>
                            <div class="col-1 col-md-6">
                                <label for="no">No</label>
                                <input type="radio" name="voucher[]" id="voucher_c" value="0" class="mb-3">
                            </div>
                        </div>
                    </div> 
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar Comprobante de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio[]" id="file_studio_c">
                            <label class="custom-file-label" for="file_studio" data-browse="Buscar"></label>
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_escolar_c"></div>
        <hr>
    
        <h5>Universitarios</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_escolar_uni_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilEscuelaSuperior as $escuela ) --}}
        @forelse (isset($profile) ? $profile->perfilEscuelaSuperior : [] as $escuela )
                <div id="escolar_uni_c">
                    <div class="form-row">
                        <div class="col-12 mb-3 col-md-3">
                            <input type="hidden" name="type_uni[]" value="superior">
                            <label for="studio_uni">Tipo de Carrera</label>
                            <select name="studio_uni[]" id="studio_uni_c" class="form-control">
                                {{-- <option disabled value="" selected hidden>Seleccione una opción...</option> --}}
                                <option value="Licenciatura" class="s-Licenciatura-c" {{ ($escuela->studio=='Licenciatura') ? 'selected' : null }}>Licenciatura</option>
                                <option value="Maestria" class="s-Maestria-c" {{ ($escuela->studio=='Maestria') ? 'selected' : null }}>Maestria</option>
                                <option value="Doctorado" class="s-Doctorado-c" {{ ($escuela->studio=='Doctorado') ? 'selected' : null }}>Doctorado/Posgrado</option>
                            </select>
                        </div>
                        <div class="col-12 mb-3 col-md-6">
                            <label for="career">Carrera</label>
                            <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c" value="{{ $escuela->career }}">
                        </div>
                        <div class="col-12 mb-3 col-md-2">
                            <label for="comp">Comprobante</label>
                            <div class="form-row">
                                <div class="col-1 col-md-6">
                                    <label for="yes">Sí</label>
                                    <input class="mb-3" type="radio" name="voucher_uni[]" value="1" id="voucher_uni_c" {{ ($escuela->voucher == '1') ? 'checked' : null }}>
                                </div>
                                <div class="col-1 col-md-6">
                                    <label for="no">No</label>
                                    <input class="mb-3" type="radio" name="voucher_uni[]" value="0" id="voucher_uni_c" {{ ($escuela->voucher == '0') ? 'checked' : null }}>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                            <button type="button" class="btn btn-danger borrar_escolar_uni_c">
                                <span class="fa fa-minus"></span>
                            </button>
                        </div>
                        <div class="col-12 mb-3 col-md-4">
                            <label for="">Agregar Comprobante de Último Grado de Estudios</label>
                            <div class="custom-file col">
                                <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                                <label class="custom-file-label" for="file_studio_uni" data-browse="Buscar"></label>
                            </div>
                        </div>
                        <div class="col-12 mb-3 mt-4 col-md-2">
                            @if (!is_null($escuela->file_studio))
                                @if ($profile->getFileExists($escuela->file_studio))
                                    <a class="btn morado" href="{{ asset('/uploads/profile/'. $escuela->file_studio) }}" target="_blank">
                                        <i class="fa fa-file-alt"></i> Ver
                                    </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
        @empty 
            <div id="escolar_uni_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <input type="hidden" name="type_uni[]" value="superior">
                        <label for="studio_uni_c">Tipo de Carrera</label>
                        <select name="studio_uni[]" id="studio_uni_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Licenciatura" class="s-Licenciatura-c">Licenciatura</option>
                            <option value="Maestria" class="s-Maestria-c">Maestria</option>
                            <option value="Doctorado" class="s-Doctorado-c">Doctorado/Posgrado</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="career">Carrera</label>
                        <input class="form-control mb-3" type="text" name="career_uni[]" id="career_uni_c">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="comp">Comprobante</label>
                        <div class="form-row">
                            <div class="col-1 col-md-6">
                                <label for="yes">Sí</label>
                                <input class="mb-3" type="radio" name="voucher_uni[]" value="1" id="voucher_uni_c">
                            </div>
                            <div class="col-1 col-md-6">
                                <label for="no">No</label>
                                <input class="mb-3" type="radio" name="voucher_uni[]" value="0" id="voucher_uni_c" checked>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_escolar_uni_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                    <div class="col-12 mb-3 col-md-4">
                        <label for="">Agregar Comprobante de Último Grado de Estudios</label>
                        <div class="custom-file col">
                            <input class="form-control-file custom-file-input" type="file" name="file_studio_uni[]" id="file_studio_uni">
                            <label class="custom-file-label" for="file_studio_uni" data-browse="Buscar"></label>
                        </div>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_escolar_uni_c"></div>
        <hr>
        
        <h5>Otros Conocimientos</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_language_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilLenguaje as $idioma ) --}}
        @forelse (isset($profile) ? $profile->perfilLenguaje : [] as $idioma )
            <div id="language_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="language">Idioma</label>
                        <input type="text" name="language[]" id="lenguaje_c" class="form-control" value="{{ $idioma->language }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="spoken">Conversación</label>
                        <input type="text" name="spoken[]" id="spoken_c" class="form-control" value="{{ $idioma->spoken }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reading">Lectura</label>
                        <input type="text" name="reading[]" id="reading_c" class="form-control" value={{ $idioma->reading }}>
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="writing">Escritura</label>
                        <input type="text" name="writing[]" id="writing_c" class="form-control" value="{{ $idioma->writing }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_language_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="language_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="language">Idioma</label>
                        <input type="text" name="language[]" id="language_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="spoken">Conversación</label>
                        <input type="text" name="spoken[]" id="spoken_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reading">Lectura</label>
                        <input type="text" name="reading[]" id="reading_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="writing">Escritura</label>
                        <input type="text" name="writing[]" id="writing_c" class="form-control">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_language_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
    
        <div id="destino_language_c"></div>
    
    
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_knowledge_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilConocimientos as $conocimiento) --}}
        @forelse (isset($profile) ? $profile->perfilConocimientos : [] as $conocimiento)
            <div id="escolar_knowledge_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="knowledge_type">Conocimientos que Domino</label>
                        <select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Maquinaria" {{ ($conocimiento->knowledge_type == "Maquinaria") ? 'selected' : null }}>Maquinaria y equipo</option>
                            <option value="Programas" {{ ($conocimiento->knowledge_type == "Programas") ? 'selected' : null }}>Programas y sistemas</option>
                            <option value="Funciones" {{ ($conocimiento->knowledge_type == "Funciones") ? 'selected' : null }}>Funciones de oficina</option>
                        </select>
                    </div>
                    
                    <div class="col-12 mb-3 col-md-8">
                        <label for="knowledge_name">Descripción:</label>     
                        <input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control" value="{{ $conocimiento->knowledge_name }}">
                    </div>
        
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_knowledge_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_knowledge_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="knowledge_type">Conocimientos que Domino</label>
                        <select name="knowledge_type[]" id="knowledge_type_c" class="form-control">
                            <option disabled value="" selected hidden>Seleccione una opción...</option>
                            <option value="Maquinaria">Maquinaria y equipo</option>
                            <option value="Programas">Programas y sistemas</option>
                            <option value="Funciones">Funciones de oficina</option>
                        </select>
                    </div>
                    
                    <div class="col-12 mb-3 col-md-8">
                        <label for="knowledge_name">Descripción:</label>     
                        <input type="text" name="knowledge_name[]" id="knowledge_name_c" class="form-control">
                    </div>
        
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_knowledge_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        <div id="destino_knowledge_c"></div>
        <hr>
    
        <h5>Antecedentes Laborales</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-12">
                <label for="motivo">Agregar al menos dos últimos empleos:</label>
            </div>
        </div>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_experience_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse($profile->perfilExperiencia as $experiencia) --}}
        @forelse(isset($profile) ? $profile->perfilExperiencia : [] as $experiencia)
            <div id="escolar_experience_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="job">Puesto Ocupado</label>
                        <input class="form-control mb-3" type="text" name="job[]" id="job_c" value="{{ $experiencia->job }}">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="company">Empresa/Domicilio</label>
                        <input class="form-control mb-3" type="text" name="company[]" id="company_c" value="{{ $experiencia->company }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_experience_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_begin_experience">Fecha Inicio</label>
                        <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c" value="{{ $experiencia->date_begin_experience }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end_experience">Fecha Fin</label>
                        <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c" value="{{ $experiencia->date_end_experience }}">
                        <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="salary">Sueldo</label>
                        <input class="form-control mb-3" type="text" name="salary[]" id="salary_c" value="{{ $experiencia->salary }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reason_separation">Motivo de Separación</label>
                        <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c" value="{{ $experiencia->reason_separation }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-12">
                        <label for="activity">Principales Actividades y Responsabilidades del Puesto</label>
                        <input class="form-control mb-3" type="text" name="activity[]" id="activity_c" value="{{ $experiencia->activity }}">
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_experience_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-5">
                        <label for="job">Puesto Ocupado</label>
                        <input class="form-control mb-3" type="text" name="job[]" id="job_c">
                    </div>
                    <div class="col-12 mb-3 col-md-6">
                        <label for="company">Empresa/Domicilio</label>
                        <input class="form-control mb-3" type="text" name="company[]" id="company_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_experience_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_begin_experience">Fecha Inicio</label>
                        <input class="form-control mb-3" type="date" name="date_begin_experience[]" id="date_begin_experience_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="date_end_experience">Fecha Fin</label>
                        <input class="form-control mb-3" type="date" name="date_end_experience[]" id="date_end_experience_c">
                        <span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="salary">Sueldo</label>
                        <input class="form-control mb-3" type="text" name="salary[]" id="salary_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reason_separation">Motivo de Separación</label>
                        <input class="form-control mb-3" type="text" name="reason_separation[]" id="reason_separation_c">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-12">
                        <label for="activity">Principales Actividades y Responsabilidades del Puesto</label>
                        <input class="form-control mb-3" type="text" name="activity[]" id="activity_c">
                    </div>
                </div>
            </div>
        @endforelse
    
        <div id="destino_experience_c"></div>
    
        <h5>Referencias</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_reference_c">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    
        {{-- @forelse ($profile->perfilReferencias as $referencia) --}}
        @forelse (isset($profile) ? $profile->perfilReferencias : [] as $referencia)
            <div id="escolar_reference_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre y Apellido</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c" value="{{ $referencia->reference_name }}">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reference_phone">Teléfono</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c" value="{{ $referencia->reference_phone }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerle</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c" value="{{ $referencia->reference_time_meet }}">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c" value="{{ $referencia->reference_occupation }}">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @empty
            <div id="escolar_reference_c">
                <div class="form-row">
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_name">Nombre y Apellido</label>
                        <input class="form-control mb-3" type="text" name="reference_name[]" id="reference_name_c">
                    </div>
                    <div class="col-12 mb-3 col-md-2">
                        <label for="reference_phone">Teléfono</label>
                        <input class="form-control mb-3" type="text" name="reference_phone[]" id="reference_phone_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_time_meet">Tiempo de conocerle</label>
                        <input class="form-control mb-3" type="text" name="reference_time_meet[]" id="reference_time_meet_c">
                    </div>
                    <div class="col-12 mb-3 col-md-3">
                        <label for="reference_occupation">Ocupación</label>
                        <input class="form-control mb-3" type="text" name="reference_occupation[]" id="reference_occupation_c">
                    </div>
                    <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                        <button type="button" class="btn btn-danger borrar_reference_c">
                            <span class="fa fa-minus"></span>
                        </button>
                    </div>
                </div>
            </div>
        @endforelse
        
    
        <div id="destino_reference_c"></div>
    
    
        <h5>Tiene Parientes Trabajando en la Empresa?</h5>
        <div class="form-row">
            <div class="col-1 col-md-1">
                <label for="family_working">Sí</label>
                <input class="mb-3" type="radio" name="family_working" id="family_working_1_c" value="1" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->family_working == '1' ? 'checked' : null : null }}>
            </div>
            <div class="col-1 col-md-1">
                <label for="family_working">No</label>
                <input class="mb-3" type="radio" name="family_working" id="family_working_0_c" value="0" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->family_working == '0' ? 'checked' : null : 'checked' }}>
            </div>
            <div class="col-1 col-md-6">
                <label for="family_working_name">Nombre:</label>
                <input class="form-control mb-3" type="text" name="family_working_name" id="family_working_name_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->family_working_name : null }}">
            </div>
        </div>
        <h5>Disponibilidad Para</h5>
        <div class="form-row">
            <div class="col-1 col-md-1">
                <label for="">Viajar</label>
            </div>
            <div class="col-1 col-md-1">
                <label for="availability_travel">Sí</label>
                <input class="mb-3" type="radio" name="availability_travel" id="availability_travel" value="1" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->availability_travel == '1' ? 'checked' : null : null}}>
            </div>
            <div class="col-1 col-md-1">
                <label for="availability_travel">No</label>
                <input class="mb-3" type="radio" name="availability_travel" id="availability_travel" value="0" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->availability_travel == '0' ? 'checked' : null : 'checked'}}>
            </div>
            <div class="col-1 col-md-1">
                <label for="">Rolar turno</label>
            </div>
            <div class="col-1 col-md-1">
                <label for="availability_shifts">Sí</label>
                <input class="mb-3" type="radio" name="availability_shifts" id="availability_shifts" value="1" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->availability_shifts == '1' ? 'checked' : null : null}}>
            </div>
            <div class="col-1 col-md-1">
                <label for="availability_shifts">No</label>
                <input class="mb-3" type="radio" name="availability_shifts" id="availability_shifts" value="0" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->availability_shifts == '0' ? 'checked' : null : 'checked' }}>
            </div>
            <div class="col-1 col-md-2">
                <label for="">Cambiar de residencia</label>
            </div>
            <div class="col-1 col-md-1">
                <label for="availability_residence">Sí</label>
                <input class="mb-3" type="radio" name="availability_residence" id="availability_residence" value="1" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->availability_residence == '1' ? 'checked' : null : null }}>
            </div>
            <div class="col-1 col-md-1">
                <label for="availability_residence">No</label>
                <input class="mb-3" type="radio" name="availability_residence" id="availability_residence" value="0" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->availability_residence == '0' ? 'checked' : null : 'checked' }}>
            </div>
        </div>
        <br>
        <h5>Ha estado Sindicalizado?</h5>
        <div class="form-row">
            <div class="col-1 col-md-1">
                <label for="unionized">Sí</label>
                <input class="mb-3" type="radio" name="unionized" id="unionized_1_c" value="1" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->unionized == '1' ? 'checked' : null : null }}>
            </div>
            <div class="col-1 col-md-1">
                <label for="unionized">No</label>
                <input class="mb-3" type="radio" name="unionized" id="unionized_0_c" value="0" {{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->unionized == '0' ? 'checked' : null : 'checked' }}>
            </div>
            <div class="col-1 col-md-6">
                <label for="unionized_name">Nombre del sindicato:</label>
                <input class="form-control mb-3" type="text" name="unionized_name" id="unionized_name_c" value="{{ isset($profile->perfilAdicional) ? $profile->perfilAdicional->unionized_name : null }}">
            </div>
        </div>
        
        <div class="row">
            <div class="col text-right">
                <button type="submit" class="btn morado">{{ ($crear) ? 'Guardar' : 'Actualizar' }}</button>
            </div>
        </div>
    </div>
    
<div class="container-fluid">
    <h5>FICHA DE SALUD:</h5>
    <div class="form-row">
        <div class="col-12 col-md-3 text-center">
            @if (isset($profile) && !is_null($profile->image) && $fileExists)
                <img src="{{ asset('storage/profile/'.$profile->image) }}" class="mb-3" style="max-width: 270px; max-height: 183px">
            @else
                <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
            @endif
        </div>
        <div class="col-9">
            <div class="row">
                <div class="form-group col"></div>
                
                <div class="form-group col"></div>

                <div class="form-group col">
                    <label for="" class="requerido">Número de empleado</label>
                    <input type="text" name="" id="" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->idempleado : null : null }}" disabled>
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="name" class="requerido">Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ isset($profile) ? !is_null($profile->user->employee) ? $profile->user->employee->nombre : null : $user->first_name }}" disabled={{ is_null($user) ? false : true }}>
                </div>
                
                <div class="form-group col">
                    <label for="surname_father" class="requerido">Apellido Paterno</label>
                    <input type="text" name="surname_father" id="surname_father" class="form-control" value="{{ isset($profile) ? !is_null($profile->user->employee) ? $profile->user->employee->paterno : null : $user->last_name }}" disabled={{ is_null($user) ? false : true }}>
                </div>

                <div class="form-group col">
                    <label for="surname_mother" class="requerido">Apellido Materno</label>
                    <input type="text" name="surname_mother" id="surname_mother" class="form-control" value="{{ isset($profile) ? !is_null($profile->user->employee) ? $profile->user->employee->materno : null : null }}" disabled={{ is_null($user) ? false : true }}>
                </div>
            </div>

            <div class="row col"></div>
        </div>
    </div>

    <h5>GENERALIDADES</h5>
    @if(is_null($profileSalud))
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="imss">IMSS:</label>
                <input type="text" name="imss" id="imss" class="form-control" value="{{  !is_null($user) ? !is_null($user->employee) ? $user->employee->nss : null : null }}" readonly={{ is_null($user) ? false : true }}>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="policy">Número de póliza SGMM / compañía</label>
                <input class="form-control" type="text" name="policy" id="policy">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="blood_type">Grupo sanguíneo:</label>
                <input class="form-control" type="text" name="blood_type" id="blood_type">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="allergies">Alergías conocidas</label>
                <input class="form-control" type="text" name="allergies" id="allergies">
            </div>
            <div class="col-12 mb-3 col-md-6">
                <label for="current_condition">Padecimientos actuales / medicamentos:</label>
                <input class="form-control" type="text" name="current_condition" id="current_condition">
            </div>
        </div>
        <br><br>

        <h5>EXAMEN MÉDICO</h5>
        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_salud">
                <span class="fa fa-plus"></span>
            </button>
        </div>

        <div id="salud_s">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-6">
                    <label for="date_medical">Fecha:</label>
                    <input class="form-control" type="date" name="date_medical[]" id="date_medical">
                </div>
                <div class="col-12 mb-3 col-md-5">
                    <label for="">Subir documento</label>
                    <div class="custom-file col">
                        <input class="form-control-file custom-file-input" type="file" name="file_medical[]" id="file_medical">
                        <label class="custom-file-label" for="file_medical" data-browse="Buscar"></label>
                    </div>
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_salud">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        </div>
        
        <div id="destino_salud"></div>

        <h5>CONTACTO DE EMERGENCIA</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="contact1">Nombre:</label>
                <input class="form-control" type="text" name="contact1" id="contact1">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="phone1">Teléfono</label>
                <input class="form-control" type="text" name="phone1" id="phone1">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="relationship1">Parentesco:</label>
                <input class="form-control" type="text" name="relationship1" id="relationship1">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="contact2">Nombre:</label>
                <input class="form-control" type="text" name="contact2" id="contact2">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="phone2">Teléfono:</label>
                <input class="form-control" type="text" name="phone2" id="phone2">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="relationship2">Parentesco:</label>
                <input class="form-control" type="text" name="relationship2" id="relationship2">
            </div>
        </div>
    @else
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="imss">IMSS:</label>
                <input type="text" name="imss" id="imss" class="form-control" value="{{ !is_null($user) ? !is_null($user->employee) ? $user->employee->nss : null : null }}" readonly={{ is_null($user) ? false : true }}>
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="policy">Número de póliza SGMM / compañía</label>
                <input class="form-control" type="text" name="policy" id="policy" value="{{ $profileSalud->policy }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="blood_type">Grupo sanguíneo:</label>
                <input class="form-control" type="text" name="blood_type" id="blood_type" value="{{ $profileSalud->blood_type }}">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-6">
                <label for="allergies">Alergías conocidas</label>
                <input class="form-control" type="text" name="allergies" id="allergies" value="{{ $profileSalud->allergies }}">
            </div>
            <div class="col-12 mb-3 col-md-6">
                <label for="current_condition">Padecimientos actuales / medicamentos:</label>
                <input class="form-control" type="text" name="current_condition" id="current_condition" value="{{ $profileSalud->current_condition }}">
            </div>
        </div>
        <br><br>

        <h5>EXAMEN MÉDICO</h5> 
        {{-- @forelse ($profileSalud->perfilSaludDet as $det) --}}
        @forelse (isset($profileSalud) ? $profileSalud->perfilSaludDet : [] as $det )
            <div class="form-row d-inline-flex col-12 mb-3 col-md-4">
                <div>
                    <label for="date_medical">Fecha:</label>
                    <input class="form-control" type="date" name="date_medical" id="date_medical" value="{{ $det->date }}" readonly={{ is_null($det->date) ? false : true }}>
                </div>
                <div class="mt-4 pl-2">
                    @if (!is_null($det->file_medical))
                        @if ($profile->getFileExists($det->file_medical))
                            <a class="btn morado" href="{{ asset('/storage/profile/'. $det->file_medical) }}" target="_blank">
                                <i class="fa fa-file-alt"></i> Ver
                            </a>
                        @endif
                    @endif
                </div>
            </div>
        @empty
        @endforelse
        <br><br>

        <div class="float-right">
            <button type="button" class="btn btn-success mt-4" id="agregar_salud">
                <span class="fa fa-plus"></span>
            </button>
        </div>
        <div id="salud_s">
            <div class="form-row">
                <div class="col-12 mb-3 col-md-6">
                    <label for="date_medical">Fecha:</label>
                    <input class="form-control" type="date" name="date_medical[]" id="date_medical">
                </div>
                <div class="col-12 mb-3 col-md-5">
                    <label for="">Subir documento</label>
                    <div class="custom-file col">
                        <input class="form-control-file custom-file-input" type="file" name="file_medical[]" id="file_medical">
                        <label class="custom-file-label" for="file_medical" data-browse="Buscar"></label>
                    </div>
                </div>
                <div class="col-12 mb-3 col-md-1 mt-4 text-right">
                    <button type="button" class="btn btn-danger borrar_salud">
                        <span class="fa fa-minus"></span>
                    </button>
                </div>
            </div>
        </div>
        
        <div id="destino_salud"></div>

        <h5 class="mt-4">CONTACTO DE EMERGENCIA</h5>
        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="contact1">Nombre:</label>
                <input class="form-control" type="text" name="contact1" id="contact1" value="{{ $profileSalud->contact1 }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="phone1">Teléfono</label>
                <input class="form-control" type="text" name="phone1" id="phone1" value="{{ $profileSalud->phone1 }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="relationship1">Parentesco:</label>
                <input class="form-control" type="text" name="relationship1" id="relationship1" value="{{ $profileSalud->relationship1 }}">
            </div>
        </div>

        <div class="form-row">
            <div class="col-12 mb-3 col-md-4">
                <label for="contact2">Nombre:</label>
                <input class="form-control" type="text" name="contact2" id="contact2" value="{{ $profileSalud->contact2 }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="phone2">Teléfono:</label>
                <input class="form-control" type="text" name="phone2" id="phone2" value="{{ $profileSalud->phone2 }}">
            </div>
            <div class="col-12 mb-3 col-md-4">
                <label for="relationship2">Parentesco:</label>
                <input class="form-control" type="text" name="relationship2" id="relationship2" value="{{ $profileSalud->relationship2 }}">
            </div>
        </div>
    @endif
    
    <div class="row">
        <div class="col text-right">
            <button type="submit" class="btn morado">{{ is_null($profileSalud) ? 'Guardar' : 'Actualizar' }}</button>
        </div>
    </div>

</div>

<div class="container-fluid">
    <h5>SOLICITUD DE EMPLEO:</h5>
    {{-- <div class="form-row">
        <div class="col-12 col-md-3 text-center">
            @if (isset($profile) && !is_null($profile->photo))
                <img src="{{ asset('img/vacantes/'.$profile->photo) }}" class="mb-3" style="max-width: 300px; max-height: 183px">
            @else
                <img src="{{ asset('img/vacantes/sinimagen.png') }}" class="mb-3" style="max-width: 300px; max-height: 183px">
            @endif
                <div class="custom-file form-group col">                
                    <input type="file" name="image" class="form-control custom-file-input" id="image" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="image" data-browse="Subir fotografía"></label>
                </div>
        </div>
        <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
            <label for="name">Nombre:</label>
            <input class="form-control mb-3" type="text" name="name" id="name" disabled>
            <label for="cellphone">Número de celular:</label>
            <input class="form-control mb-3" type="text" name="cellphone" id="cellphone" disabled>
            <label for="sex">Sexo:</label>
            <input class="form-control mb-3" type="text" name="sex" id="sex" disabled>
        </div>
        <div class="col-12 col-md-3 d-flex flex-column align-content-start justify-content-start">
            <label for="lname">Apellido Paterno:</label>
            <input class="form-control mb-3" type="text" name="lname" id="lname" disabled>
            <label for="phone">Número de teléfono fijo:</label>
            <input class="form-control mb-3" type="text" name="phone" id="phone" disabled>
            <label for="birthday">Fecha de Nacimiento:</label>
            <input class="form-control mb-3" type="text" name="birthday" id="birthday" disabled>
        </div>
        <div class="col-12 col-md-3  d-flex flex-column align-items-start justify-content-start">
            <label for="sname">Apellido Paterno:</label>
            <input class="form-control mb-3" type="text" name="sname" id="sname" disabled>
            <label for="lname">Correo Eléctronico:</label>
            <input class="form-control mb-3" type="text" name="email" id="email" disabled>
            <label for="rfc">RFC:</label>
            <input class="form-control mb-3" type="text" name="rfc" id="rfc" disabled>
        </div>
    </div> --}}

    <h5>DOMICILIO ACTUAL</h5>
    <div class="form-row">
        <div class="col-12 mb-3 col-md-6">
            <label for="address">Calle:</label>
            <input class="form-control mb-3" type="text" name="address" id="address">
        </div>
        <div class="col-12 mb-3 col-md-2">
            <label for="num_outside">No. Exterior</label>
            <input class="form-control mb-3" type="text" name="num_outside" id="num_outside">
        </div>
        <div class="col-12 mb-3 col-md-2">
            <label for="num_inside">No. Interior</label>
            <input class="form-control mb-3" type="text" name="num_inside" id="num_inside">
        </div>
        <div class="col-12 mb-3 col-md-2">
            <label for="colony">Colonia</label>
            <input class="form-control mb-3" type="text" name="colony" id="colony">
        </div>

        <div>
            <label for="zip_code">Código Postal</label>
            <input type="text" name="zip_code" id="zip_code">
        </div>

        <div>
            <label for="city">Ciudad:</label>
            <input type="text" name="city" id="city">
        </div>

        <div>
            <label for="state">Estado:</label>
            <input type="text" name="state" id="state">
        </div>

        <div>
            <label for="file_address">Comprobante de domicilio:</label>
            <input type="file" name="file_address" id="file_address">
        </div>
    </div>
    <br>

    <h5>PREPARACIÓN</h5>
    <div class="float-right">
        <button type="button" class="btn btn-success" id="agregar_application" style="margin-top: 32px;">
            <span class="fa fa-plus"></span>
        </button>
    </div>
    
    <div id="escolar_application">
        <div class="row">
            <div class="form-group col-3">
                <label for="career" class="requerido">Nivel alcanzado</label>
                <select name="career[]" id="career[]" class="form-control">
                    <option disabled value="" selected hidden>Seleccione una opción...</option>
                    <option value="Primaria">Primaria</option>
                    <option value="Secundaria">Secundaria</option>
                    <option value="Preparatoria">Preparatoria</option>
                    <option value="Tecnico">Técnico</option>
                </select>
            </div>

            <div class="form-group col-3">
                <label for="date_end" class="requerido">Fecha terminación</label>
                <input type="date" name="date_end[]" id="date_end[]" class="form-control" value="{{ isset($profile) ? $profile->FullNamePerfil : null}}">
            </div>

            <div class="form-group col-3">
                <label for="voucher">Comprobante</label>
                <input type="radio" name="voucher[]" id="voucher[]"> Si
            </div>

            <div class="form-group col-2">
                <label for="voucher">Comprobante</label>
                <input type="radio" name="voucher[]" id="voucher[]"> No        
            </div>

            <div class="form-group col-1 text-right">
                <button type="button" class="btn btn-danger" id="borrar_application" style="margin-top: 32px;">
                    <span class="fa fa-minus"></span>
                </button>
            </div>
        </div>
    </div>

    <div id="destino_application">
    </div>
    <br>

    <h5>UNIVERSITARIOS</h5>
    <div class="float-right">
        <button type="button" class="btn btn-success" id="agregar_application_uni" style="margin-top: 32px;">
            <span class="fa fa-plus"></span>
        </button>
    </div>
    
    <div id="escolar_application_uni">
        <div class="row">
            <div class="form-group col-2">
                <label for="career" class="requerido">Nivel alcanzado</label>
                <select name="career[]" id="career[]" class="form-control">
                    <option disabled value="" selected hidden>Seleccione una opción...</option>
                    <option value="Licenciatura">Licenciatura</option>
                    <option value="Maestria">Maestria</option>
                    <option value="Doctorado">Doctorado</option>
                </select>
            </div>

            <div class="form-group col-5">
                <label for="studio" class="requerido">Carrera</label>
                <input type="text" name="studio[]" id="studio[]" class="form-control" value="{{ isset($profile) ? $profile->FullNamePerfil : null}}">
            </div>

            <div class="form-group col-4">
                <label for="file_studio">Agregar comprobante del último grado de estudios</label>
                <input type="file" name="file_studio[]" id="file_studio[]">     
            </div>

            <div class="form-group col-1 text-right">
                <button type="button" class="btn btn-danger" id="borrar_application_uni" style="margin-top: 32px;">
                    <span class="fa fa-minus"></span>
                </button>
            </div>
        </div>
    </div>
    
    <div id="destino_application_uni">
    </div>
    <br>

    <h5>OTROS CONOCIMIENTOS:</h5>
    <div class="float-right">
        <button type="button" class="btn btn-success" id="agregar_application_language" style="margin-top: 32px;">
            <span class="fa fa-plus"></span>
        </button>
    </div>
    
    <div id="escolar_application_language">
        <div class="row">
            <div class="form-group col-11">
                <label for="language">Idiomas:</label>
                <input type="text" name="language[]" id="language[]">     
            </div>

            <div class="form-group col-1 text-right">
                <button type="button" class="btn btn-danger" id="borrar_application_language" style="margin-top: 32px;">
                    <span class="fa fa-minus"></span>
                </button>
            </div>
        </div>
    </div>
    
    <div id="destino_application_language">
    </div>
    <br>

    <div class="float-right">
        <button type="button" class="btn btn-success" id="agregar_application_knowledge" style="margin-top: 32px;">
            <span class="fa fa-plus"></span>
        </button>
    </div>
    
    <div id="escolar_application_knowledge">
        <div class="row">
            <div class="form-group col-2">
                <label for="knowledge_type" class="requerido">Tipo</label>
                <select name="knowledge_type[]" id="knowledge_type[]" class="form-control">
                    <option disabled value="" selected hidden>Seleccione una opción...</option>
                    <option value="Maquinaria">Maquinaria y equipo</option>
                    <option value="Programas">Programas y sistemas</option>
                    <option value="Funciones">Funciones de oficina</option>
                </select>
            </div>

            <div class="form-group col-9">
                <label for="knowledge_name">Descripción:</label>
                <input type="text" name="knowledge_name[]" id="knowledge_name[]">     
            </div>

            <div class="form-group col-1 text-right">
                <button type="button" class="btn btn-danger" id="borrar_application_knowledge" style="margin-top: 32px;">
                    <span class="fa fa-minus"></span>
                </button>
            </div>
        </div>
    </div>
    
    <div id="destino_application_knowledge">
    </div>
    <br>

    <h5>ANTECEDENTES LABORALES:</h5>
    <div class="float-right">
        <button type="button" class="btn btn-success" id="agregar_application_experience" style="margin-top: 32px;">
            <span class="fa fa-plus"></span>
        </button>
    </div>
    
    <div id="escolar_application_experience">
        <div class="row">
            <div class="form-group col-6">
                <label for="job">Puesto ocupado:</label>
                <input type="text" name="job[]" id="job[]">     
            </div>

            <div class="form-group col-6">
                <label for="company">Empresa/domicilio:</label>
                <input type="text" name="company[]" id="company[]">     
            </div>
        </div>

        <div class="row">
            <div class="form-group col-3">
                <label for="date_begin">Periodo - Inicio:</label>
                <input type="date" name="date_begin[]" id="date_begin[]">     
            </div>

            <div class="form-group col-3">
                <label for="date_end">Periodo - fin:</label>
                <input type="date" name="date_end[]" id="date_end[]">     
            </div>

            <div class="form-group col-3">
                <label for="salary">Salario:</label>
                <input type="text" name="salary[]" id="salary[]">     
            </div>

            <div class="form-group col-3">
                <label for="reason_separation">Motivo de separación:</label>
                <input type="text" name="reason_separation[]" id="reason_separation[]">     
            </div>
        </div>

        <div class="row">
            <div class="form-group col-12">
                <label for="activity">Actividades:</label>
                <input type="text" name="activity[]" id="activity[]">     
            </div>
        </div>

        <div class="form-group col-1 text-right">
            <button type="button" class="btn btn-danger" id="borrar_application_experience" style="margin-top: 32px;">
                <span class="fa fa-minus"></span>
            </button>
        </div>
    </div>
    
    <div id="destino_application_experience">
    </div>
    <br>

    <h5>REFERENCIAS:</h5>
    <div class="float-right">
        <button type="button" class="btn btn-success" id="agregar_application_reference" style="margin-top: 32px;">
            <span class="fa fa-plus"></span>
        </button>
    </div>
    
    <div id="escolar_application_reference">
        <div class="row">
            <div class="form-group col-3">
                <label for="reference_name">Nombre:</label>
                <input type="text" name="reference_name[]" id="reference_name[]">     
            </div>

            <div class="form-group col-3">
                <label for="reference_phone">Teléfono:</label>
                <input type="text" name="reference_phone[]" id="reference_phone[]">     
            </div>

            <div class="form-group col-3">
                <label for="reference_time_meet">Tiempo de conocerla:</label>
                <input type="text" name="reference_time_meet[]" id="reference_time_meet[]">     
            </div>

            <div class="form-group col-2">
                <label for="reference_occupation">Ocupación:</label>
                <input type="text" name="reference_occupation[]" id="reference_occupation[]">     
            </div>

            <div class="form-group col-1 text-right">
                <button type="button" class="btn btn-danger" id="borrar_application_reference" style="margin-top: 32px;">
                    <span class="fa fa-minus"></span>
                </button>
            </div>
        </div>
    </div>
    
    <div id="destino_application_reference">
    </div>
    <br>

    <h5>TIENES PARIENTES TRABAJANDO EN LA EMPRESA</h5>
    <div class="row">
        <label for="family_working">Si</label>
        <input type="radio" name="family_working[]" id="family_working[]">
        
        <label for="family_working">No</label>
        <input type="radio" name="family_working[]" id="family_working[]">

        <label for="family_working_name">Nombre:</label>
        <input type="text" name="family_working_name" id="family_working_name">
    </div>
    <br>

    <h5>DISPONIBILIDAD PARA:</h5>
    <div class="row">
        <label for="">Viajar: </label>
        <label for="availability_travel">Si</label>
        <input type="radio" name="availability_travel[]" id="availability_travel[]">
        
        <label for="availability_travel">No</label>
        <input type="radio" name="availability_travel[]" id="availability_travel[]">

        <label for="">Rolar turno: </label>
        <label for="availibility_shifts">Si</label>
        <input type="radio" name="availibility_shifts[]" id="availibility_shifts[]">
        
        <label for="availibility_shifts">No</label>
        <input type="radio" name="availibility_shifts[]" id="availibility_shifts[]">

        <label for="">CAmbiar de residencia: </label>
        <label for="availability_residence">Si</label>
        <input type="radio" name="availability_residence[]" id="availability_residence[]">
        
        <label for="availability_residence">No</label>
        <input type="radio" name="availability_residence[]" id="availability_residence[]">
    </div>
    <br>

    <h5>HA ESTADO SINDICALIZADO:</h5>
    <div class="row">
        <label for="unionized">Si</label>
        <input type="radio" name="unionized[]" id="unionized[]">
        
        <label for="unionized">No</label>
        <input type="radio" name="unionized[]" id="unionized[]">

        <label for="unionized_name">Nombre:</label>
        <input type="text" name="unionized_name" id="unionized_name">
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
        $(function() {
            var largo_esc = 0;
            $("#agregar_application").click(function() {
                $("#escolar_application").clone(true,true).appendTo("#destino_application");
                largo_esc++;
            });

            $("#borrar_application").click(function() {
                if(largo_esc > 0) {
                    $("#escolar_application").remove().end().appendTo("#destino_application");
                    largo_esc--;
                }
            });

            var largo_uni = 0;
            $("#agregar_application_uni").click(function() {
                $("#escolar_application_uni").clone(true,true).appendTo("#destino_application_uni");
                largo_uni++;
            });

            $("#borrar_application_uni").click(function() {
                if(largo_uni > 0) {
                    $("#escolar_application_uni").remove().end().appendTo("#destino_application_uni");
                    largo_uni--;
                }
            });

            var largo_language = 0;
            $("#agregar_application_language").click(function() {
                $("#escolar_application_language").clone(true,true).appendTo("#destino_application_language");
                largo_language++;
            });

            $("#borrar_application_language").click(function() {
                if(largo_language > 0) {
                    $("#escolar_application_language").remove().end().appendTo("#destino_application_language");
                    largo_language--;
                }
            });

            var largo_knowledge = 0;
            $("#agregar_application_knowledge").click(function() {
                $("#escolar_application_knowledge").clone(true,true).appendTo("#destino_application_knowledge");
                largo_knowledge++;
            });

            $("#borrar_application_knowledge").click(function() {
                if(largo_knowledge > 0) {
                    $("#escolar_application_knowledge").remove().end().appendTo("#destino_application_knowledge");
                    largo_knowledge--;
                }
            });

            var largo_experience = 0;
            $("#agregar_application_experience").click(function() {
                $("#escolar_application_experience").clone(true,true).appendTo("#destino_application_experience");
                largo_experience++;
            });

            $("#borrar_application_experience").click(function() {
                if(largo_experience > 0) {
                    $("#escolar_application_experience").remove().end().appendTo("#destino_application_experience");
                    largo_experience--;
                }
            });

            var largo_reference = 0;
            $("#agregar_application_reference").click(function() {
                $("#escolar_application_reference").clone(true,true).appendTo("#destino_application_reference");
                largo_reference++;
            });

            $("#borrar_application_reference").click(function() {
                if(largo_reference > 0) {
                    $("#escolar_application_reference").remove().end().appendTo("#destino_application_reference");
                    largo_reference--;
                }
            });
        });
    </script>
@endsection



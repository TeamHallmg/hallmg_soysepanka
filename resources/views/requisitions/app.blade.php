@section('sidebar')
    @include('requisitions.sidebar')
@endsection

@extends('layouts.app')

@section('mainScripts')
    @yield('scripts')
@endsection
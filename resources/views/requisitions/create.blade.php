@extends('requisitions.app')
@section('content')

<div class="container-fluid">
	<div class="row mb-5">
		<div class="col-12">
			<h3 class="font-weight-bold text-danger">NUEVA REQUISICIÓN DE PERSONAL</h3>
			<hr style="border: 2px solid #000000;">
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<h4 class="font-weight-bold text-danger">Datos Generales</h4>
			<hr style="border: 1px solid #000000;">
		</div>
		<div class="col-12">
			<form action="{{ url('requisitions') }}" method="POST">
				@csrf
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="">Nombre de la Vacante</label>
						<input type="email" class="form-control" name="" placeholder="">
					</div>
					<div class="form-group col-md-3">
						<label for="">Número de Vacantes</label>
						<input type="password" class="form-control" name="" placeholder="">
					</div>
					<div class="form-group col-md-3">
						<label for="">Folio</label>
						<input type="text" class="form-control" name="" placeholder="" readonly>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4 mb-0">
						<label>Tipo de Vacante</label>
					</div>
				</div>
				<div class="form-row mb-md-3 pl-1">
					<div class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Nueva</label>
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="" value="option1">
					</div>
					<div class="form-check col-md-3 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Existente</label>
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="" value="option1">
					</div>
					<div class="form-group row d-flex align-items-center col mb-4 mb-md-0">
						<label class="m-0 col-2" for="">Motivo:</label>
						<div class="col-10">
							<input type="email" class="form-control" name="" placeholder="">
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Tipo de Contratación</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label>Relación</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label>Jornada de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4 mb-0">
						<label>Publicación</label>
					</div>
				</div>
				<div class="form-row mb-md-3 pl-1">
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Internos</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Externos</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Ambos</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Confidencial</label>
						<input class="form-check-input" type="radio" name="" id="" value="option1">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label>Lugar/Centro de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="row mt-4">
					<div class="col-12">
						<h4 class="font-weight-bold text-danger">Especificaciones</h4>
						<hr style="border: 1px solid #000000;">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Lugar/Centro de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-5">
						<label>Lugar/Centro de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Lugar/Centro de Trabajo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label for="">Objetivo General</label>
						<textarea class="form-control" id="" rows="4"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col">
						<label for="">Principales Actividades y Responsabilidades del Puesto</label>
						<textarea class="form-control" id="" rows="4"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label>Conocimientos Específicos</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label>Competencias Requeridas</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-11">
						<label>Manejo de Herramientas, Software, ERP's, otro</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-6">
						<label>Nivel de Estudios Requeridos</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col col-md-1 d-flex justify-content-start align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
					<div class="form-group col-md-5">
						<label for="">Años de Experiencia</label>
						<input type="text" class="form-control" name="" placeholder="">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4 mb-0">
						<label>Idiomas</label>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group d-flex align-items-center col-12 col-md-3 mb-4 mb-md-0">
						<select class="col-12 form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group d-flex align-items-center justify-content-around col-12 col-md mb-4 mb-md-0 bg-check">
						<label class="m-0 font-weight-bold text-right" for="">Lectura</label>
						<input type="text" class="form-control form-control-sm col-6" name="" placeholder="">
					</div>
					<div class="form-group d-flex align-items-center justify-content-around col-12 col-md mb-4 mb-md-0 bg-check">
						<label class="m-0 font-weight-bold text-right" for="">Escritura</label>
						<input type="text" class="form-control form-control-sm col-6" name="" placeholder="">
					</div>
					<div class="form-group d-flex align-items-center justify-content-around col-12 col-md mb-4 mb-md-0 bg-check">
						<label class="m-0 font-weight-bold text-right" for="">Conversación</label>
						<input type="text" class="form-control form-control-sm col-6" name="" placeholder="">
					</div>
					<div class="m-0 form-group col-12 col-md-1 d-flex justify-content-end align-items-end">
						<button class="btn btn-danger"><i class="fas fa-plus"></i></button>
					</div>
				</div>

				<div class="row">
					<div class="col-12 mt-4">
						<h4 class="font-weight-bold text-danger">Equipo requerido para desempeñar el puesto</h4>
						<hr style="border: 1px solid #000000;">
					</div>
				</div>
				
				<div class="form-row mb-md-3 pl-1">
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Equipo de Cómputo</label>
						<input class="form-check-input" type="checkbox" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Celular</label>
						<input class="form-check-input" type="checkbox" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Licencia SAP</label>
						<input class="form-check-input" type="checkbox" name="" id="" value="option1">
					</div>
					<div class="form-check col-md form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Correo Empresa</label>
						<input class="form-check-input" type="checkbox" name="" id="" value="option1">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-12">
						<label for="">Otros</label>
						<input type="email" class="form-control" name="" placeholder="">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4 mb-0">
						<label>Sexo</label>
					</div>
				</div>
				<div class="form-row mb-md-3 pl-1">
					<div class="form-check col-md-2 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Hombre</label>
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="" value="option1">
					</div>
					<div class="form-check col-md-2 form-check-inline d-flex justify-content-between align-items-center bg-check py-2 mb-4 mb-md-0">
						<label class="form-check-label" for="">Mujer</label>
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="" value="option1">
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-6">
						<label>Estado Civil</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-10 col-md-6">
						<label>Disponibilidad</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-10 col-md-6">
						<label>Rango de Sueldo</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
					<div class="form-group col-10 col-md-6">
						<label>Prestaciones</label>
						<select class="form-control">
							<option>Default select</option>
						</select>
					</div>
				</div>
				<hr style="border: 2px solid #000000;">
				<div class="form-row">
					<div class="form-group col col-md-2">
						<a href="" class="btn btn-primary">Regresar</a>
					</div>
					<div class="form-group col col-md-2">
						<a href="" class="btn btn-danger">Cancelar</a>
					</div>
					<div class="form-group col col-md-8 d-flex justify-content-end">
						<a href="" class="btn btn-success">Guardar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

{{--<div class="container-fluid">
	<div class="d-flex justify-content-center">
  		<h3>NUEVA REQUISICIÓN</h3>
	</div>
	<hr style="border: 1px solid #3E4095;">

	<form action="{{ url('requisitions') }}" method="POST">
		@csrf

		<div class="row">
			<div class="col-md-3">
				<label for="fecha_requerida" class="requerido">Fecha requerida</label>
				<input type="date" name="fecha_requerida" class='form-control' min={{ date('Y-m-d') }} max={{ date('Y-m-d',strtotime('+5years')) }} required/>
			</div>
		
			<div class="col-md-3">
				<label for="fecha_elaborada">Fecha elaboración</label>
				<input type="text" name="fecha_elaborada" value={{ date('Y-m-d') }} class="form-control" readonly>
			</div>
		
			<div class="col-md-3">
				<label for="estatus_requi">Estatus requisición</label>
				<input type="text" name="estatus_requi" value="SOLICITADO" class="form-control" maxlength="45" readonly>
			</div>
		
			<div class="col-md-3">
				<label for="confidencial">Confidencial</label><br>
				<input type="checkbox" name="confidencial" value=1>
			</div>	
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="tipo_vacante" class="requerido">Tipo de vacante</label>
				<select name="tipo_vacante" id="tipo_vacante" class="form-control" required>
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="CUBRIR">Cubrir vacante</option>
					<option value="INCREMENTAR">Incrementar plantilla</option>
					<option value="CREAR">Crear puesto</option>
				</select>
			</div>
		
			<div class="col-md-3">
				<label for="cantidad">Número de plazas a autorizar:</label>
				<input type="number" name="cantidad" class="form-control", min="1" max="30">
			</div>
			
			<div class="col-md-3">
				<label for="area">Departamento:</label>
				<input type="text" name="area" id="area" class="form-control" maxlength="45">
			</div>

			<div class="col-md-3">
				<label for="puesto">Puesto:</label>
				<input type="text" name="puesto" id="puesto" class="form-control" maxlength="60">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="puesto_descripcion">Descripción del puesto:</label>
				<textarea name="puesto_descripcion" id="puesto_descripcion" class="form-control" cols="30" rows="5" maxlength="1000" style="resize: none;"></textarea>
			</div>
		</div>
		<br>

		<div class="row" id="nuevo_puesto">
			<div class="col-md-6">
				<label for="puesto">Puesto:</label>
				<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="horario">Lunes a Viernes</label><br>
				<input type="radio" name="horario" value="0">
			</div>

			<div class="col-md-3">
				<label for="horario">Lunes a Sábado</label><br>
				<input type="radio" name="horario" value="1">
			</div>

			<div class="col-md-6">
				<label for="horario_descripcion">Horario descripción:</label>
				<input type="text" name="horario_descripcion" class="form-control" maxlength="1000">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="edad_min">Edad mínima (años):</label>
				<input type="number" name="edad_min" id="edad_min" class="form-control" min="18" max="99">
			</div>

			<div class="col-md-3">
				<label for="edad_max">Edad máxima (años):</label>
				<input type="number" name="edad_max" id="edad_max" class="form-control" min="18" max="99">
				<span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
			</div>

			<div class="col-md-3">
				<label for="edo_civil">Estado civil:</label>
				<select name="edo_civil" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="SOLTERO">Soltero</option>
					<option value="CASADO">Casado</option>
					<option value="VIUDO">Viudo</option>
					<option value="DIVORCIADO">Divorciado</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>

			<div class="col-md-3">
				<label for="sexo">Sexo:</label>
				<select name="sexo" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="FEMENINO">Femenino</option>
					<option value="MASCULINO">Masculino</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="objetivo">Objetivo:</label>
				<textarea name="objetivo" class="form-control" style="resize:none;" maxlength="1000" cols="30" rows="5"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="rolesyresponsabilidades">Roles y responsabilidades:</label>
				<textarea name="rolesyresponsabilidades" class="form-control" style="resize:none;" maxlength="1000" col-md-3s="30" rows="5"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-2">
				<label for="experiencia">Requiere experiencia:</label>
			</div>

			<div class="col-md-1">
				<label for="experiencia">Si</label><br>
				<input type="radio" name="experiencia" id="experiencia_1" value="1">
			</div>

			<div class="col-md-1">
				<label for="experiencia">No</label><br>
				<input type="radio" name="experiencia" id="experiencia_0" value="0">
			</div>

			<div class="col-md-2">
				<label for="experiencia_anios">Tiempo de experiencia:</label>
				<input type="text" name="experiencia_anios" id="experiencia_anios" class="form-control" maxlength="15">
			</div>

			<div class="col-md-3">
				<label for="experiencia_grado_escolar">Nivel académico:</label>
				<select name="experiencia_grado_escolar" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="DOCTORADO">Doctorado</option>
					<option value="MAESTRIA">Maestría</option>
					<option value="LICENCIATURA">Licenciatura</option>
					<option value="TÉCNICO">Técnico</option>
					<option value="PASANTE">Pasante</option>
					<option value="ESTUDIANTE">Estudiante</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>

			<div class="col-md-3">
				<label for="experiencia_especializado">Especialidad:</label>
				<input type="text" name="experiencia_especializado" class="form-control" maxlength="45">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="experiencia_conocimientos">Conocimientos requeridos</label>
				<textarea name="experiencia_conocimientos" class="form-control" style="resize:none;" cols="30" rows="5" maxlength="1000"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="experiencia_habilidades">Habilidades requeridas:</label>
				<textarea name="experiencia_habilidades" class="form-control" style="resize:none;" cols="30" rows="5" maxlength="1000"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="0">
				<label for="lugar_trabajo">Oficina</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="1">
				<label for="lugar_trabajo">Laboratorio</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="2">
				<label for="lugar_trabajo">Mostrador (punto de venta)</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="3">
				<label for="lugar_trabajo">Otro</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Manaje información confidencial?</label>
			</div>

			<div class="col-md-2">
				<label for="informacion_confidencial">Si</label>	
				<input type="radio" name="informacion_confidencial" value="1">
			</div>

			<div class="col-md-2">
				<label for="informacion_confidencial">No</label>	
				<input type="radio" name="informacion_confidencial" value="0" checked>
			</div>

			<div class="col-md-5">
				<label style="background-color: #EEE;">Equipo a manjear (Maquinaria, equipo de oficina, vehículo, etc.)</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere equipo de cómputo?</label>
			</div>

			<div class="col-md-2">
				<label for="pc_requiere">Si</label>
				<input type="radio" name="pc_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="pc_requiere">No</label>
				<input type="radio" name="pc_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="pc_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere licencia de SAP?</label>
			</div>

			<div class="col-md-2">
				<label for="sap_requiere">Si</label>
				<input type="radio" name="sap_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="sap_requiere">No</label>
				<input type="radio" name="sap_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="sap_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere viajar (con frecuencia)?</label>
			</div>

			<div class="col-md-2">
				<label for="viajar_requiere">Si</label>
				<input type="radio" name="viajar_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="viajar_requiere">No</label>
				<input type="radio" name="viajar_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="viajar_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere automóvil?</label>
			</div>

			<div class="col-md-2">
				<label for="carro_requiere">Si</label>
				<input type="radio" name="carro_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="carro_requiere">No</label>
				<input type="radio" name="carro_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="carro_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere teléfono?</label>
			</div>

			<div class="col-md-2">
				<label for="tel_requiere">Si</label>
				<input type="radio" name="tel_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="tel_requiere">No</label>
				<input type="radio" name="tel_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="tel_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>
		
		<div class="row">
			<div class="col-md-12">
				<label for="comentarios">Comentarios</label>
				<textarea name="comentarios" class="form-control" cols="30" rows="5" style="resize:none;" maxlength="1000"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-4">
				<label for="sueldo">Sueldo</label>
				<input type="number" name="sueldo" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-4">
				<label for="ispt">ISPT</label>
				<input type="number" name="ispt" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-4">
				<label for="imss">IMSS</label>
				<input type="number" name="imss" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="sd">SD:</label>
				<input type="number" name="sd" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="sdi">SDI:</label>
				<input type="number" name="sdi" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="sueldo_aut">Sueldo autorizado:</label>
				<input type="number" name="sueldo_aut" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="prestaciones">Prestaciones:</label>
				<input type="text" name="prestaciones" class="form-control">
			</div>
		</div>
		<br>

		<div class="pull-right">
			<button type="submit" class="btn btn-success" id="btn_enviar">Guardar</button>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="{{ url('requisitions') }}" class="btn btn-danger">Cancelar</a>
		</div>
	</form>--}}


	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	@if (Session::has('flag'))
		<script type="text/javascript">
			$(function() {
				$('#modalError').modal('show');
			});
		</script>
	@endif

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>
<script type="text/javascript">
	$(function() {
		$("#nuevo_puesto").hide();

		$("#tipo_vacante").on('change', function(e) {
			e.preventDefault();
			var seleccion = $(this).val();
			if (seleccion == 'CREAR') {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideDown( "slow", function() {
					$( "#puesto" ).prop( "disabled", true );
					$( "#puesto_descripcion" ).prop( "disabled", true );
					$( "#area" ).prop( "disabled", true );
				});
			} else {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideUp( "slow", function() {
					$( "#puesto" ).prop( "disabled", false );
					$( "#puesto_descripcion" ).prop( "disabled", false );
					$( "#area" ).prop( "disabled", false );
				});
			}
		});

		$("#edad_max, #edad_min").on('change', function (e) {
			e.preventDefault();
			var edad_min = $("#edad_min").val();
			var edad_max = $("#edad_max").val();
    		if (edad_min > edad_max) {
				$("#edad_max_error").slideDown('slow');
			} else{
				$("#edad_max_error").slideUp('slow');
			}
		});

		$("#experiencia_0, #experiencia_1").on('change', function (e) {
			e.preventDefault();
			if ($("#experiencia_0").is(":checked")) {
				$( "#experiencia_anios" ).prop( "disabled", true );
			} else  {
				$( "#experiencia_anios" ).prop( "disabled", false );
			}
		});

		$('form').submit(function() {
			$("button[type='submit']").prop('disabled',true);
		});
	});
</script>
@endsection
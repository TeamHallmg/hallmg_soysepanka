@extends('requisitions.app')
@section('content')

<div class="container-fluid">
	<div class="d-flex justify-content-center">
  		<h3>NUEVA REQUISICIÓN</h3>
	</div>
	<hr style="border: 1px solid #3E4095;">

	<form action="{{ url('requisitions') }}" method="POST">
		@csrf

		<div class="row">
			<div class="col-md-3">
				<label for="fecha_requerida" class="requerido">Fecha requerida</label>
				<input type="date" name="fecha_requerida" class='form-control' min={{ date('Y-m-d') }} max={{ date('Y-m-d',strtotime('+5years')) }} required/>
			</div>
		
			<div class="col-md-3">
				<label for="fecha_elaborada">Fecha elaboración</label>
				<input type="text" name="fecha_elaborada" value={{ date('Y-m-d') }} class="form-control" readonly>
			</div>
		
			<div class="col-md-3">
				<label for="estatus_requi">Estatus requisición</label>
				<input type="text" name="estatus_requi" value="SOLICITADO" class="form-control" maxlength="45" readonly>
			</div>
		
			<div class="col-md-3">
				<label for="confidencial">Confidencial</label><br>
				<input type="checkbox" name="confidencial" value=1>
			</div>	
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="tipo_vacante" class="requerido">Tipo de vacante</label>
				<select name="tipo_vacante" id="tipo_vacante" class="form-control" required>
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="CUBRIR">Cubrir vacante</option>
					<option value="INCREMENTAR">Incrementar plantilla</option>
					<option value="CREAR">Crear puesto</option>
				</select>
			</div>
		
			<div class="col-md-3">
				<label for="cantidad">Número de plazas a autorizar:</label>
				<input type="number" name="cantidad" class="form-control", min="1" max="30">
			</div>
			
			<div class="col-md-3">
				<label for="area">Departamento:</label>
				<input type="text" name="area" id="area" class="form-control" maxlength="45">
			</div>

			<div class="col-md-3">
				<label for="puesto">Puesto:</label>
				<input type="text" name="puesto" id="puesto" class="form-control" maxlength="60">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="puesto_descripcion">Descripción del puesto:</label>
				<textarea name="puesto_descripcion" id="puesto_descripcion" class="form-control" cols="30" rows="5" maxlength="1000" style="resize: none;"></textarea>
			</div>
		</div>
		<br>

		<div class="row" id="nuevo_puesto">
			<div class="col-md-6">
				<label for="puesto">Puesto:</label>
				<input type="text" name="puesto_nuevo" id="puesto_nuevo" class="form-control" maxlength="60">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="horario">Lunes a Viernes</label><br>
				<input type="radio" name="horario" value="0">
			</div>

			<div class="col-md-3">
				<label for="horario">Lunes a Sábado</label><br>
				<input type="radio" name="horario" value="1">
			</div>

			<div class="col-md-6">
				<label for="horario_descripcion">Horario descripción:</label>
				<input type="text" name="horario_descripcion" class="form-control" maxlength="1000">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="edad_min">Edad mínima (años):</label>
				<input type="number" name="edad_min" id="edad_min" class="form-control" min="18" max="99">
			</div>

			<div class="col-md-3">
				<label for="edad_max">Edad máxima (años):</label>
				<input type="number" name="edad_max" id="edad_max" class="form-control" min="18" max="99">
				<span id="edad_max_error" class="clase_error"><strong>Mayor o igual a edad mínima</strong></span>
			</div>

			<div class="col-md-3">
				<label for="edo_civil">Estado civil:</label>
				<select name="edo_civil" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="SOLTERO">Soltero</option>
					<option value="CASADO">Casado</option>
					<option value="VIUDO">Viudo</option>
					<option value="DIVORCIADO">Divorciado</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>

			<div class="col-md-3">
				<label for="sexo">Sexo:</label>
				<select name="sexo" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="FEMENINO">Femenino</option>
					<option value="MASCULINO">Masculino</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="objetivo">Objetivo:</label>
				<textarea name="objetivo" class="form-control" style="resize:none;" maxlength="1000" cols="30" rows="5"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="rolesyresponsabilidades">Roles y responsabilidades:</label>
				<textarea name="rolesyresponsabilidades" class="form-control" style="resize:none;" maxlength="1000" col-md-3s="30" rows="5"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-2">
				<label for="experiencia">Requiere experiencia:</label>
			</div>

			<div class="col-md-1">
				<label for="experiencia">Si</label><br>
				<input type="radio" name="experiencia" id="experiencia_1" value="1">
			</div>

			<div class="col-md-1">
				<label for="experiencia">No</label><br>
				<input type="radio" name="experiencia" id="experiencia_0" value="0">
			</div>

			<div class="col-md-2">
				<label for="experiencia_anios">Tiempo de experiencia:</label>
				<input type="text" name="experiencia_anios" id="experiencia_anios" class="form-control" maxlength="15">
			</div>

			<div class="col-md-3">
				<label for="experiencia_grado_escolar">Nivel académico:</label>
				<select name="experiencia_grado_escolar" class="form-control">
					<option disabled value="" selected hidden>Seleccione una opción...</option>
					<option value="DOCTORADO">Doctorado</option>
					<option value="MAESTRIA">Maestría</option>
					<option value="LICENCIATURA">Licenciatura</option>
					<option value="TÉCNICO">Técnico</option>
					<option value="PASANTE">Pasante</option>
					<option value="ESTUDIANTE">Estudiante</option>
					<option value="INDISTINTO">Indistinto</option>
				</select>
			</div>

			<div class="col-md-3">
				<label for="experiencia_especializado">Especialidad:</label>
				<input type="text" name="experiencia_especializado" class="form-control" maxlength="45">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="experiencia_conocimientos">Conocimientos requeridos</label>
				<textarea name="experiencia_conocimientos" class="form-control" style="resize:none;" cols="30" rows="5" maxlength="1000"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<label for="experiencia_habilidades">Habilidades requeridas:</label>
				<textarea name="experiencia_habilidades" class="form-control" style="resize:none;" cols="30" rows="5" maxlength="1000"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="0">
				<label for="lugar_trabajo">Oficina</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="1">
				<label for="lugar_trabajo">Laboratorio</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="2">
				<label for="lugar_trabajo">Mostrador (punto de venta)</label>
			</div>

			<div class="col-md-3">
				<input type="radio" name="lugar_trabajo" value="3">
				<label for="lugar_trabajo">Otro</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Manaje información confidencial?</label>
			</div>

			<div class="col-md-2">
				<label for="informacion_confidencial">Si</label>	
				<input type="radio" name="informacion_confidencial" value="1">
			</div>

			<div class="col-md-2">
				<label for="informacion_confidencial">No</label>	
				<input type="radio" name="informacion_confidencial" value="0" checked>
			</div>

			<div class="col-md-5">
				<label style="background-color: #EEE;">Equipo a manjear (Maquinaria, equipo de oficina, vehículo, etc.)</label>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere equipo de cómputo?</label>
			</div>

			<div class="col-md-2">
				<label for="pc_requiere">Si</label>
				<input type="radio" name="pc_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="pc_requiere">No</label>
				<input type="radio" name="pc_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="pc_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere licencia de SAP?</label>
			</div>

			<div class="col-md-2">
				<label for="sap_requiere">Si</label>
				<input type="radio" name="sap_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="sap_requiere">No</label>
				<input type="radio" name="sap_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="sap_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere viajar (con frecuencia)?</label>
			</div>

			<div class="col-md-2">
				<label for="viajar_requiere">Si</label>
				<input type="radio" name="viajar_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="viajar_requiere">No</label>
				<input type="radio" name="viajar_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="viajar_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere automóvil?</label>
			</div>

			<div class="col-md-2">
				<label for="carro_requiere">Si</label>
				<input type="radio" name="carro_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="carro_requiere">No</label>
				<input type="radio" name="carro_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="carro_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label>¿Requiere teléfono?</label>
			</div>

			<div class="col-md-2">
				<label for="tel_requiere">Si</label>
				<input type="radio" name="tel_requiere" value="1">
			</div>

			<div class="col-md-2">
				<label for="tel_requiere">No</label>
				<input type="radio" name="tel_requiere" value="0" checked>
			</div>

			<div class="col-md-5">
				<input type="text" name="tel_descripcion" class="form-control" maxlength="300">
			</div>
		</div>
		<br>
		
		<div class="row">
			<div class="col-md-12">
				<label for="comentarios">Comentarios</label>
				<textarea name="comentarios" class="form-control" cols="30" rows="5" style="resize:none;" maxlength="1000"></textarea>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-4">
				<label for="sueldo">Sueldo</label>
				<input type="number" name="sueldo" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-4">
				<label for="ispt">ISPT</label>
				<input type="number" name="ispt" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-4">
				<label for="imss">IMSS</label>
				<input type="number" name="imss" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-3">
				<label for="sd">SD:</label>
				<input type="number" name="sd" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="sdi">SDI:</label>
				<input type="number" name="sdi" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="sueldo_aut">Sueldo autorizado:</label>
				<input type="number" name="sueldo_aut" placeholder="0.00" class="form-control" min="1" max="99999999.99" step="0.01"/>
			</div>
		
			<div class="col-md-3">
				<label for="prestaciones">Prestaciones:</label>
				<input type="text" name="prestaciones" class="form-control">
			</div>
		</div>
		<br>

		<div class="pull-right">
			<button type="submit" class="btn btn-success" id="btn_enviar">Guardar</button>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="{{ url('requisitions') }}" class="btn btn-danger">Cancelar</a>
		</div>
	</form>


	{{-- PARA MOSTRAR UNA VENTANA DE ERROR SINO SE ENCUENTRA EL ARCHIVO --}}
	@if (Session::has('flag'))
		<script type="text/javascript">
			$(function() {
				$('#modalError').modal('show');
			});
		</script>
	@endif

	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					{!! Session::get('flag') !!}
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<style type="text/css">
	.clase_error {
		display: none;
		color: red;
	}
</style>
<script type="text/javascript">
	$(function() {
		$("#nuevo_puesto").hide();

		$("#tipo_vacante").on('change', function(e) {
			e.preventDefault();
			var seleccion = $(this).val();
			if (seleccion == 'CREAR') {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideDown( "slow", function() {
					$( "#puesto" ).prop( "disabled", true );
					$( "#puesto_descripcion" ).prop( "disabled", true );
					$( "#area" ).prop( "disabled", true );
				});
			} else {
				$( "#puesto_nuevo" ).val('');
				$( "#nuevo_puesto" ).slideUp( "slow", function() {
					$( "#puesto" ).prop( "disabled", false );
					$( "#puesto_descripcion" ).prop( "disabled", false );
					$( "#area" ).prop( "disabled", false );
				});
			}
		});

		$("#edad_max, #edad_min").on('change', function (e) {
			e.preventDefault();
			var edad_min = $("#edad_min").val();
			var edad_max = $("#edad_max").val();
    		if (edad_min > edad_max) {
				$("#edad_max_error").slideDown('slow');
			} else{
				$("#edad_max_error").slideUp('slow');
			}
		});

		$("#experiencia_0, #experiencia_1").on('change', function (e) {
			e.preventDefault();
			if ($("#experiencia_0").is(":checked")) {
				$( "#experiencia_anios" ).prop( "disabled", true );
			} else  {
				$( "#experiencia_anios" ).prop( "disabled", false );
			}
		});

		$('form').submit(function() {
			$("button[type='submit']").prop('disabled',true);
		});
	});
</script>
@endsection
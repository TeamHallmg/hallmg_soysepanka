@extends('requisitions.app')
@section('content')

	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="row">
		<div class="col-md-7">
			<h3><center>REQUISICIONES DE PERSONAL</center></h3>
		</div>
		<div class="col-md-4" style="text-align: right;">
			{{-- @if (Auth::user()->role == 'admin')
				<button type="button" id="depurar" class="btn btn-danger" data-toggle="modal" data-target="#modalDepurar">
					Depurar
				</button>
			@endif --}}
			<a class="btn btn-small btn-info" href="{{ url('requisitions/create') }}">Nueva</a>
		</div>
	</div>
	<div class="container-fluid">
	<br><br>
	<table class="table table-striped table-bordered nowrap" id="tableRequisition">
		<thead>
			<tr>
				<th>Requisición</th>
				<th>Solicitante</th>
				<th>Tipo Vacante</th>
				<th>Fecha Solicitud</th>
				<th>Fecha Requerida</th>
				<th>Puesto Solicitado</th>
				<th>Acciones</th>
				<th>ruta</th>
			</tr>
		</thead>
		<tbody>
			@foreach($requisitions as $requisition)
				<tr>
					<td>{{ $requisition->id }}</td>
					<td>{{ $requisition->usuario->FullName }}</td>
					<td>{{ $requisition->tipo_vacante }}</td>
					<td>{{ Carbon\Carbon::createFromFormat('Y-m-d', $requisition->fecha_elaborada)->format('d-m-Y') }}</td>
					<td>{{ Carbon\Carbon::createFromFormat('Y-m-d', $requisition->fecha_requerida)->format('d-m-Y') }}</td>
					<td>{{ $requisition->puesto }} </td>
					{{--  we will also add show, edit, and delete buttons  --}}
					<td>
						@if ($requisition->autorizacion)
							<a class="btn btn-small btn-primary" href="{{ url('requisitions/' . $requisition->id) }}">Autorizar</a>
						@endif

						@if (!$requisition->autorizacion && !$requisition->rechazo)
							{{--  show the nerd (uses the show method found at GET /nerds/{id}  --}}
							<a class="btn btn-small btn-success" href="{{ url('requisitions/' . $requisition->id) }}">Ver</a>
						@endif
						{{--  edit this nerd (uses the edit method found at GET /nerds/{id}/edit  --}}
						@if ($requisition->editar)
							<a class="btn btn-small btn-info" href="{{ url('requisitions/' . $requisition->id . '/edit') }}">Editar</a>
						@endif
						@if ($requisition->rechazo)
							<a class="btn btn-small btn-danger" href="{{ url('requisitions/' . $requisition->id ) }}">Rechazada</a>
						@endif
					</td>
					<td>
						@php
							$flag = false;	
						@endphp
						@foreach ($rutas as $i => $ruta)
							@if ($ruta->tipo_vacante == $requisition->tipo_vacante)
								@if ($ruta->user_id == $requisition->current_id && $requisition->estatus_requi != 'AUTORIZADA')
									<span style="color: red;"> {{ $ruta->usuario->id }} {{ $ruta->usuario->FullName }}</span> <br>
									@php
										$flag=true;
									@endphp
								@elseif ($ruta->user_id == $requisition->current_id && $requisition->estatus_requi == 'AUTORIZADA')
									<span style="color: green;"> {{ $ruta->usuario->id }} {{ $ruta->usuario->FullName }} </span> <br>
								@elseif ($ruta->user_id != $requisition->current_id && $flag)
									<span style="color: red;"> {{ $ruta->usuario->id }} {{ $ruta->usuario->FullName }} </span> <br>
								@elseif ($ruta->user_id != $requisition->current_id && $flag == false)
									<span style="color: green;"> {{ $ruta->usuario->id }} {{ $ruta->usuario->FullName }} </span> <br>
								@endif
							@endif
						@endforeach
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableRequisition').DataTable({
		"scrollX": "true",
		"fixedColumns":   {
			"leftColumns": "1",
			"rightColumns": "1",
		},
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection
@extends('layouts.app')

@section('content')
<div class="row my-5">
		<div class="col">
			@if(session()->has('success1'))
				<div class="alert alert-success alert-dismissible fade show card-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>¡El usuario {{session()->get('success1')}} fue suspendido con éxito!</strong>
				</div>
			@elseif(session()->has('success0'))
				<div class="alert alert-success alert-dismissible fade show card-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>¡El usuario {{session()->get('success0')}} fue creado con éxito!</strong>
				</div>
			@elseif(session()->has('success2'))
				<div class="alert alert-success alert-dismissible fade show card-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>¡El usuario {{session()->get('success2')}} fue reactivado con éxito!</strong>
				</div>
			@elseif(session()->has('errors'))
				<div class="alert alert-danger alert-dismissible fade show card-1" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>{{$errors->first()}}</strong>
				</div>
			@endif
			<div class="card card-3">
				<div class="card-header">
					<h2 class="my-auto font-weight-bold">Administrar Usuarios</h2>
				</div>
				<div class="card-body">
					<div class="row mb-4">
						<div class="col col-md-12 my-3 text-center">
							<a href="{{route('admin-de-usuarios.create')}}" class="btn btn-success mx-1 deleteBtn card-1">
								<i class="fas fa-plus-circle"></i> Crear Nuevo Usuario
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 tableWrapper hidden">
						<table id="userAdmin" class="table table-striped table-bordered w-100">
							<thead style="background-color: #373435; color:white;">
								<tr>
									<th style="background-color: #ED3237">ID de Usuario</th>
									{{--<th style="background-color: #005C99">Employee ID</th>--}}
									<th style="background-color: #ED3237">No. de Empleado</th>
									<th style="background-color: #ED3237">Nombre</th>
									<th style="background-color: #ED3237">Apellidos</th>
									<th>Email Empresarial</th>
									<th>Email Personal</th>
									<th>Fecha de Ingreso</th>
									<th>Cumpleaños</th>
									<th>Fuente</th>
									{{-- <th>RFC</th> --}} <th>NIT</th>
									{{-- <th>CURP</th> --}} <th>DPI</th>
									{{-- <th>NSS</th> --}} <th>IGSS</th>
									<th>Sexo</th>
									<th>Civil</th>
									<th>Teléfono</th>
									<th>Ext.</th>
									<th>Celular</th>
									<th>Rol</th>
									<th>Jefe</th>
									<th>Dirección</th>
									{{--<th>Departamento</th>
									<th>Sección</th>--}}
									<th>Puesto</th>
									<th>Grado</th>
									<th>Región</th>
									<th>Sucursal</th>
									{{--<th>ID Empresa</th>--}}
									{{--<th>Empresa</th>--}}
									<th>División</th>
									<th>Marca</th>
									<th>Centro</th>
									<th>Checador</th>
									<th>Turno</th>
									<th>Tipo Nómina</th>
									<th>Clave Nómina</th>
									<th>Nombre Nómina</th>
									<th>Generalista</th>
									<th>Relación</th>
									<th>Contrato</th>
									<th>Horario</th>
									<th>Jornada</th>
									<th>Cálculo</th>
									<th>Vacaciones</th>
									<th>Flotante</th>
									<th>Base</th>
									<th>Extra1</th>
									<th>Extra2</th>
									<th>Extra3</th>
									<th>Extra4</th>
									<th>Extra5</th>
									<th style="background-color: #ED3237">Acciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($usuarios as $usuario)
									<tr @if($usuario->deleted_at != null) class="bg-warning" @endif>
										<td class="text-center">
											{{$usuario->employee->id}}
										</td>
										{{--<td class="text-center">
											{{$usuario->employee->employee_id}}
										</td>--}}
										<td nowrap class="text-center">
											{{$usuario->employee->idempleado}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nombre}} 
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->paterno}} {{$usuario->employee->materno}} 
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->correoempresa}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->correopersonal}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->ingreso}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nacimiento}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->fuente}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->rfc}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->curp}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nss}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->sexo}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->civil}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->telefono}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extension}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->celular}}
										</td>
										<td nowrap class="text-center">
											@if($usuario->employee->rol == 'admin')
											Administrador
											@elseif($usuario->employee->rol == 'employee')
											Empleado
											@endif
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->jefe}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->direccion}}
										</td>
										{{--<td nowrap class="text-center">
											{{$usuario->employee->departamento}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->seccion}}
										</td>--}}
										<td nowrap class="text-center">
											{{$usuario->employee->job_position_id}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->grado}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->region}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->sucursal}}
										</td>
										{{--<td nowrap class="text-center">
											{{$usuario->employee->idempresa}}
										</td>--}}
										{{--<td nowrap class="text-center">
											{{$usuario->employee->empresa}}
										</td>--}}

										<td nowrap class="text-center">
											{{$usuario->employee->division}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->marca}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->centro}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->checador}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->turno}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->tiponomina}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->clavenomina}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->nombrenomina}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->generalista}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->relación}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->contrato}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->horario}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->jornada}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->calculo}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->vacaciones}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->flotante}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->base}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra1}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra2}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra3}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra4}}
										</td>
										<td nowrap class="text-center">
											{{$usuario->employee->extra5}}
										</td>
										<td style="vertical-align:middle" nowrap>
											@if(!$usuario->deleted_at)
											<form class="form" action="{{ route('admin-de-usuarios.destroy' , $usuario->employee->id)}}" method="POST">
												<div class="form-row d-inline justify-content-center align-content-center">
													<a href="{{url('profile/'.$usuario->id)}}" class="btn btn-info mx-1 editBtn card-1 text-white" title="Expediente">
														<i class="far fa-file-alt"></i>
													</a>
													{{-- <a href="{{url('expediente/ver')}}" class="btn btn-info mx-1 editBtn card-1 text-white" title="Expediente">
														<i class="far fa-file-alt"></i>
													</a> --}}
													<a href="{{route('admin-de-usuarios.edit', $usuario->employee->id)}}" class="btn btn-dark mx-1 editBtn card-1" title="Editar">
														<i class="fas fa-pencil-alt"></i>
													</a>
													{!! method_field('DELETE') !!}
													{!! csrf_field() !!}
													<button data-toggle="tooltip" title="Eliminar" type="submit" class="btn btn-danger card-1">
														<i class="fas fa-times"></i>
													</button>
												</div>
											</form>
											@else
											<form class="form" action="{{ route('activate-user', $usuario->employee->id) }}" method="POST">
												<div class="form-row d-inline justify-content-center align-content-center">
													<a href="{{route('admin-de-usuarios.edit',$usuario->employee->id)}}" class="btn btn-dark mx-1 editBtn card-1">
														<i class="fas fa-pencil-alt text-white"></i>
													</a>
													{!! method_field('POST') !!}
													{!! csrf_field() !!}
													<button title="Activar" type="submit" class="btn btn-success editBtn card-1">
														<i class="fas fa-check text-white"></i>
													</button>
												</div>
											</form>
											@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {

		$('#userAdmin').DataTable({
			"order": [[ 0, "asc" ]],
			"paging": true,
			"pagingType": "numbers",
			"scrollX": true,
			"fixedColumns":{
				"leftColumns": 4,
				"rightColumns": 1,
			},
			language: {
		 		"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ <br>de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 <br>de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     ">",
					"sPrevious": "<"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
      		  }
	  	});

		$('.tableWrapper').hide();
		setTimeout(() => {
			$('.tableWrapper').show();
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust()
			.fixedColumns().relayout();
		}, 500);
	});
	
		
</script>
@endsection
@section('sidebar')
    @include('vacantes.sidebar')
@endsection

@extends('layouts.app')

@section('mainScripts')
    @yield('scripts')
@endsection
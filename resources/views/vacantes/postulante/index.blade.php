@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				<h3><center>POSTULADOS A LA VACANTE</center></h3>
				@if($can > 0)
					<h4><center>{{ $postulados[0]->vacante->requisicion->puesto }}</center></h4>
				@endif
			</div>
			<div class="col-md-3" style="text-align: center;">
				<a class="btn btn-small btn-info" href="{{ url('vacantes/administrar') }}">Regresar</a>
			</div>
		</div>
		<table class="table table-striped table-bordered" id="tableReclutador">
			<thead>
				<tr>
					<th>id</th>
					<th>Postulado</th>
					<th>Tipo Postulación</th>
					<th>Reclutador</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($postulados as $postulado)
					<tr>
						<td>{{ $postulado->id }}</td>
						@if ($postulado->user_id != 0)
							<td>{{ $postulado->usuario->FullName }}</td>
							<td>Interno</td>
						@else
							<td>{{ $postulado->perfil->FullNamePerfil }}</td>
							<td>Externo</td>
						@endif
						<td>{{ $postulado->vacante->reclutador->usuario->FullName }}</td>
						<td>
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar-modal{{ $postulado->id }}">
								<i class="fa fa-trash"> Remover</i>
							</button>
							<a class="btn btn-small btn-primary" href="{{ url('profile/'.$postulado->user_id.'/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/consulta') }}">Info postulante</a>
							<a class="btn btn-small btn-warning" href="{{ url('profile/'.$postulado->profile_id.'/'.$postulado->vacante_id.'/generar') }}">Generar Acceso</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	{{-- modal para agregar un nuevo reclutador --}}
	@foreach($postulados as $postulado)
	<div class="modal fade" id="eliminar-modal{{ $postulado->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
			<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form action="{{ route('postulante.destroy', $postulado->id) }}" method="POST">
							@csrf
							@method('DELETE')
							<div class="modal-header">
								REMOVER POSTULANTE
							</div>
							<div class="modal-body">
								<label for="motivo">Motivo: </label>
									<input type="hidden" name="id" value="{{ $postulado->id }}">
									<textarea name="motivo" id="motivo" cols="30" rows="5" style="resize:none;"></textarea>
							</div>
							<div class="modal-footer">
									<button type="submit" class="btn btn-danger">Eliminar</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</form>
					</div>
			</div>
		</div>
	@endforeach
@endsection

@section('scripts')
<style type="text/css">
	.requerido:after {
        content: '*';
        color: red;
        padding-left: 5px;
    }
</style>
<script type="text/javascript">
	$('#tableReclutador').DataTable({
		language: {
		 		"sProcessing":     "Procesando...",
                             "sLengthMenu":     "Mostrar _MENU_ registros",
                             "sZeroRecords":    "No se encontraron resultados",
                             "sEmptyTable":     "Ningún dato disponible en esta tabla",
                             "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                             "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                             "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                             "sInfoPostFix":    "",
                             "sSearch":         "Buscar:",
                             "sUrl":            "",
                             "sInfoThousands":  ",",
                             "sLoadingRecords": "Cargando...",
                             "oPaginate": {
                                 "sFirst":    "Primero",
                                 "sLast":     "Último",
                                 "sNext":     "Siguiente",
                                 "sPrevious": "Anterior"
                             },
                             "oAria": {
                                 "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                 "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                             }
      		  }
   	});
</script>
@endsection
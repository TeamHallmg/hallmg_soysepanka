@extends('vacantes.app')
@section('content')
	<div class="flash-message" id="mensaje">
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
			@if(Session::has('alert-'.$msg))
				<p class="alert alert-{{ $msg }}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
			@endif
		@endforeach
	</div>
	<br>
	
	<div class="d-flex justify-content-center">
		<div class="row">
			<h3>GESTIÓN - VACANTES DE PERSONAL</h3>
		</div>
	</div>
	<div class="d-flex justify-content-center">
		<div class="row">
			<small>Debe asignar un reclutador, para que la vacante este disponible</small>
		</div>
	</div>
	
	<div class="container-fluid">
	<br><br>
	<table class="table table-striped table-bordered nowrap" id="tableRequisition">
		<thead>
			<tr>
				<th>Requisición</th>
				<th>Solicitante</th>
				<th>Tipo Vacante</th>
				<th>Puesto Solicitado</th>
				<th>Cantidad</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($requisitions as $pos => $requisition)
				<tr>
					<td>{{ $requisition->id }}</td>
					<td>{{ $requisition->usuario->FullName }}</td>
					<td>{{ $requisition->tipo_vacante }}</td>
					<td>{{ $requisition->puesto }} </td>
					<td>{{ $requisition->cantidad }} </td>
					{{--  we will also add show, edit, and delete buttons  --}}
					<td>
						<a class="btn btn-small btn-success" href="{{ url('vacantes/' . $requisition->id) }}">Ver Vacante</a>
						@if(is_null($requisition->vacante)) 
							<a class="btn btn-small btn-info" href="{{ url('vacantes/recruiter/' . $requisition->id ) }}">Generar Vacante</a>	
						@else
							<a class="btn btn-small btn-info" href="{{ url('postulante/' . $requisition->vacante->id ) }}">POSTULANTES</a>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
	$('#tableRequisition').DataTable({
		"scrollX": "true",
		"fixedColumns":   {
			"leftColumns": "1",
			"rightColumns": "1",
		},
		language: {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
   	});
</script>
@endsection
<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
            <i class="nav-icon fas fa-temperature-low"></i>
        <p>
        Vacantes
        <i class="right fa fa-angle-left"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('requisitions') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Requisiciones</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('reclutador') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Reclutadores</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('vacantes/administrar') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Admin Vacantes</span></p>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('vacantes') }}">
                <i class="fa fa-plus nav-icon"></i>
                <p><span>Vacantes</span></p>
            </a>
        </li>
        
    </ul>
</li>
      
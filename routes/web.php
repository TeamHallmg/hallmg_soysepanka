<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('asdasd', function(){ return \Hash::make('admin');});
Route::get('/', function () {
    return view('welcome');
})->name('main');

Route::get('mision', function(){
    return view('principales.mision');
})->name('mision');

Route::get('vision', function(){
    return view('principales.vision');
})->name('vision');

Route::get('valores', function(){
    return view('principales.valores');
})->name('valores');

Route::get('historia', function(){
    return view('principales.historia');
})->name('historia');

Route::get('organigrama', function(){
    return view('principales.organigrama');
})->name('organigrama');

Route::get('directorio', function () {
    return view('principales.directorio');
})->name('directorio');

Route::get('prestaciones', function () {
    return view('principales.prestaciones');
})->name('prestaciones');

Route::get('convenios', function () {
    return view('principales.convenios');
})->name('convenios');

//---------


Route::get('requisition', function () {
    return view('plain-requisitions');
})->name('requisiciones');

Route::get('requisition/create', function () {
    return view('plain-requisitions-create');
})->name('crear-requisicion');

Route::get('requisition/autorizar', function () {
    return view('plain-requisitions-autorizar');
})->name('autorizar-requisicion');

Route::get('expediente/ver', function () {
    return view('plain-curriculum');
})->name('expediente');

Route::get('mis-postulaciones', function () {
    return view('plain-mis-postulaciones');
})->name('mis-postulaciones');

Route::get('postulantes-puesto', function () {
    return view('plain-postulantes-puesto');
})->name('postulantes-puesto');

Route::get('postularme-encuesta', function () {
    return view('plain-postularme-encuesta');
})->name('postularme-encuesta');

Route::get('vacancies', function () {
    return view('plain-vacantes');
})->name('vacantes');

Route::get('vacancies/list', function () {
    return view('plain-vacantes-list');
})->name('listado-vacantes');

Route::get('vacancies/create', function () {
    return view('plain-vacante-create');
})->name('crear-vacante');

Route::get('vacancies/consulta', function () {
    return view('plain-vacante-consulta');
})->name('consulta-vacante');

Route::get('vacancies/disponibles', function () {
    return view('plain-vacante-disponible');
})->name('vacante-disponible');

/*
 *	Rutas de Importacion y de Moodle
 */
Route::group(['prefix' => 'cron'], function () {
    Route::get('import', 'CronAutomatico\CronController@mainFunction');
});


/**
 *  ================ Announcement Routes ================ 
 */
Route::resource('announcements', 'Announcement\AnnouncementController');
Route::resource('announcement_types', 'Announcement\AnnouncementTypeController');
Route::resource('announcement_categories','Announcement\AnnouncementCategoryController');
Route::resource('views', 'Announcement\ViewController');
Route::resource('announcement_forms', 'Announcement\AnnouncementFormController');
Route::resource('announcement_types_per_view', 'Announcement\AnnouncementTypePerViewController');

Route::get('example', 'Announcement\AnnouncementController@example');
Route::post('announcements/{announcement}/activate', 'Announcement\AnnouncementController@activate');
/**
 *  =============== /Announcement Routes ================
 */

 /**
 *   ================ Admin de Usuarios ================ 
 */
Route::resource('admin-de-usuarios','UserController');
Route::resource('admin-form','EmployeeFormController');

Route::post('admin-de-usuarios/{id}/activate','UserController@activate')->name('activate-user');

Route::resource('direcciones','DirectionController');
Route::resource('departamentos','DepartmentController');
Route::resource('areas','AreaController');

Route::get('trashedDepartments/{id}','DepartmentController@getTrashed')->name('trashed-departments');
Route::get('trashedAreas/{id}','AreaController@trashedAreas')->name('trashed-areas');

Route::get('departments/{id}','PuestoController@departments')->name('departments');
Route::get('area/{id}','PuestoController@areas')->name('area');
Route::get('trashedJobs/{id}','PuestoController@trashedJobs')->name('trashed-jobs');
Route::get('jobpositions/{id}','PuestoController@jobpositions')->name('jobpositions');
Route::get('jobboss','PuestoController@jobboss')->name('jobboss');
Route::get('levels','PuestoController@levels')->name('levels');

// Route::get('organigrama', 'HomeController@organigrama');
Route::get('organigrama-puestos', 'HomeController@organigramaPuestos');
 /**
 *   =============== /Admin de Usuarios ================ 
 */

Route::get('filosofia', 'HomeController@filosofia');
 
//*********************************************************************************
//rutas para la requisición de personal
//inicio
Route::get('requisitions/{id}/autorizar', 'Requisitions\RequisitionController@autorizar');
Route::post('rechazarRequi', 'Requisitions\RequisitionController@rechazar');

Route::resource('requisitions', 'Requisitions\RequisitionController');

//fin
//*********************************************************************************

//*********************************************************************************
//rutas para vacntes de personal y todos los elemetos necesarios
//vacantes - reclutadores
//inicio
Route::get('vacantes_que_es', function () {
    return view ('vacantes.que-es');
});
Route::get('vacantes/showExt/{id}', 'Vacantes\VacantesController@showExt');
Route::get('vacantes/administrar', 'Vacantes\VacantesController@administrar');
Route::get('vacantes/recruiter/{id}', 'Vacantes\VacantesController@recruiter');
Route::post('vacantes/addRecruiter', 'Vacantes\VacantesController@addRecruiter');

Route::get('postulante/{id}/crear', 'Vacantes\PostulanteController@crear');

Route::resources([
  'vacantes' => 'Vacantes\VacantesController',
  'reclutador' => 'Vacantes\ReclutadorController',
  'postulante' => 'Vacantes\PostulanteController'
]);

//fin
//*********************************************************************************

//*********************************************************************************
//rutas para el perfil de personal
//inicio
Route::get('profile/{u}/{p}/{v}/consulta', 'Profile\ProfileController@consulta');
Route::get('profile/{p}/{v}/generar', 'Profile\ProfileController@generar');
Route::post('createBasica', 'Profile\ProfileController@createBasica');
Route::post('createComplementaria', 'Profile\ProfileController@createComplementaria');
Route::post('createRegistro', 'Profile\ProfileController@createRegistro');
Route::put('updateRegistro', 'Profile\ProfileController@updateRegistro');
Route::post('createSalud', 'Profile\ProfileController@createSalud');
Route::put('updateSalud', 'Profile\ProfileController@updateSalud');
Route::post('createBeneficiarios', 'Profile\ProfileController@createBeneficiarios');
Route::put('updateBeneficiarios', 'Profile\ProfileController@updateBeneficiarios');

Route::get('curriculum', 'Profile\ProfileController@curriculum');

Route::resource('profile', 'Profile\ProfileController');
//fin
//*********************************************************************************

/********************************************************************************** */
//rutas para el listado dependiente de estados y municipios
//inicio ***************
Route::get('listStates/{id}', 'Entidades\EntidadesController@listadoMunicipios');
 
//fin
//*************************************************************************

Route::auth();
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
